# The 10th Week

以下為到職第10週的紀錄，這週重點：

- RMG棋牌
  - 處理 RMG 每登入一次就會上分10,000 的問題
  - 處理 RMG 在正式站抓不到遊戲紀錄問題
  - 處理 RMG 取得遊戲紀錄過慢的問題（15分鐘）
- MG 遊戲
  - 修改打 `WalletOrder::RescueJob`, `WalletOrder::TransferJob` api 失敗的測試
  - 修改進入大廳的測試
- portal cat
  - 新聞 CRUD



### RMG 

#### 抓取遊戲紀錄

```sql
SELECT time, partner, action, http_method, params, return_code, response_data, url FROM "autogen"."api_log" WHERE $timeFilter AND partner = 'rmg' AND action = 'login'
```

![image-20191113135944284](/Users/hanting/Library/Application Support/typora-user-images/image-20191113135944284.png)

```sql
SELECT time, partner, action, http_method, params, return_code, response_data, url FROM "autogen"."api_log" WHERE $timeFilter AND partner = 'rmg' AND action = 'bet log'
```

#### Rails logger：寫紀錄很重要

以下為使用 console 呼叫 poker_rmg.log

```shell
cd log
ls
tail -f poker_rmg.log
```

在 ruby 使用 logger

```ruby
Rails.logger.poker_rmg.info("...")
Rails.logger.poker_rmg.error("...")
Rails.logger.poker_rmg.warning("...")
```

config/initialize/logger.rb

```ruby
formatter_type_01 = Proc.new do |severity, time, progname, msg|
  formatted_severity = sprintf("%-5s",severity.to_s)
  formatted_time = time.strftime("%FT%T.%5N")
  msg = (formatted_severity == "ERROR") ? msg.red : msg
  "[#{formatted_time} #{$$}] #{formatted_severity} : #{msg.to_s.strip}\n"
end

# GamePartnerMap for config/initializers/jvd.rb
#
# log_name = [
#   :casino_allbet, formatter: formatter_type_01,
#   :casino_ebet, formatter: formatter_type_01,
#   ...
#   :egame_qtech, formatter: formatter_type_01,
#   ...
#   :fishing_allbet, formatter: formatter_type_01,
# ]
#
log_name = GamePartnerMap.each_with_object([]) do |(k, v), arr|
  v.map do |z|
    arr << ["#{k}_#{z}".to_sym, { formatter: formatter_type_01 }] # formatter_type_01 use call
  end
end
log_name << [:transfer_fail, { formatter: formatter_type_01 }]
log_name << [:get_bet_log_job, { formatter: formatter_type_01 }]

log_name.each { |i| MultiLogger.add_logger(*i) }
```

```ruby
GamePartnerMap = {
  :casino    => [:allbet, :ebet, :ag, :agin, :sunbet, :ts, :sa, :n2, :ku, :sunbettgp, :ea], # Deprecated: :hy, :bbin
  :egame     => [:mg, :allbet, :pt, :sa, :qtech], # Deprecated: :ace333, :bbin
  :sports    => [:m8, :imsb, :ibcbet, :crownm, :betlink, :xj_agile_bet], # Deprecated: :bbin
  :lottery   => [:tcg, :gg], # Deprecated: :bbin
  :cockfight => [:s128cockfight, ],
  :horserace => [:r128horse, ],
  # :bullfight => [:t128bullfight, ], # Deprecated
  :esports   => [:esportsbull, :vb],
  # :stream    => [:zm], # Deprecated
  :poker     => [:gamingmart, :kygaming, :bole, :rmg],
  :fishing   => [:agin, :sa, :allbet]
}.freeze
```

### MG

#### 測試錯誤：`check_job`、`transfer_job`

需要修改測試的檔案為：check_job_spec.rb, transfer_job_spec.rb

````shell
Failures:

1) WalletOrder::RescueJob MG 轉進 check fail resuce job perform fail if api fail, game_order -> pending
   Failure/Error: expect(@game_order.status).to eq 'pending'

     expected: "pending"
          got: "success"

     (compared using ==)
   # ./spec/jobs/wallet_order/rescue_job_spec.rb:84:in `block (6 levels) in <top (required)>'

2) WalletOrder::RescueJob MG 轉出 check fail resuce job perform fail if api fail, game_order -> pending
   Failure/Error: expect(@game_order.status).to eq 'pending'

     expected: "pending"
          got: "success"

     (compared using ==)
   # ./spec/jobs/wallet_order/rescue_job_spec.rb:84:in `block (6 levels) in <top (required)>'

Failures:

1) WalletOrder::TransferJob MG 轉進 job  perform fail if api fail, game_order -> resuce
   Failure/Error: expect(@game_order.status).to eq 'processing'

     expected: "processing"
          got: "checking"

     (compared using ==)
   # ./spec/jobs/wallet_order/transfer_job_spec.rb:67:in `block (6 levels) in <top (required)>'

2) WalletOrder::TransferJob MG 轉出 job  perform fail if api fail, game_order -> resuce
   Failure/Error: expect(@game_order.status).to eq 'processing'

     expected: "processing"
          got: "checking"

     (compared using ==)
   # ./spec/jobs/wallet_order/transfer_job_spec.rb:67:in `block (6 levels) in <top (required)>'
````

````shell
Failed examples:

rspec ./spec/jobs/wallet_order/transfer_job_spec.rb[1:7:3:1] # WalletOrder::TransferJob MG 轉進 job  perform fail if api fail, game_order -> resuce

rspec ./spec/jobs/wallet_order/transfer_job_spec.rb[1:8:3:1] # WalletOrder::TransferJob MG 轉出 job  perform fail if api fail, game_order -> resuce
````

如何跑單元測試，請下以下指令

````shell
$ rspec spec/jobs/wallet_order/rescue_job_spec.rb:76
# ......FF......

$ rspec spec/jobs/wallet_order/transfer_job_spec.rb:59
````

測試錯誤結果是因為被以下的rescue所救，進去 rescue_job.rb 的 :success 而錯了：

````ruby
def transfer(game_order, _options = {})
  ...
  # 1263 examples, 4 failures, 47 pendings
  begin
    sty_flow(__method__)
  rescue => e
    Rails.logger.egame_mg.debug(e.message)
  end
end
````

以下為 rescue_job.rb 的 perform

````ruby
def perform(game_wallet_id)
  game_wallet = Client::GameWallet.find(game_wallet_id)
  game_order = game_wallet.game_order
  set_render_text(game_order.order_type) # @LOADING_TEXT @FAIL_TEXT @SUCCESS_TEXT
  begin
    game_object = SimpleGameFactory.create_game(game_wallet.name, game_order.user)
    check_method = game_object.class.method(:CheckState)
    check = if game_object.respond_to?(:check)
              game_object.check(game_order)
            else
              # Since these game objects don't have the check method, so we
              # use the result of its transfer method as the check result.
              game_object.transfer(game_order)
            end
  
    case check_method.call(check, game_order)
=>  when :success
      success_path(game_wallet, game_order)
      return { status: true, message: @SUCCESS_TEXT }
    when :error
      raise CustomError.new(:checking_error, @FAIL_TEXT)
    when :loading # call check job again
    ...
end
````

#### 進入大廳測試無法過

看測試下指令

```shell
$ rspec spec/requests/api/games_spec.rb:20
```

執行到 @user_token = gen_user_token 的錯誤訊息

````shell
VCR::Errors::UnhandledHTTPRequestError: 
````

rails routes 的用法，此目的是要找到 api 相關的路徑，可是後來發現用 `bin/rails routes -g api`卻找不到 grape api相關的路徑

```cmd
bin/rails routes                # 列出所有路由
bin/rails routes -g admin       # 搜尋特定路由 (grep)
bin/rails routes -c admin/users # 搜尋路由中特定 controller
```

mock 主要是用來模擬「外部邏輯」，因此可以使用 mock objects，在 RSpec 裡面叫做 double（替身）。

```ruby
allow(user).to receive(:mg_account).and_return('DFCT1234567890')
allow(user).to receive(:mg_pwd).and_return('sivnru23j4')
```

使用webmock

````ruby
stub_request(:any, "www.example.com").
  to_return(body: "abc", status: 200,
    headers: { 'Content-Length' => 3 })

Net::HTTP.get("www.example.com", '/')    # ===> "abc"
````

````ruby
stub_request(:any, 'www.example.net').
  to_return { |request| {body: request.body} }

RestClient.post('www.example.net', 'abc')    # ===> "abc\n"
````

以下為用在修理測試的實例

gen_token:

````ruby
stub_request(:post, "https://ag.adminserv88.com/lps/j_spring_security_check").
  to_return(:body => {'token': 'xxxx', 'id': 'xxx'}.to_json)

params = {
   method: :post,
   url: 'https://ag.adminserv88.com/lps/j_spring_security_check',
   payload: {
     j_username: "chenhanting",
     j_password: "as789123456",
     },
   headers: {
     'X-Requested-With' => 'X-Api-Client',
     'X-Api-Call' => 'X-Api-Client',
   },
}
RestClient::Request.execute(params)
````

gen_user_token

````ruby
stub_request(:post, "https://ag.adminserv88.com/member-api-web/member-api").
  to_return(body: "<mbrapi-login-resp status=\"0\" token=\"yyyy\" />")

params = {
   method: :post,
   url: 'https://ag.adminserv88.com/member-api-web/member-api',
   payload: {
     j_username: "chenhanting",
     j_password: "as789123456",
     },
   headers: {
     'X-Requested-With' => 'X-Api-Client',
     'X-Api-Call' => 'X-Api-Client',
   },
}
RestClient::Request.execute(params)
````

create

````ruby
stub_request(:post, "https://ag.adminserv88.com//lps/secure/network/:id/downline").
  to_return(body: { "success": true }.to_json)
````

如何使得 games_spec.rb 通過測試，我在 games_spec.rb 編輯幾行

````ruby
require 'rails_helper'

RSpec.describe 'Games API', type: :request do
  let!(:company) { FactoryBot.create :fatcat_com_with_games_pt_mg_crownm }
  let!(:user) { FactoryBot.create :user, company: company, quick_pass: true }
  before(:all) { VCR.turn_off! }
  after(:all) { VCR.turn_on! }

  describe 'Show Games' do
    it 'shows E-games' do
      get '/api/FATCAT/games'
      expect(response).to have_http_status 200
      res = JSON.parse(response.body)
      expect(res['status']).to eq 'OK'
      expect(res['content']).to have_key('egame')
      expect(res['content']['egame'][0]).to include('name' => 'mg')
      expect(res['content']['egame'][1]).to include('name' => 'pt')
    end
  end

  describe 'Login Game' do
    it 'goes to MG game lobby' do
      post '/api/FATCAT/login', params: { account: user.account, password: '12345678' }
      res = JSON.parse(response.body)
      token = res['content']['auth_token']
      expect(response).to have_http_status 201

      # 通用
      # mockup_request('mg', :success)

      # mg 帳號密碼偽裝
      # 使用mock偽裝mg帳/密(失敗)
      # allow(user).to receive(:mg_account).and_return('DFCT1234567890')
      # allow(user).to receive(:mg_pwd).and_return('sivnru23j4')
      # 新增mg帳/密       (成功)
      # user.update(mg_account: 'DFCT1234567890', mg_pwd: 'sivnru23j4')

      # 提供 gen_token 使用
      stub_request(:post, 'https://ag.adminserv88.com/lps/j_spring_security_check')
        .to_return(body: { 'token': 'xxxx', 'id': 'xxx' }.to_json)
      # 提供 gen_user_token 使用
      stub_request(:post, 'https://ag.adminserv88.com/member-api-web/member-api')
        .to_return(body: "<mbrapi-login-resp status=\"0\" token=\"yyyy\" />")
      # 提供 create 使用
      stub_request(:put, 'https://ag.adminserv88.com/lps/secure/network/xxx/downline')
        .to_return(status: 200, body: { "success": true }.to_json)

      get '/api/FATCAT/games/egame/mg', params: { auth_token: token, device: 'mobile', locale: 'zh-CN' }
      res = JSON.parse(response.body)

      expect(response).to have_http_status 200
      expect(res['status']).to eq 'OK'
      expect(res['content']).to have_key('login_url')

      # TODO
      # it should go into fatcat game lobby,
      # but test need to switch to WWW mode...
    end
  end
end

````

以下為 `get '/api/FATCAT/games/egame/mg', params: ...` 的用法，其中`/api/FATCAT/games/egame/mg` 會進入grape api（進入 jvd/games.rb ）

````ruby
%w(get post patch put head delete cookies assigns follow_redirect!).each do |method|
  define_method(method) do |*args|
    # reset the html_document variable, except for cookies/assigns calls
    unless method == "cookies" || method == "assigns"
      @html_document = nil
    end

    integration_session.__send__(method, *args).tap do
      copy_session_variables!
    end
  end
end
````

以下為 spec/rails_helper.rb 的 mockup_request 方法，這個方法可以提供 mg 遊戲打 token,  user_token, create...等 api。

```ruby
def mockup_request(partner, status)
  case partner
  when 'allbet'        then url, route_class = /fake_allbet_route/, FakeAllbetRoute
  when 'ebet'          then url, route_class = /fake_ebet_route/, FakeEbetRoute
  when 'mg'            then url, route_class = /ag.adminserv88.com/, FakeMgRoute
  when 'pt'            then url, route_class = /imone.imaegisapi.com/, FakePtRoute
  when 'r128horse'     then url, route_class = /fake_r128_horse_route/, FakeR128HorseRoute
  when 's128cockfight' then url, route_class = /fake_s128_cock_fight_route/, FakeS128CockFightRoute
  when 'm8'            then url, route_class = /apir.mywinday.com/, FakeM8SportRoute
  when 'ag'            then url, route_class = /fake_ag_route/, FakeAgRoute
  end

  case status
  when :success    then stub_request(:any, url).to_rack(route_class)
  when :fail       then stub_request(:any, url).to_return(status: [500, 'Internal Server Error'])
  when :not_found  then stub_request(:any, url).to_return(status: 404)
  else false
  end
end
```

再來看 FakeMgRoute，位於 fake_mg_routes.rb

````ruby
require 'sinatra/base'

class FakeMgRoute < Sinatra::Base
  configure do
    # include new mime type
    mime_type :tpago, 'application/vnd.tpago.billpayment'
  end

  post '/lps/j_spring_security_check' do
    file_response 'token.json'
  end

  put '/lps/secure/network/:id/downline' do
    file_response 'create.json'
  end

  post '/member-api-web/member-api' do
    if request.env["HTTP_X_REQUESTED_WITH"] == "X-Api-Client"
      if Hash.from_xml(request.body)["mbrapi_login_call"]
        file_response 'login.xml'
      else
        file_response 'transfer.xml'
      end
    else
      file_response 'balance.xml'
    end
  end

  private

  def file_response(file)
    status 200
    content_type :xml
    File.open("#{File.dirname(__FILE__)}/fixtures/fake_mg/success/#{file}", 'r').read
  end
end
````

在肥貓的測試中，VCR的預設值為開啟VCR。如何把VCR關掉，使用下面行數

````ruby
before(:all) { VCR.turn_off! }
after(:all) { VCR.turn_on! }
````

### 新專案：新聞

只要下 rails db:create 便不用自己在mysql新增資料表。
另外附上直接在mysql創建資料表的流程：http://www.mysqltutorial.org/mysql-create-database/

```shell
$ rails db:create                                                  
Database 'portalcat_dev' already exists
Database 'portalcat_test' already exists
```

先新增 model 欄位：

```shell
rails g model News title summary content:text click:integer category:string publish:boolean user:references
```

```ruby
class CreateNews < ActiveRecord::Migration[5.1]
  def change
    create_table :news do |t|
      t.string :title
      t.string :summary
      t.string :editor
      t.string :category
      t.text :content
      t.integer :click
      t.datetime :publish_at
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
```

新增controller

```shell
rails g controller admin/news index show edit 
```

線上 erb 轉換 slim 產生器：http://erb2slim.com/

如何處理bootstrap導覽列蓋掉內文問題：加入以下 css 至 css

```css
body { 
    padding-top: 65px; 
}
```

查看db不能存取的原因

```ruby
person = Person.new
person.validate!            # => ["cannot be nil"]
person.errors.full_messages # => ["name cannot be nil"]
# etc..
```

此外 rails 有內建加入索引的方式，有許多教學教導如何使用 add_index

### 其他

1. 利用 crontab 來做 Linux 固定排程：[https://code.kpman.cc/2015/02/11/%E5%88%A9%E7%94%A8-crontab-%E4%BE%86%E5%81%9A-Linux-%E5%9B%BA%E5%AE%9A%E6%8E%92%E7%A8%8B/](https://code.kpman.cc/2015/02/11/利用-crontab-來做-Linux-固定排程/)

2. Jack 提醒我們在 app/api/v2/helpers/bet_histories.rb，會因為大富翁棋牌和DG真人的命名產生錯誤，如果當時命名Rmg, Dg，而不是RMG, DG便不會產生錯誤 

3. 打成功的 api，以後可能有機會用

   ````text
   https://api.github.com/users/kaochenlong
   https://api.github.com/users/chenhanting
   ````

4. mock 參考資料

   - https://mgleon08.github.io/blog/2016/01/29/rspec-plus-factory-girl/
   - https://ihower.tw/rails/files/ihower-rspec-mock.pdf
   - https://github.com/rspec/rspec-mocks

5. Gem 安裝
   - simple_form: https://github.com/plataformatec/simple_form
   - Breadcrumbs：https://github.com/weppos/breadcrumbs_on_rails
   - act-as-taggable-on: https://github.com/mbleigh/acts-as-taggable-on
   - font-awesome-rails: https://github.com/bokmann/font-awesome-rails

6. 資料庫索引：http://www.codedata.com.tw/database/mysql-tutorial-9-table-index/

7. 處理bootstrap導覽列蓋掉內文問題：https://stackoverflow.com/questions/10336194/top-nav-bar-blocking-top-content-of-the-page
8. 使用AASM或draftsman：https://www.reddit.com/r/rails/comments/8d3nb2/do_i_need_a_gem_for_draftpublish_functionality/