# The 11th Week

以下為到職第11週的紀錄，這週重點：

- 處理 MG遊戲紀錄回不來的問題
- 威博電競論壇-新聞部分
  - View：新增index, new, show (使用slim)，以及更新畫面的js（update.js.erb）
  - Controller：基本的CRUD，加上update_time 的 action 操作 index 的按鈕
  - Model：新增基本欄位

### MG 

遊戲紀錄沒有回來

```ruby
user = User.find_by account:"chenhanting"
key = GamesV3::Mg.keys('mg')
job = GetBetLogJob.new.perform('mg', key, '2019.11.19 15:30:00', '2019.11.19 16:30:00', 3, 6, '隨便打')
```

### 威博電競論壇：新聞

*下列為新增至 monday 的紀錄*

view

1. 整體
   1. 加入麵包屑(Breadcrumbs)
   2. 套用 bootstrap 版型
   3. 加入繁體中文、簡體中文、英文語系
2. index
   1. 分頁功能
3. edit/new
   1. 表單內容：標題、種類、作者、總結、內文、發佈日期
   2. 內文使用ckeditor (富文本編輯工具)
   3. 發佈日期一共會新增兩個按鈕
      1. 立即發佈（發佈時間等於現在時間）
      2. 不發佈（意即發佈時間為空值）
4. show(先擱置，因為還未確定如何顯示)

controller

model：新增以下欄位

-  :title
-  :summary
-  :editor
-  :category
-  :content
-  :click_base, default:0
-  :click, default:0
-  :publish_at

bootstrap simple_form 使用範例：http://simple-form-bootstrap.plataformatec.com.br/documentation

按鈕

- ckeditor 整合 form_for

````slim
= simple_form_for(article) do |f|
  .form-inputs
    = f.cktext_area :content, value: 'Default value', id: 'sometext'
  .form-actions
    = f.button :submit, data: { disable_with: "Submitting..." }, class: 'btn btn-primary'
````

- 2019.11.20 學到：button 四種屬性、form 有 hidden_field、remote: true 如何使用

- 2019.11.21 學到：`$("#idname").html('<%= j(render 'new') %>');` 

- 2019.11.22 學到：打api、toggle text、靜態/動態之間相互關係、hidden/show對應屬性、前後分離概念

  - data-remote = "true" is used by the Ruby On Rails framework to submit the form/link/button as an ajax request. If you are interested here is the guide discussing how Ruby on Rails works with javascript

  - jquery select option：https://dotblogs.com.tw/alanjiang/archive/2011/01/26/21061.aspx

    ````javascript
    // 選值
    $("#news_publish_at_1i").find(":selected").text();
    $("#news_publish_at_1i").find(":selected").val();
    
    // 清空
    $("#news_publish_at_1i").empty();
    
    // 直接賦值
    $("#news_publish_at_1i").val("2018");
    
    // disabled
    $("#news_publish_at_1i").attr('disabled','disabled')
    
    // 自己連同兄弟一起 disabled
    $("#news_publish_at_1i").attr('disabled', true).siblings().attr('disabled',true);
    
    // 自己連同兄弟一起 enabled
    $("#news_publish_at_1i").attr('disabled', false).siblings().attr('disabled',true);
    
    // 狀態（ 一共有"undefined"和"disabled" ）
    $("#news_publish_at_1i").attr('disabled');
    ````

  - 基本 javascript 範例

    ````html
    <script>
      function myFunction() {
        console.log('This is myFunction');
      }
    </script>
    ````

  - html-button 屬性

    | 性質   | 描述                                                         |
    | ------ | ------------------------------------------------------------ |
    | button | 该按钮是可点击的按钮（Internet Explorer 的默认值）。         |
    | reset  | 该按钮是重置按钮（清除表单数据）。                           |
    | submit | 该按钮是提交按钮（除了 Internet Explorer，该值是其他浏览器的默认值）。 |
    | none   |                                                              |

    參考資料：https://dev.to/clairecodes/why-its-important-to-give-your-html-button-a-type-58k9

  - 要怎麼把 slim/erb 值讀進 javascript：[How to embed dynamic Ruby code to “javascript” section in Slim templates?](https://stackoverflow.com/questions/5646315/how-to-embed-dynamic-ruby-code-to-javascript-section-in-slim-templates)

  - button: https://apidock.com/rails/ActionView/Helpers/FormBuilder/button

  - button_to: https://apidock.com/rails/ActionView/Helpers/UrlHelper/button_to

    button_to 跟 form_for 的功能很像

  - Rails 中使用 AJAX ： form 表單加上 `remote: true`

    參考資料：[https://pjchender.github.io/2018/01/22/rails-ajax-%E5%B0%8F%E6%8A%80%E5%B7%A7/](https://pjchender.github.io/2018/01/22/rails-ajax-小技巧/)

    參考資料：https://www.rubyguides.com/2019/03/rails-ajax/

  - hidden_field 使用方式：https://stackoverflow.com/questions/3131982/how-do-i-use-hidden-field-in-a-form-for-in-ruby-on-rails

  - `$("#idname").html('<%= j(render 'new') %>');` 

    - 如果只寫 `$("#idname").html('<%= render 'new' %>');` 會產生錯誤，舉例來說，當讀到雙引號`""`的時候，瀏覽器就會讀取錯誤。

      參考資料：https://stackoverflow.com/questions/12518526/what-does-the-j-function-in-rails-do

    - `.html()` 為 Ajax 用來渲染畫面用的

  - `$("#idname").modal('show');`  裡面的 modal 指的是 bootstrap 特有的 dialog 套件

    參考資料：https://www.runoob.com/bootstrap/bootstrap-modal-plugin.html

  -  routes

    關鍵字：collection, member

    參考資料：https://railsbook.tw/chapters/11-routes.html

    ````ruby
    # 錯的（違反慣例）
    resources :news do
      get 'update_time'
    end
    
    # 對的
    resources :news do
      get 'update_time', on: :member
    end
    ````

    rails routes

    ````shell
    # 錯的（違反慣例）
    news_update_time GET        /news/:news_id/update_time(.:format)
    
    # 對的
    update_time_news GET        /news/:id/update_time(.:format)
    ````

    在於:news_id 和 :id 的差別。:id符合慣例，:news_id違反慣例。如果不使用慣例，就無法使用

    ````ruby
    class Admin::NewsController < ApplicationController
      before_action :find_news, only: %i[edit update destroy show update_time]
      ...
      
      private
    
      def find_news
        @article = News.find_by(id: params[:id])
      end
      ...
    end
    ````

  - 在path傳入參數，controllers 的 params 即可讀取。這樣一來，如果兩個按鈕進去同一個action即可分辨

    ````ruby
= link_to '註銷', update_path(a, val: "0"), remote: true, class: 'btn'
    = link_to '立即', update_path(a, val: "1"), remote: true, class: 'btn'
    ````
    
    ````html
{
     "val"= "canceled", 
     "subdomain"= "admin", 
     "controller"= "admin/news", 
     "action"= "update_time", 
     "id"= "39"
    }
    ````
    
  - 如何使用bootstrap tooltip：https://remonote.jp/rails-bootstrap-tooltip

  - index 顯示內容以 id 排序：https://apidock.com/rails/ActiveRecord/QueryMethods/order

  - 如何使用jquery重整畫面：https://stackoverflow.com/questions/5404839/how-can-i-refresh-a-page-with-jquery

  - hidden 與 show 會遇到的問題：https://stackoverflow.com/questions/40749613/how-to-show-hidden-elements-in-javascript-html

  - bootstrap 省略文字：[How to work with ellipsis in bootstrap responsive table](https://stackoverflow.com/questions/39148127/how-to-work-with-ellipsis-in-bootstrap-responsive-table)

  - 厲害的提示窗套件：https://github.com/CodeSeven/toastr

  - 在yaml檔如何打冒號：https://stackoverflow.com/questions/11301650/how-to-escape-indicator-characters-i-e-or-in-yaml

  - 如何在發佈還沒完成時禁止一直按按鈕： `aria-disabled="true"`

    相較於 `aria-disabled="true"`， `disabled="true"` 就是單純不能按

  - 踩雷：[What does $(selector)[0\] mean in jQuery?](https://stackoverflow.com/questions/32783869/what-does-selector0-mean-in-jquery)

  - 如何在 jquery 使用 toggle 狀態

    以下為範例：

    ````javascript
  function myFunction() {
      var x = document.getElementById("myDIV");
    if (x.innerHTML === "Hello") {
        x.innerHTML = "Swapped text!";
      } else {
        x.innerHTML = "Hello";
      }
    }
    ````
  
    參考資料：https://www.w3schools.com/howto/howto_js_toggle_text.asp
  
  - Rails query: https://rails.ruby.tw/active_record_querying.html

  - Rails api mode: https://railsbook.tw/chapters/22-api-mode.html

  - 如何打 api

    下列為 api/v1/news_controller.rb

    ````ruby
  class Api::V1::NewsController < Api::V1::BaseController
      before_action :find_news, only: %i[show]
  
      def index
        @articles = News.all
        render json: News.publish_articles
      end
    
      def show
        render json: @article
      end
    
      private
    
      def find_news
        @article = News.find_by(id: params[:id])
      end
    end
    ````
  
    下列為 routes.rb api 的路由
  
    ````ruby
  constraints subdomain: 'api' do
      scope module: 'api' do
      namespace :v1, defaults: { format: :json } do
          root to: 'base#index'
          get 'ping', to: 'base#ping'
          get 'ping_auth', to: 'base#ping_auth'
          get 'info', to: 'base#info'
          resources :news, only: [:index, :show]
        end
      end
    end
    ````
  

### 其他

1. 學習部署：current 與 release 資料夾

2. 了解games_v2的歷史

3. 了解Gemfile.lock 與 schema 不要直接編輯的原因

4. Stack 與 Heap

5. 部署問題：unicorn 與 Puma 的差別

6. x-www-form-urlencoded 

7. How do I create multiple submit buttons for the same form in Rails：https://stackoverflow.com/questions/3027149/how-do-i-create-multiple-submit-buttons-for-the-same-form-in-rails

8. 如何前後分離? 以下為兩種解析json的工具

   ````javascript
   function reqListener () {
     console.log(this.responseText);
   }
   
   var oReq = new XMLHttpRequest();
   oReq.addEventListener("load", reqListener);
   oReq.open("GET", "http://api.lvh.me:3100/v1/news");
   oReq.send();
   ````

   ````javascript
   fetch('http://api.lvh.me:3100/v1/news')
     .then(function(response) {
       return response.json();
     })
     .then(function(myJson) {
       console.log(myJson);
     });
   ````

   ````javascript
   fetchNews() {
     // Where we're fetching data from
     fetch('http://api.lvh.me:3100/v1/news')
       // We get the API response and receive data in JSON format...
       .then(response => response.json())
       // ...then we update the users state
       .then(data =>
         this.setState({
           news: data,
           isLoading: false,
         })
       )
       // Catch any errors we hit and update the app
       .catch(error => this.setState({ error, isLoading: false }));
   }
   ````

   將 api 畫成 table：https://www.freecodecamp.org/forum/t/translating-a-json-data-to-html-element/137934