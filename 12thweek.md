# The 12th Week

以下為到職第12週的紀錄，這週重點：

- 處理 Rubocop跳出錯誤
- 三個分支
  - (練習) 上傳圖片：利用carrierwave上傳一張、多張照片
  - (練習) 使用多型關聯上傳圖片、訊息，並且在view寫nested_form，改寫controller的strong_parameter
  - (工作) ckeditor 傳圖片設定

### 新聞的CRUD

pagy的api也要打分頁：https://ddnexus.github.io/pagy/how-to.html

分頁部分總共分成頁面的分頁、前端框架的分頁，以及純api的分頁，目前新聞的需求為純api的分頁。除此之外，打出的api中除了打出新聞資料外，也要輸出分頁資訊。

pagy 預設一頁共有25筆資料，不過在pagy.rb可以重設預設值，並且在controller可以調整一頁的資料數

````ruby
class Api::V1::NewsController < Api::V1::BaseController
  ...
  after_action { pagy_headers_merge(@pagy) if @pagy }

  def index
    @pagy, @records = pagy(News.all.publish_articles, items: 3)
    render json: { page: @pagy, data: @records }
  end
  ...
end
````

### Carrierwave Uploader

使用方式：https://pjchender.github.io/2017/10/06/rails-carrierwave-uploader/

查看圖片：https://code.tutsplus.com/articles/uploading-with-rails-and-carrierwave--cms-28409

#### 上傳一張照片

````shell
> rails generate uploader Pic
create  app/uploaders/pic_uploader.rb

> rails g migration add_pic_to_news pic:string
create  db/migrate/20191125041334_add_pic_to_news.rb

> rails db:migrate
````

rails console

````ruby
> news = News.find_by id:4

> news.pic
=> #<NPicUploader:0x00007facf6c84128
 @cache_id=nil,
 @file=
  #<CarrierWave::SanitizedFile:0x00007facf6c7f7b8
   @content=nil,
   ...
 ...
 @model=
  #<News:0x00007facf5b5b608
   id: 4,
   title: "RMG",
   editor: "ck",
   ...
   created_at: Sat, 23 Nov 2019 00:35:14 CST +08:00,
   updated_at: Mon, 25 Nov 2019 12:19:50 CST +08:00,
   pic: "bRB0a20BJleJMoPMhWFm.jpg">,
 @mounted_as=:pic,
...
  
> news.pic.url
=> "/uploads/news/pic/4/bRB0a20BJleJMoPMhWFm.jpg"
````

#### 上傳多張照片

目前遇到的問題是，在simple_form_for裡面無法正常運作

參考資料：

- [carriverwave multiple file upload with sql3 多圖上傳](http://kuro-sean-blog.logdown.com/posts/702965)
- 文件：https://github.com/carrierwaveuploader/carrierwave
- 如何使用 image_tag

#### 上傳多張照片（不覆蓋）

文件中提到，如果不覆寫的話，要改成這樣。

identifier用意好像為印出圖片名，而不是印出全部的url。

````erb
<% user.avatars.each do |avatar| %> 
  <%= hidden_field :user, :avatars, multiple: true, value: avatar.identifier %> 
<% end %> 
<%= form.file_field :avatars, multiple: true %>
````

專案 code

````erb
<% @photo.images.each do |pic| %>
  <%= hidden_field :photo, :images, multiple: true, value: pic.identifier %>
  <%= "舊檔案：#{pic.identifier}" %><br />
<% end %>
<%= form.file_field :images, id: :photos, class: 'form-control', multiple: true %>
````

rails console

````ruby
> params
=> <ActionController::Parameters {"utf8"=>"✓", "_method"=>"patch", "authenticity_token"=>"mAWxe0AsgcXn/dxeSbOLFbXSAwAn41s9//XOer2691mVsqnlmGgRdi+gqKnYJqDinr597r4gBzsjX04LF0z7aw==", "photo"=>{"title"=>"鬼滅之刃", "content"=>"<p>哈囉</p>", "images"=>["images__1_.jpeg", "images__3_.jpeg", "images__4_.jpeg", "images__2_.jpeg", "bRB0a20BJleJMoPMhWFm.jpg"]}, "commit"=>"更新Photo", "subdomain"=>"admin", "controller"=>"admin/photo", "action"=>"update", "id"=>"6"} permitted: false>
  
> params[:photo]
=> <ActionController::Parameters {"title"=>"鬼滅之刃", "content"=>"<p>哈囉</p>", "images"=>["images__1_.jpeg", "images__3_.jpeg", "images__4_.jpeg", "images__2_.jpeg", "bRB0a20BJleJMoPMhWFm.jpg"]} permitted: false>

> params[:photo][:images]
=> ["images__1_.jpeg", "images__3_.jpeg", "images__4_.jpeg", "images__2_.jpeg", "bRB0a20BJleJMoPMhWFm.jpg"]
````

上述功能可能在gem做掉，才有不覆寫舊圖片的效果。不過儘管這樣，還是要了解html name的用法，以及rails params 與 name的關係。

hidden_field用法為：https://apidock.com/rails/ActionView/Helpers/FormHelper/hidden_field

如何在表單選多個選項：[How do I select multiple things in a form?](https://www.ruby-forum.com/t/how-do-i-select-multiple-things-in-a-form/70186)

#### 圖庫

接下來的目標是在同一個表單裡面，存取不同model的關聯

第一個範例為在form裡面的表單填寫種類，種類為不同model。在這情境中，category與post為主從關係，post屬於某種類，一個種類對應很多post，但目前遇到的狀況：news和galleries為主從關係，news對上很多照片，照片屬於news的狀況

```ruby
# app/models/post.rb
class Post < ActiveRecord::Base
  belongs_to :category
end

# app/models/category.rb
class Category < ActiveRecord::Base
  has_many :posts
end
```

接下來看第二個範例：

For an ActiveRecord::Base model and association this writer method is commonly defined with the `accepts_nested_attributes_for` class method

````ruby
class Person < ActiveRecord::Base
  has_one :address
  accepts_nested_attributes_for :address
end
````

As you might have inflected from this explanation, you *don't* necessarily need an ActiveRecord::Base model to use this functionality. The following examples are sufficient to enable the nested model form behavior:

````ruby
class Person
  def address
    Address.new
  end
 
  def address_attributes=(attributes)
    # ...
  end
end
````

A nested model form will *only* be built if the associated object(s) exist. This means that for a new model instance you would probably want to build the associated object(s) first.

Consider the following typical RESTful controller which will prepare a new Person instance and its `address` and `projects` associations before rendering the `new` template

````ruby
class PeopleController < ApplicationController
  def new
    @person = Person.new
    @person.built_address
  end
 
  def create
    @person = Person.new(params[:person])
    if @person.save
      # ...
    end
  end
end
````

form_for and output HTML

````erb
<%= form_for @person do |f| %>
  <%= f.text_field :name %>
 
  <%= f.fields_for :address do |af| %>
    <%= af.text_field :street %>
  <% end %>
<% end %>
````

````html
<form action="/people" class="new_person" id="new_person" method="post">
  <input id="person_name" name="person[name]" type="text" />
 
  <input id="person_address_attributes_street" name="person[address_attributes][street]" type="text" />
</form>
````

params

````ruby
{
  "person" => {
    "name" => "Eloy Duran",
    "address_attributes" => {
      "street" => "Nieuwe Prinsengracht"
    }
  }
}
````

以下新增model：Message來練習使用nested_form

````shell
rails g model Message content news:references
rails g model Gallery images:json news:references
rails db:migrate

rails generate uploader image
````

````ruby
class News < ApplicationRecord
  ...
  has_many :galleries
  accepts_nested_attributes_for :galleries
  
  has_many :messages
  accepts_nested_attributes_for :messages
end
````

````ruby
class Gallery < ApplicationRecord
  belongs_to :news
  
  mount_uploaders :pics, ImageUploader
end
````

````ruby
class Message < ApplicationRecord
  belongs_to :news
end
````

params(沒有圖庫時)

````ruby
> params
=> <ActionController::Parameters {"utf8"=>"✓", "authenticity_token"=>"x/OH34WIB//w71TtZlX7vsBR8KXt8wQY2kuA99CZXczKRJ9BXcyXTDiyIBr3wNBJ6z2OS3QwWB4G4QCGem9R/g==", "news"=><ActionController::Parameters {"title"=>"RMG", "category"=>"gossip", "editor"=>"Han", "summary"=>"！＠＃＄％", "content"=>"<p>！＠＃＄％</p>", "messages_attributes"=>{"0"=>{"content"=>"你好，稍等一下"}, "1"=>{"content"=>"您好，稍等一下"}}, "publish_status"=>"0", "publish_at(1i)"=>"2019", "publish_at(2i)"=>"12", "publish_at(3i)"=>"26", "publish_at(4i)"=>"18", "publish_at(5i)"=>"26"} permitted: false>, "commit"=>"發佈新聞", "subdomain"=>"admin", "controller"=>"admin/news", "action"=>"create"} permitted: false>
  
> params[:news][:messages_attributes]
=> <ActionController::Parameters {"0"=>{"content"=>"你好，稍等一下"}, "1"=>{"content"=>"您好，稍等一下"}} permitted: false>
````

simple_form_for 也有：https://github.com/plataformatec/simple_form/wiki/Nested-Models

params(有圖庫)

````ruby
> params[:news][:galleries_attributes]["0"]

=> <ActionController::Parameters {"images"=>[#<ActionDispatch::Http::UploadedFile:0x00007fadc69af468 @tempfile=#<Tempfile:/var/folders/tl/3p_457555lz9zy3mv_5kpnqh0000gn/T/RackMultipart20191126-49673-eixlbo.jpeg>, @original_filename="images (1).jpeg", @content_type="image/jpeg", @headers="Content-Disposition: form-data; name=\"news[galleries_attributes][0][images][]\"; filename=\"images (1).jpeg\"\r\nContent-Type: image/jpeg\r\n">, #<ActionDispatch::Http::UploadedFile:0x00007fadc69af418 @tempfile=#<Tempfile:/var/folders/tl/3p_457555lz9zy3mv_5kpnqh0000gn/T/RackMultipart20191126-49673-1bshoh.jpeg>, @original_filename="images (2).jpeg", @content_type="image/jpeg", @headers="Content-Disposition: form-data; name=\"news[galleries_attributes][0][images][]\"; filename=\"images (2).jpeg\"\r\nContent-Type: image/jpeg\r\n">, #<ActionDispatch::Http::UploadedFile:0x00007fadc69af3c8 @tempfile=#<Tempfile:/var/folders/tl/3p_457555lz9zy3mv_5kpnqh0000gn/T/RackMultipart20191126-49673-68e2np.jpeg>, @original_filename="images (3).jpeg", @content_type="image/jpeg", @headers="Content-Disposition: form-data; name=\"news[galleries_attributes][0][images][]\"; filename=\"images (3).jpeg\"\r\nContent-Type: image/jpeg\r\n">, #<ActionDispatch::Http::UploadedFile:0x00007fadc69af378 @tempfile=#<Tempfile:/var/folders/tl/3p_457555lz9zy3mv_5kpnqh0000gn/T/RackMultipart20191126-49673-qg3t8m.jpeg>, @original_filename="images (4).jpeg", @content_type="image/jpeg", @headers="Content-Disposition: form-data; name=\"news[galleries_attributes][0][images][]\"; filename=\"images (4).jpeg\"\r\nContent-Type: image/jpeg\r\n">]} permitted: false>
````

後來發現，如果以圖庫的概念來思考，若在圖庫的某一筆存進多張照片的話，就不符合需求。所以圖庫的每一筆都只要是一張照片！

參考資料：https://learn.co/lessons/forms-and-basic-associations-rails

參考資料：https://rails.ruby.tw/nested_model_forms.html

參考資料：https://pjchender.github.io/2018/06/04/rails-nested-model/

#### 多型關聯

參考資料：https://ihower.tw/rails/activerecord-relationships.html

有趣的例子，有空可以實作：https://6ftdan.com/allyourdev/2016/03/22/rails-has-one-through-polymorphic-relation/

carrierwave + polymorphic：https://gist.github.com/mhenrixon/978371/c2d1298e9e738849c725efa5ce698cbba0b9e065

#### CKeditor

開啟範例專案：https://github.com/z1z1z1joey/ckeditor

環境：ruby / rails ->  2.5.1 / 5.1.5 (r515)，改動的地方為本來使用sqlite3，改為使用mysql2

ckeditor 遇到的問題：https://github.com/galetahub/ckeditor/issues/829。目前還是將就將 ckeditor 退到 4.2.4 版

裝好之後，卡在一個地方，就是要如何關聯 ckeditor 跟 news，以下連結為ckeditor內部的controller

ckeditor/pictures_controller.rb：https://github.com/galetahub/ckeditor/blob/master/app/controllers/ckeditor/pictures_controller.rb

ckeditor/application_controller.rb : https://github.com/galetahub/ckeditor/blob/master/app/controllers/ckeditor/application_controller.rb

以下連結看起來好像有答案：

- http://hk.voidcc.com/question/p-modepnzp-yo.html

- https://stackoverflow.com/questions/16002849/cant-add-parameters-to-the-picture-model-with-the-use-of-ckeditor-paperclip-an/20025004

- http://ccaloha.cc/blog/2014/12/10/howto-setup-ckeditor-upload-picture-to-a-special-folder-in-ruby-on-rails/

- 與設定有關：https://medium.com/ruby-on-rails-web-application-development/how-to-upload-images-with-ckeditor-rubyonrails-cms-uploads-tutorial-a69874724d9b

- 與config.js可以搭配：

  <%= form.input :content, :as => :ckeditor, :input_html => { :ckeditor => {:toolbar => 'Full'} } %>其中{:toolbar => 'Full'} 也可以設為{:toolbar => 'mini'}。

#### 限制圖片容量上限、圖片大小

#### 跨網域資源共享

gem：https://github.com/cyu/rack-cors

#### 遇到問題

- 在Ruby換一個版本號，Rubocop就壞掉了

  ````txt
  Error: configuration for Syntax cop found in .rubocop.yml It's not possible to disable this cop.
  ````

- 不知道要怎麼在 simple_form_for 使用上傳多張照片

  解決方法：

  ````ruby
  = f.input :pics, as: :file, input_html: { multiple: true }
  ````

- 如何在 ckeditor 新增上傳圖片功能

  - 目前 news 使用肥貓載入ckeditor的方式載入
  - 目前 photo 使用ckeditor gem

- 2019.11.28 進度和遇到的問題

  - 早上的時候練習將上傳照片與訊息加上多型，很快就成功了，後來剩下的時間就開始想辦法在ckeditor的圖庫多型的欄位填上東西，不過一直到下班之前都沒有成功。目前已經找到可以填進去的點，不過卡在還沒有辦法從 view 傳進去。原本想在ckeditor的model用before_save的回呼攔截，但卻也攔不到？
  
  - 發現圖庫的照片只會增加不會減少。若按上傳圖片的icon，將圖片上傳到資料庫以後，ckeditor就會多一張照片，之後就算在ckeditor把它刪除，也只是把與圖片相連的url刪掉，並沒有真的刪掉照片，所以可能還要想想要怎麼刪除照片。
  
  - 在ckeditor的model設定回呼的時候，程式停不下來，所以如果要做ckeditor的圖片驗證時，不知道怎麼辦？
  
  - 目前想要用monkey patch 覆蓋 Ckeditor::PicturesController 的 create方法，目前已經確定可以，但現在問題卡在當要將圖片上傳到資料庫的時候，還沒有辦法將路徑指向 controller。
  
  - 今天在看 paperclip, carrierwave 還有 rails 5.2 的 active storage 的優劣比較
  
  - carrierwave 現在還沒有找到驗證圖片大小的方法
  
  - 找到關鍵字：query_params
  
    在 pics_ocntroller 下 request.query_parameters 就會看到query進去的三個參數
  
    相關連結：https://stackoverflow.com/questions/2695538/add-querystring-parameters-to-link-to
  
    相關連結：https://stackoverflow.com/questions/2772778/parse-a-string-as-if-it-were-a-querystring-in-ruby-on-rails
  
    相關連結：https://stackoverflow.com/questions/13488358/ckeditor-in-rails-3

### 其他

- <%==  %>：https://stackoverflow.com/questions/4251284/raw-vs-html-safe-vs-h-to-unescape-html

  在裡面放javascript竟然可以執行

  ````javascript
  <%== "<script>alert('Hello!')</script>" %>
  ````

- 印萱跟我分享好用的ckeditor：

  https://www.responsivefilemanager.com/index.php#sthash.vcRyV5UI.dpbs

- update method 的不同之處：[https://blog.niclin.tw/2017/03/08/%E9%97%9C%E6%96%BC-update-method-%E7%9A%84%E4%B8%8D%E5%90%8C%E4%B9%8B%E8%99%95/](https://blog.niclin.tw/2017/03/08/關於-update-method-的不同之處/)

- 像是如果存進db失敗的話，可以寫一個after_initialize, after_rollback, before_save 的 block 查看

  參考資料：[https://rails.ruby.tw/active_record_callbacks.html#after-initialize-%E8%88%87-after-find](https://rails.ruby.tw/active_record_callbacks.html#after-initialize-與-after-find)

- paperclip和carrierwave的比較：https://stackoverflow.com/questions/7419731/rails-3-paperclip-vs-carrierwave-vs-dragonfly-vs-attachment-fu

  評論1.

   I've used both Paperclip and Carrierwave, and if I were you, I'd go for Carrierwave. It's a lot more flexible. I also love the fact that it doesnt clutter your models with configuration. You can define uploader classes instead. It allows you to easily reuse, extend etc your upload configuration.

  Did you watch the Carrierwave railscast? http://railscasts.com/episodes/253-carrierwave-file-uploads

  Paperclip is not a bad choice though, it's been the "best" choice for a long time. But Carrierwave definitely seems like the new generation ;)

  評論2.
  I have used CarrierWave and after some hours of frustration I'm switching to Paperclip.

  Here are the problems I have seen with CarrierWave:

  - You can't validate file size. There is a wiki article that explains how to do it, but it does not work.
  - Integrity validations do not work when using MiniMagick (very convenient if you are concerned about RAM usage). You can upload a corrupted image file and CarrierWave will throw an error at first, but the next time will swallow it.
  - You can't delete the original file. You can instead resize it, compress, etc. There is a wiki article explaining how to do it, but again it does not work.
  - It depends on external libraries such as RMagick or MiniMagick. Paperclip works directly with the `convert` command line (ImageMagick). So, if you have problems with Minimagick (I had), you will lose hours diving in Google searches. Both RMagick and Minimagick are abandoned at the time of this writing (I contacted the author of Minimagic, no response).
  - It needs some configuration files. This is seen as an advantage, but I don't like having single configuration files around my project just for one gem. Configuration in the model seems more natural to me. This is a matter of personal taste anyway.
  - If you find some bug and report it, the dev team is really absent and busy. They will tell you to fix bugs yourself. It seems like a personal project that is improved in spare time. For me it's not valid for a professional project with deadlines.

### Day Off

假日的時候研究active_storage與ckeditor如何配合，在設定方面就花了不少時間。

上班時使用rails版本為5.1.6，ckeditor版本為4.2.4，並使用carrierwave掛載圖片；在自己的side project中所使用的rails版本為5.2.3，ckeditor版本為5.0.0，並使用原生的active_storage掛載圖片。

active_storage很棒的地方，在於當專案使用上圖功能，它會導入active_storage存圖的地方，相當於圖庫的概念，並且自動附帶多型，使用者只要寫has_one_attached 或者 has_many_attached，active_storage 就會自動在record_type寫上model的名字，並且配好相對應的id，像是如果存在message，型別就為Message，而如果存在ckeditor，則型別為ckeditor::asset，不用像carrierwave，還要自己建立關聯。

### 可以實作的文章

- https://uploadcare.com/docs/guides/ruby_rails/
- https://andyyou.github.io/2018/06/26/using-active-storage/





