# The 13th Week

以下為到職第13週的紀錄，這週重點：

- 升級 rails 至 5.2.3
- 使用active storage
- 改寫devise的view
- 新增按讚、留言的model

### 升級 rails 至 5.2.3

Gemfile

````ruby
# gem 'rails', '~> 5.1.6'
gem 'rails', '~> 5.2.3'

# ckeditor 搭配 carrierwave
# gem 'ckeditor', '4.2.4'
# gem 'carrierwave'
# gem 'mini_magick'

# ckeditor 搭配 active storage
gem 'ckeditor', github: 'galetahub/ckeditor'
gem 'mini_magick'
````

console

````shell
# 升級
> rails app:update
> bundle update

# 安裝 active_storage
> rails active_storage:install
Copied migration 20191202025814_create_active_storage_tables.active_storage.rb from active_storage

> rails db:migrate
== 20191202025814 CreateActiveStorageTables: migrating ========================
-- create_table(:active_storage_blobs)
   -> 0.0675s
-- create_table(:active_storage_attachments)
   -> 0.0113s
== 20191202025814 CreateActiveStorageTables: migrated (0.0872s) ===============
````

使用 active_storage 和 ckeditor

````shell
> rails generate ckeditor:install --orm=active_record --backend=active_storage --force

Running via Spring preloader in process 84256
   identical  config/initializers/ckeditor.rb
       route  mount Ckeditor::Engine => '/ckeditor'
   identical  app/models/ckeditor/asset.rb
   identical  app/models/ckeditor/picture.rb
   identical  app/models/ckeditor/attachment_file.rb
      remove  db/migrate/20191119080945_create_ckeditor_assets.rb
      create  db/migrate/20191202032542_create_ckeditor_assets.rb
      
> rails db:drop db:create db:migrate 
````

早上升級版本成功，不過到下午寫圖片驗證的時候，沒有辦法驗證ckeditor裡面的圖片。我在寫驗證時，它跟我說：「 storage_data can't be blank ! 」

解決方法

````ruby
class Ckeditor::Picture < Ckeditor::Asset
  ...
  before_save :validate_upload_image

  WIDTH = 80
  HEIGHT = 80
  FILE_SIZE_LIMIT = 10000

  ...

  private

  def validate_upload_image
    if storage_data.blob.byte_size > FILE_SIZE_LIMIT
      halt_callback('is too big')
    elsif !storage_data.blob.content_type.starts_with?('image/')
      halt_callback('is in wrong format')
    else
      dimension = ActiveStorage::Analyzer::ImageAnalyzer.new(blob).metadata
      width = dimension.dig(:width)
      height = dimension.dig(:height)
      halt_callback('is too big in dimension') if width > WIDTH || height > HEIGHT
    end
  end

  def halt_callback(error_message)
    storage_data.purge
    errors.add(:storage_data, error_message)
    throw(:abort)
  end
end
````

參考資料

- rails 4 如何在before_save終止存檔：https://stackoverflow.com/questions/23837573/rails-4-how-to-cancel-save-on-a-before-save-callback
- rails 5 如何在before_save終止存檔：https://blog.bigbinary.com/2016/02/13/rails-5-does-not-halt-callback-chain-when-false-is-returned.html
- rails 5 如何在before_save終止存檔（官方文檔）：https://guides.rubyonrails.org/active_record_callbacks.html#halting-execution
- 如何限制圖片容量大小：https://stackoverflow.com/questions/48158770/activestorage-file-attachment-validation
- 取得照片長寬：https://api.rubyonrails.org/classes/ActiveStorage/Analyzer/ImageAnalyzer.html
- 刪除檔案-使用purge：https://pjchender.github.io/2018/05/25/rails-active-storage-overview/
- 檔案改名：https://medium.com/fiatinsight/how-to-change-a-filename-in-rails-active-storage-f3e4f26f427e

### Devise

1. before action：https://fullstack.qzy.camp/posts/675

2. 如何讓所有的 devise 導向客製化的 view 畫面，而不是 gem 提供的畫面

    devise 所有 model 都使用相同的 view ，幸運的是 devise 提供一個簡單的方法來自訂這些 views
   您只需要在 `config/initializers/devise.rb` 中設定 `config.scoped_views = true`

   參考資料：https://andyyou.github.io/2015/04/04/devise/

3. 如何在新增會員的畫面客製化參數

   ````ruby
   class ApplicationController < ActionController::Base
     before_action :configure_permitted_parameters, if: :devise_controller?
   
     protected
   
     def configure_permitted_parameters
       devise_parameter_sanitizer.permit(:sign_up, keys: [:nickname])
       devise_parameter_sanitizer.permit(:account_update, keys: [:nickname])
     end
   end
   ````

   ````ruby
   class ApplicationController < ActionController::Base
     include Error::Handler
     include Helper::Site
     include Pagy::Backend
     include ApplicationHelper
   
     protect_from_forgery with: :exception
   
     before_action :configure_permitted_parameters, if: :devise_controller?
   
     protected
   
     # 如何客製化自己的參數
     def configure_permitted_parameters
       added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
       devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
       devise_parameter_sanitizer.permit :account_update, keys: added_attrs
     end
   end
   ````

   參考資料：

   - https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign-in-using-their-username-or-email-address
   - https://web-crunch.com/devise-login-with-username-email/
   - https://andyyou.github.io/2015/04/04/devise/
   - 若有覆寫「登入」控制器的需求時可以用：[Override devise registrations controller](https://stackoverflow.com/questions/3546289/override-devise-registrations-controller)
   - devise I18n：https://github.com/tigrish/devise-i18n

4. devise bootstrap views：https://www.rubydoc.info/gems/devise-bootstrap-views/1.1.0

### 新增留言

*model*

````shell
# 以往的寫法
rails g model Comment content user:reference news:reference

# 多型的寫法
rails g model Comment content:text commentable:references{polymorphic} user:references
````

如何加入 index：https://mauricio.github.io/2008/09/27/handling-database-indexes-for-rails-polymorphic-associations.html

rails console 寫入一筆資料

````ruby
# 不好寫法
a.comments.create(content: "Hello", user_id: 2)

# 正規寫法
user = User.first
a.comments.create(content: "Hello", user: user)
````

*controller -> 暫時不需要寫，保留彈性！*

### 新增按讚

*model*

````ruby
# 多型的寫法
rails g model Like like:integer likes_count:integer likeable:references{polymorphic} user:references
````

關於按讚所需的關鍵字為：`計數快取 Counter Cache`、`callback`

My suggestion would be to manually increment or decrement your counter cache in an after_commit callback so that you can test if the record was persisted and it updates outside of the transaction. This is because it will make your code more explicit, and less mysterious on how and when the cache is updated or invalidated.

Also manually updating the cache gives you extra flexibility if for example you wanted to give some users more authority when they agree or disagree with a comment (e.g. karma systems).

參考資料：https://stackoverflow.com/questions/9809508/polymorphic-relationships-and-counter-cache

從 model 找尋欄位是否存在

````ruby
self.new.respond_to? :click
self.method_defined? :click
self.column_names.include? 'click'
````

最後結果：

````ruby
class Like < ApplicationRecord
  belongs_to :likeable, polymorphic: true
  belongs_to :user

  after_save -> { click_counter(true) }
  after_destroy -> { click_counter(false) }

  def click_counter(operation)
    # like
    click_proc('click', 'positive?', operation)
    # dislike
    click_proc('dislike', 'negative?', operation)
  end

  private

  def click_proc(existing_col, action, operation)
    if ( likeable.respond_to? existing_col ) && self.like.send(action)
      case operation
      when true then likeable.increment!(existing_col.to_sym)
      when false then likeable.decrement!(existing_col.to_sym)
      end
    end
  end
end
````

### Keynote

利用 highlight 貼程式碼

````shell
# 複製為rtf格式
highlight -l -O rtf code.js | pbcopy

# 如果有中文，記得貼 -u 'utf-8'
highlight -u 'utf-8' -O rtf code.js | pbcopy
````

參考資料

- https://zju.date/insert-code-in-ppt/
- https://gist.github.com/jimbojsb/1630790

在找資料的時候，發現一個可以貼 code 的地方：https://gist.github.com/ChenHanTing

### 其他

- Explain：[https://www.scrivinor.com/article/rails-sql-db-%E7%B0%A1%E6%98%93%E6%95%88%E8%83%BD%E5%88%A4%E6%96%B7?locale=zh-TW](https://www.scrivinor.com/article/rails-sql-db-簡易效能判斷?locale=zh-TW)
- 動態寫法：https://blog.chh.tw/posts/dynamic-method/

### 作業

1. View
2. SQL
3. RESTful api

