





# The 14th Week

以下為到職第14週的紀錄，這週重點：

- 理解 form_for 和 form 所提供的功能
- 了解csrf token的存在
- 了解save跟update是透過表單觸發submit後開始執行
- 使用datetime picker
- bootstrap card 排版（學習如何看前端的官方文件）
- 了解 Ohm如何使用 devise 打 token 驗證按讚跟留言
- 撰寫留言跟按讚的controller

### 範例- BMI

routes.rb

```ruby
get 'bmi', to: "bmi#index"
post 'bmi/result', to: "bmi#result"
```

首先先看bmi表單

````erb
<h1>BMI 計算機</h1>

<%= form_tag '/bmi/result' do %>
  身高：<%= text_field_tag 'body_height' %> 公分<br />
  體重：<%= text_field_tag 'body_weight' %> 公斤<br />
  <%= submit_tag "開始計算" %>
<% end %>

<p id="result"></p>
````

對應的html

````html
<body>
<h1>BMI 計算機</h1>

<form action="/bmi/result" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="✓"><input type="hidden" name="authenticity_token" value="FQyB/Y8PgVFzKx57Wkl7NPNSOPUPL1C1/4JBAYSxGKsN0llvJsfbINLZuY5deMRFJwH+FLwt9OlVDhQWeRGE3w==">
  身高：<input type="text" name="body_height" id="body_height"> 公分<br>
  體重：<input type="text" name="body_weight" id="body_weight"> 公斤<br>
  <input type="submit" name="commit" value="開始計算" data-disable-with="開始計算">
</form>

<p id="result"></p>
</body>
````

 `authenticity_token` 的隱藏 input 標籤，不只內容看起來像是亂碼，而且每次重新整理又會得到不一樣的值，這個是做什麼用的呢？ 如果網站是用 Rails 開發的，Rails 預設在處理表單的時候會檢查這個 `authenticity_token` 是不是由本站所產生的，如果沒有這個欄位，或是這個欄位的值經 Rails 核對後發現並不是本身所產生，就會出現錯誤訊息：ActionController::InvalidAuthenticityToken

`form_tag` 或是 `form_for`，在產生 `<form>` 標籤的時候都會自動幫你加上並產生 `authenticity_token` 的欄位，確保比較不會太容易被有心人士所破壞。所以結論： rails 的表單傳送理論上可以用 postman 代替，但因為有  `authenticity_token` 的存在，所以無法。只能乖乖打api囉！

參考資料：https://railsbook.tw/chapters/12-controllers.html

順便學form_tag與form_for的異同之處：https://motion-express.com/blog/rails-form-tag-form-for

### 使用範例- Name

routes.rb

````ruby
get 'name', to: "name#index"
post 'name/result', to: "name#result"
````

index.html.erb

````erb
<h2>HTML Forms</h2>

<form action="/name/result" accept-charset="UTF-8" method="post">
  First name:
  <input type="text" name="firstname" value="Chen"><br>
  Last name:
  <input type="text" name="lastname" value="Han-Ting"><br><br>
  <input type="submit" value="Submit">
</form>
````

畫面

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191210140405472.png" alt="image-20191210140405472" style="zoom:50%;" />

controller

````ruby
class NameController < ApplicationController
  # 跳過ActionController::InvalidAuthenticityToken
  skip_before_action :verify_authenticity_token

  def index
  end

  def result
    @name = "#{params[:firstname]}, #{params[:lastname]}"
  end
end
````

postman

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191210140542133.png" alt="image-20191210140542133" style="zoom:50%;" />

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191210140607636.png" alt="image-20191210140607636" style="zoom:50%;" />

參考資料：https://www.w3schools.com/html/html_forms.asp

參考資料：https://ihower.tw/rails/actioncontroller.html



除了上述使用post的方式傳值，也可以使用get的方式

index.html.erb

````erb
<form action="/name/result" accept-charset="UTF-8" method="get">
  Nick name:
  <input type="text" name="nickname" value="Han"><br><br>
  <input type="submit" value="Submit">
</form>
````

controller

````ruby
class NameController < ApplicationController
  # 跳過ActionController::InvalidAuthenticityToken
  skip_before_action :verify_authenticity_token

  def index
  end

  def result
    @nickname = "#{params[:nickname]}"
  end
end
````

### 威博資訊站- 新聞：留言部份

從以上的範例可以看到，不管get和post，只和`result`有關，與`index`並沒有任何關聯。依照這個邏輯，我們知道在comments_controller.rb裡面只需要創 create, update, destroy 方法就足夠了。（不過後來又得知，只需要`create`便足夠）

routes.rb

````shell
> rails routes -g comments

     Prefix Verb   URI Pattern                Controller#Action
v1_comments POST   /v1/comments(.:format)     api/v1/comments#create 
 v1_comment PATCH  /v1/comments/:id(.:format) api/v1/comments#update 
            PUT    /v1/comments/:id(.:format) api/v1/comments#update 
            DELETE /v1/comments/:id(.:format) api/v1/comments#destroy 
````

Rails 在 routes 的慣例中，可以看到慣例中的action對應的不同verb，這是 Rails 給我們的福利。

#### create

![image-20191210153913832](/Users/hanting/Library/Application Support/typora-user-images/image-20191210153913832.png)

![image-20191210153949361](/Users/hanting/Library/Application Support/typora-user-images/image-20191210153949361.png)

#### update

- PUT 定義是 Replace (Create or Update)，例如`PUT /items/1`的意思是替換`/items/1`，如果已經存在就替換，沒有就新增。PUT 必須包含`items/1`的所有屬性資料。
- 如果只是為了更新其中一個屬性，就需要重傳所有`items/1`的屬性也太浪費頻寬了，所以後來又有新的 [PATCH Method](http://tools.ietf.org/html/rfc5789) 標準，可以用來做部分更新(Partial Update)。
- 參考網址：https://ihower.tw/blog/archives/6483

![image-20191210154931835](/Users/hanting/Library/Application Support/typora-user-images/image-20191210154931835.png)

mysql成功更新第一筆紀錄

![image-20191210155052723](/Users/hanting/Library/Application Support/typora-user-images/image-20191210155052723.png)

#### destroy

![image-20191210155236337](/Users/hanting/Library/Application Support/typora-user-images/image-20191210155236337.png)

mysql成功刪除第一筆紀錄

![image-20191210155343285](/Users/hanting/Library/Application Support/typora-user-images/image-20191210155343285.png)

#### controller

````ruby
class Api::V1::CommentsController < Api::V1::BaseController
  # 跳過ActionController::InvalidAuthenticityToken
  skip_before_action :verify_authenticity_token
  before_action :find_comments, only: %i[update destroy]

  def create
    @comment = Comment.new(comments_params)
    @comment.save
  end

  def update
    @comment.update(comments_params)
  end

  def destroy
    @comment.destroy
  end

  private

  def find_comments
    @comment = Comment.find_by(id: params[:id])
  end

  def comments_params
    params.require(:comments).permit(:content, :commentable_type, :commentable_id, :user_id)
  end
end
````

其中`skip_before_action :verify_authenticity_token` 是特別為 form 所設計帶的token，防止有心的使用者模擬類似的方式灌水等等

後台的留言者留言時需帶一組token辨識留言者是否為前台的使用者，後面會講到。

### 威博資訊站- 新聞：按讚部份

與留言部份幾乎相同

### Datetime Picker

1. (bootstrap 3) simple_form datetime 工具：https://datetime-picker-input.herokuapp.com/
2. (bootstrap 4) simple_form datetime 工具：https://qiita.com/tsunemiso/items/5c20213791ced02b43c0
3. How to disable datetime picker：https://stackoverflow.com/questions/44057022/how-to-disable-bootstrap-datetimepicker

### Card

1. Bulma Example：https://github.com/xiaoweiruby/dribbble_clone

2. Bootstrap Reference：https://getbootstrap.com/docs/4.3/components/card/

3. 要怎麼設定 bootstrap 一排只有3個：

   目前出現`Undefined mixin 'media-breakpoint-only'`尚未找到解法

   要怎麼做到很簡單，只要在每一個each給上bootstrap的RWD就行了

   參考資料：https://getbootstrap.com/docs/4.3/layout/grid/

4. margin 間距：https://www.w3schools.com/css/css_margin.asp

   以下圖例為從間距0 -> 間距20

   ![image-20191211180733843](/Users/hanting/Library/Application Support/typora-user-images/image-20191211180733843.png)

   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191211180703750.png" alt="image-20191211180703750" style="zoom:50%;" />

### Default Values

1. https://stackoverflow.com/questions/19029129/default-value-for-input-with-simple-form
2. https://stackoverflow.com/questions/41556889/what-does-f-object-do-in-the-rails-form-builder

#### 登出時，會遇到以下問題

No template found for Auth::SessionsController#destroy, rendering head :no_content
Completed 204 No Content in 2458ms (ActiveRecord: 0.0ms)

那是因為我在routes.rb進行以下的改動後，打api之後會有問題

````ruby
# devise_for :users, module: 'auth'
devise_for :users, :controllers => {:registrations => "admin/registrations"}
````

另外可以了解一下，`module: 'auth'`的意思為去掉auth的前綴

auth/sessions_controller.rb是Ohm寫的程式碼。因為我們不能對routes.rb做改動，取而代之的是在 auth/sessions_controller.rb 改成如下，在else的地方使用`super`繼承原本的程式碼。下面的程式碼意思為，如果走api模式走Ohm的程式碼，其他地方走原本的程式碼。

````ruby
class Auth::SessionsController < Devise::SessionsController
  include Error::Handler
  include Helper::Site

  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token, only: %i[create destroy]
  skip_before_action :verify_signed_out_user

  before_action :param_permit
  respond_to :json, if: -> { request.format.json? }

  def new
    super
  end


  def create
    user = User.find_for_database_authentication login: @params[:login]

    raise Error::SysErr[:login_user_or_password] if user.blank? || !user.valid_password?(@params[:password])

    if site_api?
      render json: {
        info: {
          name: user.username,
          # auth_token: user.build_auth_token(request.headers['Auth-Token']),
          auth_token: user.build_auth_token,
        },
      }
    elsif site_admin?
      super
    end

  end

  def destroy
    Rails.cache.delete(request.headers['Auth-Token'])

    if site_api?
      render json: {
        info: {
          sign_out: 'ok',
        }
      }
    elsif site_admin?
      super
    end
  end

  private

  def param_permit
    @params = params.fetch(:user, {}).permit(:login, :password)
  end
end
````

這是Ohm定義的方法，其中`site_admin?`與`site_api?`，在 helper/site.rb 裡面有定義

````ruby
module Helper::Site
  def site_admin?
    request.subdomain == 'admin'
  end

  def site_api?
    request.subdomain == 'api'
  end
end
````

#### 了解token

controllers/api/api_controller.rb

````ruby
class Api::ApiController < BaseController
  include Helper::Render

  def authenticate_user!
    auth_token = Rails.cache.read(request.headers['Auth-Token']) rescue nil
    @current_user ||= User.find_by(id: auth_token&.dig(:user_id))
    raise Error::SysErr[:authenticate_user] if @current_user.blank?
  end

  def respond_with_data(*args)
    res_data = args.each_with_object(Tool::deep_hash) do |arg, h|
      h.deep_merge!(arg.kind_of?(Symbol) ? send(arg) : arg)
    end

    respond_with res_data
  end

  def res_info
    site = Site.find_by(domain: request.domain)&.serializable_hash(only: %i(name domain theme locale locales]))

    {
      info: {
        site: site,
      }
    }
  end
end
````

`request.headers['Auth-Token']`指的是從標頭檔帶進去的Auth-Token，

`Rails.cache`是快取，有`read`跟`write`兩個動作

`request` 常見的幾個方法：https://api.rubyonrails.org/classes/ActionDispatch/Request.html

原本的`authenticate_user!`

````ruby
def authenticate_#{mapping}!(opts={})
  opts[:scope] = :#{mapping}
  warden.authenticate!(opts) if !devise_controller? || opts.delete(:force)
end
````

````ruby
def authenticate_user!(opts={})
  opts[:scope] = :user
  warden.authenticate!(opts) if !devise_controller? || opts.delete(:force)
end
````

`Devise` uses [Warden](https://github.com/hassox/warden) for authentication. And in order to use it `Devise` provides own authentication strategies implementing `authenticate!` method. This is what you need. You already have the first part of the code (from the link in your question)

參考資料：https://stackoverflow.com/questions/32237346/what-is-code-for-devise-authenticate-user-after-generated-for-user

model/user.rb

````ruby
class User  
  ...
  def build_auth_token(token = nil)
    token = Devise.friendly_token(32) if token.blank?

    Rails.cache.write(
      token,{ user_id: id,},
      expires_in: 6.hours
    )

    token
  end
end
````

`Rails.cache.write(a, b)`中， `a`為key， `b`為value，以這邊為例，key為token，value為user_id，這組 token 會在六小時過後到期。

有關於`read`跟 `write`的更詳細說明在：https://ihower.tw/rails/caching.html。

另外還有`fetch`的使用方式：https://apidock.com/ruby/Hash/fetch

Rails 有內建的的快取，除此之外也可以用Redis的快取。不管如何，Raiis都提供給我們存取cache的語法，就像ORM一樣，快取部分只要使用Rails提供的指令即可。

#### 利用`token`將comment, like 寫進去

帳號密碼取得`token`

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191212184741759.png" alt="image-20191212184741759" style="zoom:80%;" />

複製`token`到按讚的Auth_Token

![image-20191212184848624](/Users/hanting/Library/Application Support/typora-user-images/image-20191212184848624.png)

`body`內容

![image-20191212184910290](/Users/hanting/Library/Application Support/typora-user-images/image-20191212184910290.png)

#### 錯誤訊息

controllers/concerns/error/sys_err.rb

````ruby
module Error
  class SysErr < StandardError
    attr_reader :error, :message, :status

    class << self
      def [](init, status = 400)

        if init.kind_of?(Symbol)
          error = init
          message = I18n.t("error.#{init}")
        else
          error = :server_error
          message = init
          status = 500
        end

        new(error: error, message: message, status: status)
      end
    end

    def initialize(error: nil, message: 'Something went wrong !!!', status: 400)
      @error = error
      @message = message
      @status = status
    end

    def to_res_json
      {
        json: {
          error: error,
          message: message,
          status: status,
        },
        status: (status.presence || 200),
      }
    end
  end
end
````

使用方式

````ruby
raise Error::SysErr[@comment.errors.messages]
````

#### 如何設定語系

參考資料：http://www.xyzpub.com/en/ruby-on-rails/3.2/i18n_mehrsprachige_rails_applikation.html

#### Scope

scope 可以使用組合技，以下為原本的程式碼

`news.rb`

````ruby
class News < ApplicationRecord
  include Helper
  ...
    
  scope :publish_articles, -> { where("publish_at != null OR publish_at < ?", Time.current).order("id desc") }
  scope :publish_article, -> (id){ where("id = ? AND ( publish_at != null OR publish_at < ? )", id, Time.current) }
  
  ...
end
````

`news_controller.rb`

````ruby
class Api::V1::NewsController < Api::V1::BaseController
  include Pagy::Backend

  before_action :find_news, only: %i[show]
  after_action { pagy_headers_merge(@pagy) if @pagy }

  def index
    @pagy, @records = pagy(News.all.publish_articles, items: 3)
    render json: { page: @pagy, data: @records }
  end

  def show
    raise Error::SysErr["Not Found"] if @article

    render json: @article
  end

  private

  def find_news
    @article = News.publish_article(params[:id])
  end
end
````

可以改成以下寫法

`news.rb`

```ruby
class News < ApplicationRecord
  include Helper
  ...
    
  scope :publish_articles, -> { where("publish_at != null OR publish_at < ?", Time.current).order("id desc") }
  scope :publish_article, -> (id){ where("id = ? AND ( publish_at != null OR publish_at < ? )", id, Time.current) }
  
  ...
end
```

`news_controller.rb`

```ruby
class Api::V1::NewsController < Api::V1::BaseController
  include Pagy::Backend

  before_action :find_news, only: %i[show]
  after_action { pagy_headers_merge(@pagy) if @pagy }

  def index
    @pagy, @records = pagy(News.all.publish_articles, items: 3)
    render json: { page: @pagy, data: @records }
  end

  def show
    raise Error::SysErr["Not Found"] if @article

    render json: @article
  end

  private

  def find_news
    @article = News.find_by(id: params[:id]).published
  end
end
```

SQL語法：

````sql
SELECT `news`.* FROM `news` WHERE (publish_at != null OR publish_at < '2019-12-13 10:38:27.338859') AND (1) ORDER BY id desc

SELECT `news`.* FROM `news` WHERE (publish_at != null OR publish_at < '2019-12-13 10:38:37.571971') AND `news`.`id` = 1 ORDER BY id desc
````

由此可以推斷我的ORM寫錯了

### Model

以下為按讚的 model  `models/likes.rb`(15th會大改這邊)

````ruby
class Like < ApplicationRecord
  belongs_to :expressable, polymorphic: true
  belongs_to :user

  after_save :multiple_click
  after_save :like_counter
  after_destroy :dislike_counter

  # 數讚
  def like_counter
    if expressable.respond_to?('click') && self.expression == 'like'
      expressable.increment!('click'.to_sym)
    end
  end

  # 數噓
  def dislike_counter
    if expressable.respond_to?('dislike') && expression == 'dislike'
      expressable.decrement!('dislike'.to_sym)
    end
  end

  # 重複按讚
  # 如果按讚又按噓，則刪掉先前的讚
  # 如果按讚又按讚，兩筆讚都刪除
  def multiple_click
    click_data = expressable.likes.where(user_id: user.id)
    if click_data.count > 1
      first = click_data.first
      last = click_data.last
      click_data.delete_all if last.expression.eql? first.expression

      first.delete
    end
  end
end
````

這樣寫，是因為在before_save寫的話，若 rollback 則全部不算。當然還有可以避開rollback的作法，這只是一種寫法而已，我想如果要寫在before_save的話可以這樣寫（沒試過）：

````ruby
class Like < ApplicationRecord
  ...
  after_save :multiple_click
  ...

  def multiple_click
    click_data = expressable.likes.where(user_id: user.id)
    if click_data.count > 0
      first = click_data.first
      first.delete     # 需要無視rollback的寫法
      
      error.add("...") # 錯誤訊息
      throw(:abort) if expression.eql? first.expression
    end
  end
end
````

### SQL

這邊要講`ruby`對應的`sql`語句：Rails 提供了許多 `to_sql`和 `explain`的語法

````ruby
likes.where("expression = 'like'").to_sql
````

````sql
"SELECT `likes`.* FROM `likes` WHERE `likes`.`expressable_id` = 1 AND `likes`.`expressable_type` = 'News' AND (expression = 'like')"
````

create

````ruby
Comment.create(content: "First Content", commentable: LabForum.first, user: User.first)
````

````sql
LabForum Load (0.4ms)  SELECT  `lab_forums`.* FROM `lab_forums` ORDER BY `lab_forums`.`id` ASC LIMIT 1

User Load (0.4ms)  SELECT  `users`.* FROM `users` ORDER BY `users`.`id` ASC LIMIT 1

(0.2ms)  BEGIN
Comment Create (0.9ms)  INSERT INTO `comments` (`content`, `commentable_type`, `commentable_id`, `user_id`, `created_at`, `updated_at`) VALUES ('First Content', 'LabForum', 1, 1, '2019-12-14 12:33:33', '2019-12-14 12:33:33')
(1.8ms)  COMMIT
````

### 按讚功能

model

````shell
rails g model Like expression:integer user:references likeable:references{polymorphic}
````

### CSRF Token

參考文章：https://blog.techbridge.cc/2017/02/25/csrf-introduction/

### PNG => JPG

好用網站：https://png2jpg.com/