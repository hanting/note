# The 15th Week

以下為到職第15週的紀錄，這週重點：

- Side Project
  - 按讚功能
  - API
- Drifting Ruby
- 威博資訊站：Coding Style
- VR 遊戲
  - 基本登入、登出、轉入、轉出、查看錢包、遊戲紀錄
  - 記得要使用測試站的Key

### 按讚的 Models 和 Controllers

models:

````ruby
class Like < ApplicationRecord
  belongs_to :expressable, polymorphic: true
  belongs_to :user

  after_save :addition_counter
  after_destroy :substraction_counter

  def addition_counter
    counter_adj(:increment!)
  end

  def substraction_counter
    counter_adj(:decrement!)
  end

  def counter_adj(method_adj)
    {click: :like, dislike: :dislike}.each do |k, v|
      expressable.send(method_adj, k) if check_respond_to?(k, v)
    end
  end

  def check_respond_to?(column_name, exp)
    expressable.respond_to?(column_name) && expression == exp.to_s
  end
end
````

controllers

````ruby
class Api::V1::LikesController < Api::V1::BaseController
  # 跳過ActionController::InvalidAuthenticityToken
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!

  def create
    clicked = current_user.likes.find_by(likes_params.except(:expression))
    like = current_user.likes.new(likes_params)

    if clicked.blank?
      raise Error::SysErr[like.errors.full_messages] if !like.save
    else
      clicked.destroy

      if (clicked.expression != like.expression)
        raise Error::SysErr[like.errors.full_messages] if !like.save  
      end
    end

    respond_success
  end
  
  private

  def likes_params
    params.require(:likes).permit(:expression, :expressable_type, :expressable_id)
  end

  def respond_success
    render json: {status: 'ok'}
  end
end
````

之前在按讚功能寫了create和destroy兩個動作，不過後來發現按讚、按噓就只會觸發一個action

`rails db:reset` 為資料庫重新遷移並rails db:seed

`create`動作本來就不需要找id，所以要注意這個慣例

`delete` 和 `destroy`的差別：Basically `destroy` runs any callbacks on the model while `delete` doesn't.

參考資料：https://stackoverflow.com/questions/22757450/difference-between-destroy-and-delete

### SQL

ruby 語法

````sql
# 不好的寫法
current_user.likes.find_by(expressable: like.expressable)

# 這一行需要改（根據SQL）
current_user.likes.find_by(likes_params) 

# 最終寫法
current_user.likes.find_by(likes_params.except(:expression))
````

對應的sql語法

````sql
SELECT  `likes`.* FROM `likes` WHERE `likes`.`user_id` = 2 AND `likes`.`expressable_type` = 'News' AND `likes`.`expressable_id` = 2 LIMIT 1

SELECT  `likes`.* FROM `likes` WHERE `likes`.`user_id` = 2 AND `likes`.`expression` = 'like' AND `likes`.`expressable_type` = 'News' AND `likes`.`expressable_id` = 2 LIMIT 1

SELECT  `likes`.* FROM `likes` WHERE `likes`.`user_id` = 2 AND `likes`.`expressable_type` = 'News' AND `likes`.`expressable_id` = 2 LIMIT 1
````

### Coding Style

1. if-else 越容易判斷的東西要越往前寫，增加效能

````ruby
def create
  like = Like.new(likes_params)
  clicked = current_user.likes.find_by(expressable: like.expressable)
  if clicked.present? && (clicked.expression.eql? like.expression)
    clicked.destroy
  elsif clicked.present?
    clicked.destroy
    like.user = current_user
    raise Error::SysErr[like.errors.full_messages] if !like.save
  else
    like.user = current_user
    raise Error::SysErr[like.errors.full_messages] if !like.save
  end

  respond_success
end
````

````ruby
def create
  like = current_user.likes.new(likes_params)
  clicked = current_user.likes.find_by(likes_params.except(:expression))

  if clicked.blank?
    raise Error::SysErr[like.errors.full_messages] if !like.save
  elsif clicked.expression.eql? like.expression
    clicked.destroy
  else
    clicked.destroy
    raise Error::SysErr[like.errors.full_messages] if !like.save
  end

  respond_success
end
````

````ruby
def create
  clicked = current_user.likes.find_by(likes_params.except(:expression))
  like = current_user.likes.new(likes_params)

  if clicked.blank?
    raise Error::SysErr[like.errors.full_messages] if !like.save
  else
    clicked.destroy

    if (clicked.expression != like.expression)
      raise Error::SysErr[like.errors.full_messages] if !like.save  
    end
  end

  respond_success
end
````

2. 重複的工作盡量不要出現

````ruby
after_save :addition_counter
after_destroy :substraction_counter

def addition_counter
  expressable.increment!('click'.to_sym) if expressable.respond_to?('click') && self.expression == 'like'
  expressable.increment!('dislike'.to_sym) if expressable.respond_to?('dislike') && expression == 'dislike'
end

def substraction_counter
  expressable.decrement!('click'.to_sym) if expressable.respond_to?('click') && self.expression == 'like'
  expressable.decrement!('dislike'.to_sym) if expressable.respond_to?('dislike') && expression == 'dislike'
end
````

`````ruby
after_save :addition_counter
after_destroy :substraction_counter

def addition_counter
  counter_adj(:increment!)
end

def substraction_counter
  counter_adj(:decrement!)
end

def counter_adj(method_adj)
  {click: :like, dislike: :dislike}.each do |k, v|
    expressable.send(method_adj, k) if check_respond_to?(k, v)
  end
end

def check_respond_to?(column_name, exp)
  expressable.respond_to?(column_name) && expression == exp.to_s
end
`````

except的使用方式：https://apidock.com/rails/Hash/except

send的使用方式：https://apidock.com/ruby/Object/send

### VR彩票

在進行VR彩票以前，要先了解Imone是什麼？

Imone 所有遊戲的相關資訊： https://docs.google.com/spreadsheets/d/1zW8D3ePmHBMLO6VtZRkyHEJIEtiiXiqSw43Oq__C3Io/edit#gid=457315406

`games_v2/imone.rb`只有重構login的部分

````ruby
def login(device, _options = {})
  @user_ip = @user.current_sign_in_ip
  params = api_params('Game/NewLaunchGame') # 手機版與桌機版參數一樣
  res = if device == 'mobile'
          api_process('Game/NewLaunchMobileGame', params)
        else
          api_process('Game/NewLaunchGame', params)
        end

  game_url = res.dig(:result, :gameurl)
  raise GamesV2::Error.new(GamesV2::Error::INVALID_GAME_LAUNCHING_URL, "Can't get the game url correctly for Imone") unless game_url.present?
  game_url
end
````



![image-20191216160239212](/Users/hanting/Library/Application Support/typora-user-images/image-20191216160239212.png)

GetBetLog 注单日志的更新延迟 (Delay Time)

基于各种原因，营运商所能够􏰁供的注单日志更新，可能会比第 2.6 节所述的(15 分钟延迟)来得更 久。
以下的列表说明相关营运商的潜在注单日志更新延迟： 503 ->VR Lottery ->15分鐘

![image-20191216171200775](/Users/hanting/Library/Application Support/typora-user-images/image-20191216171200775.png)

IMOne: ag, esportsbull, imsb, pt, sunbet < GamesV2::Imone < Games::Imone

新增Key流程：KEY管理新增遊戲key，在設定公司遊戲key新增遊戲館的key

VR彩票只要繼承 imone 的 login 就可以了

![image-20191216183111511](/Users/hanting/Library/Application Support/typora-user-images/image-20191216183111511.png)

創帳號

````ruby
user = User.find_by account:"chenhanting"
GamesV2::Vr.new(user)

user = User.find_by account:"chenhanting"
Esportsbull.new(user)
````

登入

````ruby
user = User.find_by account:"chenhanting"
Esportsbull.new(user).login('pc')

user = User.find_by account:"chenhanting"
GamesV2::Vr.new(user).login('pc')

user = User.find_by account:"dfwrgqerqee"
GamesV2::Vr.new(user).login('pc')

user = User.find_by account:"dfwbwvergwwqee"
GamesV2::Vr.new(user).login('pc')
````

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191216190847499.png" alt="image-20191216190847499" style="zoom:50%;" />

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191216190904024.png" alt="image-20191216190904024" style="zoom:50%;" />

VR彩票所支援的幣別

![image-20191217174402216](/Users/hanting/Library/Application Support/typora-user-images/image-20191217174402216.png)

目前打api的狀況：

- url: "http://imone.imaegisapi.com/Game/NewLaunchGame"
- query_params:
  {
      MerchantCode: "OcdMDMkF8z1cMZ2x92DEyWrx5jcjdvao",
      PlayerId: "DFCTvrchenhanting",
      GameCode: "VR_LOTTERY",
      Language: "EN",
      IpAddress: "127.0.0.1",
      ProductWallet: "503"
  }
- Content-Type: application/json

**Postman**

以下為VR彩票的login api（遊戲代碼錯誤）

![image-20191217104430804](/Users/hanting/Library/Application Support/typora-user-images/image-20191217104430804.png)

目前參考申博進入遊戲大廳的方式進入VR彩票的大廳

`imlotto20042`：VR彩票的大廳遊戲代碼

![image-20191217113136568](/Users/hanting/Library/Application Support/typora-user-images/image-20191217113136568.png)

出現`546`錯誤

![image-20191217113629593](/Users/hanting/Library/Application Support/typora-user-images/image-20191217113629593.png)

查看api文件的錯誤結果為遊戲不對該營運商開放，需要跟遊戲商

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191217112701569.png" alt="image-20191217112701569" style="zoom:50%;" />

沿用第4週，調用websocket的方式做以下設定

````ruby
# config/environments/development.rb
config.action_cable.url = 'ws://localhost:3000/cable'

# app/assets/javascripts/client/cable.js.erb
SocketChannel.cable = ActionCable.createConsumer('ws://' + host + '/cable?token='+ getCookie('user_token'));

# secrets.yml
action_cable:
  host: 'localhost:3000'
  allowed_origin: localhost
````

**Balance**

````ruby
user = User.find_by account:"chenhanting"
Esportsbull.new(user).balance

user = User.find_by account:"chenhanting"
GamesV2::Vr.new(user).balance
````

呼叫`balance`的路徑，經過設定中斷點過後，發現是沒有在 simple_game_factory.rb 設定的關係

````ruby
[26] "/Users/hanting/fatcat/app/services/games/imone.rb:65:in `balance'",
[27] "/Users/hanting/fatcat/app/models/client/game_wallet.rb:38:in `get_remote_balance'",
[28] "/Users/hanting/fatcat/app/models/client/game_wallet.rb:27:in `update_balance'",
[29] "/Users/hanting/fatcat/app/channels/concerns/wallet_operations.rb:21:in `game_balance'",
````

因為VR彩票繼承imone的關係，所以`login`'`balance`,`transfer`都不用自己寫

**Getbetlog**

注意**:**

- 日志的搜寻时间区间封顶在 10 分钟 (举例, 2016-10-10 00.00.00 - 2016-10-10 00.10.00)。

- Product Wallet = 102(PlayTech)的特殊情况，每个请求的时间范围限制最长为 30 分钟(例

  如，2016-10-10 00.00.00 - 2016-10-10 00.30.00)。

- 一页最多可容纳 50,000 笔日志(即 pagesize=50,000)。如果日志超过 50,000 笔，营运商需发

  下一页的请求。

- 关于并发请求限制，请参考第 1.3 节。

- 如果需要发下一个请求，营运商需等待至当前请求处理完毕。

- 日志可能有 15 分钟的更新延迟。建议只搜索至 15 分钟前的日志 (举例，当前时间为 2016-

  10-10 00:30:00，可搜寻的日期区间为 2016-10-10 00:05:00 - 2016-10-10 00:15:00)。请参考附 录E。

- 日志是以日志最后更新时间来搜寻，即 IMOne 系统最后更新该笔日志的时间 (也就是说，StartDate 和 EndDate 参数是在搜寻 LastUpdatedDate)。

- 依据不同的供应商所􏰁供的响应，日志是有可能被更新超过一次的 (举例，结算响应(免费 旋转)在下注稍后时间才收到)。当此情况发生时，营运商在收到更新的日志，请您务必更 新营运商端的该笔日志资料。

- 更快获取响应之建议 (Product Wallet = 102):

  - 请求时用较小的搜寻时间区间。
  -  请求时用最大单页笔数(50,000)，页数会较少;而不是用小单页笔数，页数会较多。
  -  当并发多个请求时，每个请求用不同的搜寻时间区间。

![image-20191218103456096](/Users/hanting/Library/Application Support/typora-user-images/image-20191218103456096.png)![image-20191218103533158](/Users/hanting/Library/Application Support/typora-user-images/image-20191218103533158.png)

VR的狀態：open, settled, cancelled

- Open (此注单已被接受，但还未结算)
- Settled (此注单已结算)
- ~~Unsettled (此注单因某种原因由结算转为未结算。举例，成绩错 误。此注单过后将被重新结算或取消)~~
- Cancelled (此注单已被取消)

![image-20191218110136994](/Users/hanting/Library/Application Support/typora-user-images/image-20191218110136994.png)

抓取遊戲紀錄的區間只有10分鐘，潛在的延遲一共15分鐘

````ruby
past = (Time.current-9.minutes).strftime('%Y-%m-%d %H.%M.%S')
now = Time.current.strftime('%Y-%m-%d %H.%M.%S')
=> "2019-12-17 17.42.55"
=> "2019-12-17 17.51.55"

# postman 無紀錄
user = User.find_by account:"dfwbwvergwwqee"
GamesV2::Vr.new(user).bet_record((Time.current-9.minutes), Time.current)

# 打賞紀錄
user = User.find_by account:"chenhanting"
past = '2019.12.18 10:10:00'.to_datetime
now = '2019.12.18 10:15:00'.to_datetime
GamesV2::Vr.new(user).bet_record(past, now)

# 遊戲紀錄
user = User.find_by account:"chenhanting"
past = '2019.12.18 10:15:00'.to_datetime
now = '2019.12.18 10:25:00'.to_datetime
GamesV2::Vr.new(user).bet_record(past, now)

# 11:11, 11:13, 11:15, 11:21 進行投注
# 遊戲紀錄(因為潛在延遲，所以抓不到)
user = User.find_by account:"chenhanting"
past = '2019.12.18 11:10:00'.to_datetime
now = '2019.12.18 11:20:00'.to_datetime
GamesV2::Vr.new(user).bet_record(past, now)

# 遊戲紀錄(抓到7筆)
user = User.find_by account:"chenhanting"
past = '2019.12.18 11:20:00'.to_datetime
now = '2019.12.18 11:30:00'.to_datetime
GamesV2::Vr.new(user).bet_record(past, now)

# 遊戲紀錄(抓到7筆)
key = GamesV2::Vr.keys('vr')
past = '2019.12.18 11:20:00'.to_datetime
now = '2019.12.18 11:30:00'.to_datetime
BetLog::Vr.new.get_bet_history(key, past, now)
````

Postman: 無紀錄

![image-20191217180058427](/Users/hanting/Library/Application Support/typora-user-images/image-20191217180058427.png)

打賞後抓取遊戲紀錄

````ruby
res
{
        :ok => true,
    :result => {
               :log => [
            [0] {
                        "Provider" => "VR_LOTTERY",
                          "GameId" => "imlotto20001",
                        "GameName" => "Venus 1.5 Lottery",
                          "GameNo" => "",
                        "GameNoId" => "",
                        "PlayerId" => "DFCTvrchenhanting",
                "ProviderPlayerId" => "IM03GDFCTvrchenhanting",
                        "Currency" => "CNY",
                            "Tray" => "",
                           "BetId" => "31218095854608776056",
                           "BetOn" => "",
                         "BetType" => "",
                      "BetDetails" => "Anchor Name=Cathy;Gift Name=舞狮祝贺",
                            "Odds" => "",
                       "BetAmount" => 0.0,
                        "ValidBet" => 0.0,
                         "WinLoss" => 0.0,
                   "PlayerWinLoss" => 0.0,
                       "LossPrize" => 0.0,
                            "Tips" => 200.0,
                  "CommissionRate" => 0.0,
                      "Commission" => 0.0,
                          "Status" => "Settled",
                        "Platform" => "NA",
                         "BetDate" => "2019-12-18 09:58:54 +08:00",
                      "ResultDate" => "",
                     "DateCreated" => "2019-12-18 10:10:10 +08:00",
                 "LastUpdatedDate" => "2019-12-18 10:10:10 +08:00"
            }
        ],
        :pagination => {
            "CurrentPage" => 1,
              "TotalPage" => 1,
            "ItemPerPage" => 1,
             "TotalCount" => 1
        }
    }
}
````

遊戲紀錄

````ruby
[
    [0] {
                "Provider" => "VR_LOTTERY",
                  "GameId" => "imlotto20014",
                "GameName" => "VR Track Cycling",
                  "GameNo" => "20191218048",
                "GameNoId" => "1",
                "PlayerId" => "DFCTvrchenhanting",
        "ProviderPlayerId" => "IM03GDFCTvrchenhanting",
                "Currency" => "CNY",
                    "Tray" => "1980",
                   "BetId" => "11218101132865817866",
                   "BetOn" => "前三直选复式",
                 "BetType" => "Position=一,二,三;Number=01,02,03;",
              "BetDetails" => "WinningNumber=01,06,11,08,09;",
                    "Odds" => "1940.4",
               "BetAmount" => 2.0,
                "ValidBet" => 2.0,
                 "WinLoss" => -2.0,
           "PlayerWinLoss" => -2.0,
               "LossPrize" => 0.0,
                    "Tips" => 0.0,
          "CommissionRate" => 0.0,
              "Commission" => 0.0,
                  "Status" => "Settled",
                "Platform" => "NA",
                 "BetDate" => "2019-12-18 10:11:32 +08:00",
              "ResultDate" => "2019-12-18 10:12:28 +08:00",
             "DateCreated" => "2019-12-18 10:18:10 +08:00",
         "LastUpdatedDate" => "2019-12-18 10:18:10 +08:00"
    },
    [1] {
                "Provider" => "VR_LOTTERY",
                  "GameId" => "imlotto20012",
                "GameName" => "VR Horse Racing",
                  "GameNo" => "20191218049",
                "GameNoId" => "1",
                "PlayerId" => "DFCTvrchenhanting",
        "ProviderPlayerId" => "IM03GDFCTvrchenhanting",
                "Currency" => "CNY",
                    "Tray" => "1980",
                   "BetId" => "11218101241453115608",
                   "BetOn" => "独赢",
                 "BetType" => "Position=冠;Number=01;",
              "BetDetails" => "WinningNumber=05,10,07,06,03,09,08,02,04,01;",
                    "Odds" => "9.9",
               "BetAmount" => 1.0,
                "ValidBet" => 1.0,
                 "WinLoss" => -1.0,
           "PlayerWinLoss" => -1.0,
               "LossPrize" => 0.0,
                    "Tips" => 0.0,
          "CommissionRate" => 0.0,
              "Commission" => 0.0,
                  "Status" => "Settled",
                "Platform" => "NA",
                 "BetDate" => "2019-12-18 10:12:41 +08:00",
              "ResultDate" => "2019-12-18 10:14:00 +08:00",
             "DateCreated" => "2019-12-18 10:20:10 +08:00",
         "LastUpdatedDate" => "2019-12-18 10:20:10 +08:00"
    },
    [2] {
                "Provider" => "VR_LOTTERY",
                  "GameId" => "imlotto20013",
                "GameName" => " VR Swimming",
                  "GameNo" => "20191218049",
                "GameNoId" => "1",
                "PlayerId" => "DFCTvrchenhanting",
        "ProviderPlayerId" => "IM03GDFCTvrchenhanting",
                "Currency" => "CNY",
                    "Tray" => "1980",
                   "BetId" => "11218101203659812879",
                   "BetOn" => "定位胆",
                 "BetType" => "Position=冠;Number=01,,,,,,,,,;",
              "BetDetails" => "WinningNumber=07,03,04,08,01,09,02,05,06,10;",
                    "Odds" => "9.9",
               "BetAmount" => 1.0,
                "ValidBet" => 1.0,
                 "WinLoss" => -1.0,
           "PlayerWinLoss" => -1.0,
               "LossPrize" => 0.0,
                    "Tips" => 0.0,
          "CommissionRate" => 0.0,
              "Commission" => 0.0,
                  "Status" => "Settled",
                "Platform" => "NA",
                 "BetDate" => "2019-12-18 10:12:03 +08:00",
              "ResultDate" => "2019-12-18 10:14:00 +08:00",
             "DateCreated" => "2019-12-18 10:20:10 +08:00",
         "LastUpdatedDate" => "2019-12-18 10:20:10 +08:00"
    }
]
````

遊戲紀錄 => 11:20~11:30

````ruby
{
        :ok => true,
    :result => {
               :log => [
            [0] {
                 ...
            },
            [1] {
                 ...
            },
            [2] {
                 ...
            },
            [3] {
                 ...
            },
            [4] {
                        "Provider" => "VR_LOTTERY",
                          "GameId" => "imlotto20001",
                        "GameName" => "Venus 1.5 Lottery",
                          "GameNo" => "20191218090",
                        "GameNoId" => "1",
                        "PlayerId" => "DFCTvrchenhanting",
                "ProviderPlayerId" => "IM03GDFCTvrchenhanting",
                        "Currency" => "CNY",
                            "Tray" => "1980",
                           "BetId" => "11218111352260288632",
                           "BetOn" => "大小单双",
                         "BetType" => "Position=万;Number=大,,,,;",
                      "BetDetails" => "WinningNumber=0,5,6,1,8;",
                            "Odds" => "1.97",
                       "BetAmount" => 1.0,
                        "ValidBet" => 1.0,
                         "WinLoss" => -1.0,
                   "PlayerWinLoss" => -1.0,
                       "LossPrize" => 0.0,
                            "Tips" => 0.0,
                  "CommissionRate" => 0.0,
                      "Commission" => 0.0,
                          "Status" => "Settled",
                        "Platform" => "NA",
                         "BetDate" => "2019-12-18 11:13:52 +08:00",
                      "ResultDate" => "2019-12-18 11:15:37 +08:00",
                     "DateCreated" => "2019-12-18 11:22:10 +08:00",
                 "LastUpdatedDate" => "2019-12-18 11:22:10 +08:00"
            },
            [5] {
                        "Provider" => "VR_LOTTERY",
                          "GameId" => "imlotto20020",
                        "GameName" => "Peking Racing",
                          "GameNo" => "742730",
                        "GameNoId" => "",
                        "PlayerId" => "DFCTvrchenhanting",
                "ProviderPlayerId" => "IM03GDFCTvrchenhanting",
                        "Currency" => "CNY",
                            "Tray" => "1980",
                           "BetId" => "11218112120739875340",
                           "BetOn" => "定位胆",
                         "BetType" => "Position=冠;Number=01,,,,,,,,,;",
                      "BetDetails" => "",
                            "Odds" => "9.9",
                       "BetAmount" => 1.0,
                        "ValidBet" => 1.0,
                         "WinLoss" => -1.0,
                   "PlayerWinLoss" => -1.0,
                       "LossPrize" => 0.0,
                            "Tips" => 0.0,
                  "CommissionRate" => 0.0,
                      "Commission" => 0.0,
                          "Status" => "Open",
                        "Platform" => "NA",
                         "BetDate" => "2019-12-18 11:21:20 +08:00",
                      "ResultDate" => "",
                     "DateCreated" => "2019-12-18 11:24:10 +08:00",
                 "LastUpdatedDate" => "2019-12-18 11:24:10 +08:00"
            },
            [6] {
                 ...
            },
            [7] {
                 ...
            }
        ],
        :pagination => {
            "CurrentPage" => 1,
              "TotalPage" => 1,
            "ItemPerPage" => 8,
             "TotalCount" => 8
        }
    }
}
````

抓取遊戲紀錄

注意：在抓取遊戲紀錄前，先加入翻譯檔，不然`bet_log_timestamp.process!`會出現錯誤

```ruby
# 寫入redis(棄用)
key = GamesV2::Vr.keys('vr')
start_time = '2019.12.18 11:20:00'.to_datetime
end_time = '2019.12.18 11:30:00'.to_datetime
GetBetLogJob.new.back('vr', key, start_time, end_time)

def back(partner, key, start_time, end_time)
  time_flag = Time.parse(start_time.to_s).to_i
  bets = BetLog::Vr.new.get_bet_history(key, start_time, end_time)
  redis_bet_log.set("#{partner}-lottery-#{time_flag}", bets.to_json)
  bet_log_timestamp.success!(bets.count)
end

# 寫入redis
key = GamesV2::Vr.keys('vr')
start_time = '2019.12.18 11:20:00'.to_datetime
end_time = '2019.12.18 11:30:00'.to_datetime
GetBetLogJob.new.perform('vr', key, '2019.12.18 11:20:00', '2019.12.18 11:30:00', 3, 0, 'game')

namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.game_log_redis_url)
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).keys

WriteBetLogToMongoJob.new.perform

```

Redis 相關指令

```ruby
# 查看keys
namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.game_log_redis_url)
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).keys

# 取得某一筆
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).get("vr-game-1576639200")

# 刪除某一筆
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).del("vr-game-1576639200")

# 刪除全部
namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.game_log_redis_url)
redis = Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection)
redis.del(redis.keys('*'))
```

下午打賞

````ruby
key = GamesV2::Vr.keys('vr')
GetBetLogJob.new.perform('vr', key, '2019.12.18 18:35:00', '2019.12.18 18:45:00', 3, 0, 'game')
WriteBetLogToMongoJob.new.perform
````

下午打賞

````ruby
key = GamesV2::Vr.keys('vr')

user = User.find_by account: "chenhanting"
key = user.company.game_key('vr')
GetBetLogJob.new.perform('vr', key, '2019.12.19 14:30:00', '2019.12.19 14:40:00', 3, 0, 'game')
GetBetLogJob.new.perform('vr', key, '2019.12.19 14:35:00', '2019.12.19 14:45:00', 3, 0, 'game')

WriteBetLogToMongoJob.new.perform
````

使用 console 呼叫 lottery_vr.log

```shell
cd log
ls
tail -f poker_rmg.log
```

刪除GameLog裡面資料

````ruby
GameLog.delete_all
````

### 相關網站

- Get_bet_log_job：https://betlog-dev.1a2b333.com/cron
- Write_bet_log_job：https://admin.1a2b333.com/sidekiq/cron

### 專有名詞

1. 盤口(Tray)

   所謂的盤口就是[博彩公司](https://cht.aaakk.org/online-casino/)開出來體驗比賽雙方差距的一個參照物。舉個例子說，一個高中生和一個小學生打架，小學生需要高中生讓他雙手才有可能打贏，而高中生讓出的這“兩雙手的不使用權”就相當於實力差。[體育博彩](https://cht.aaakk.org/sports-betting/)中所說的盤口就是雙方實力差的表現。

   下面我們就通過具體例子來說明一下：

   假如西班牙足球甲級聯賽領頭羊皇家馬德里主場與升班馬萊萬特比賽，一般的體育球迷都知道皇馬的獲勝幾率大得多。但是足球比分是90分鐘的，所以強隊也不能一無止境地把弱隊打敗，所以博彩公司就根據本場比賽的各種因素開出一個代表著雙方實力差的數位，這個數位就叫做盤口。

   eg: 皇家馬德里 -2(兩球) 萊萬特

   例如這個不等式中，-2就等於本場比賽的亞洲讓分盤口，也就是主隊皇馬讓客隊萊萬特2個球的意思。

   如果本場比賽最後的結果是皇馬淨勝客隊2個球，那麼本場比賽就等於走盤，所有下注的本金都將退還給玩家；

   如果本場比賽最後的結果是皇馬淨勝客隊2球以上，那麼本場比賽就等於皇馬贏盤，所有下注皇馬的籌碼都是贏的；

   如果本場比賽最後的結果是皇馬贏客隊不到2球，那麼本場比賽就等於萊萬特贏盤，所有下注客隊萊萬特的籌碼都是贏的。

### 按讚功能(side project)

1. Polymorphic Routes in Rails：http://thelazylog.com/polymorphic-routes-in-rails/

2. What are Nested Routes in Rails：https://stackoverflow.com/questions/25219534/what-are-nested-routes-for-in-rails

3. Polymorphic Path：https://stackoverflow.com/questions/6142013/polymorphic-path-for-custom-collection-route

4. 如果使用Params會發生的錯誤：https://stackoverflow.com/questions/49793030/actioncontrollerparametermissing-param-is-missing-or-the-value-is-empty-use
5. Toggle CSS using CLASS：https://stackoverflow.com/questions/19085746/how-can-i-toggle-between-any-css-property-on-click-using-javascript-or-jquery

### 專案：第一個API

1. class_eval的用法：https://ithelp.ithome.com.tw/articles/10201612

2. 成果（上圖：成功 / 下圖：失敗）

   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191222161914480.png" alt="image-20191222161914480" style="zoom:80%;" />

   ![image-20191222161945475](/Users/hanting/Library/Application Support/typora-user-images/image-20191222161945475.png)

