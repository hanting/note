# The 16th Week

以下為到職第16週的紀錄，這週重點：

- 接 Gamatron 遊戲
  - （經驗）錯誤訊息來源
  - （經驗）cron_jobs 不能先新增還沒上線的遊戲
- 解析 Form
- 部署

### Gamatron 遊戲

遊戲資訊：

````text
Here is the detiails of staging. :)

backoffice URL: https://tw-stg.Gamatron.tech:8445/
account       : fatcat
password      : YM1r14oeKJJrACtx

launch game link: https://tw-stg.Gamatron.tech:8080/fatcat/launchGame?game=beatbox&mode=fun

api host      : http://211.21.63.137:7092/
gp-p-id       : fatcat
gp-api-key    : 6Y5P8hJzL3iDja59
````

推論 => partnerId: fatcat

#### 註冊 Register

Request Endpoint

````text
HTTP POST https://{host}/{partnerId}/register
````

host 可能為

- http://211.21.63.137:7092
- https://tw-stg.Gamatron.tech:8080

partnerId 可能為

- fatcat
- Gamatron

標頭 Headers

````text
Content-Type  : application/json
gp-api-key    : 6Y5P8hJzL3iDja59
gp-p-id       : fatcat
````

請求內容 Content

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191223095736052.png" alt="image-20191223095736052" style="zoom:100%;" />

文檔給的範例

````json
[ 
  {
  "playerId": "test_account",
  "currency": "EUR",
  "locale": "en"
  }, 
  {
  "playerId": "test_account_B",
  "currency": "GBP",
  "locale": "zh-CN"
  } 
]
````

明白錯誤的回應是遊戲商給的，還是瀏覽器給的很重要，下面截圖為遊戲商輸出的錯誤訊息。

- （經驗）通常`500`都為遊戲商回覆的訊息
- `Failde to parse JSON` 不是 Chrome 瀏覽器的反應

![image-20191223114348094](/Users/hanting/Library/Application Support/typora-user-images/image-20191223114348094.png)

其他如查看錢包、轉入/轉出、檢查玩家是否存在等api均正常

### SQL

1. `user = User.find_by(account: "chenhanting")` 的 SQL

   ````sql
   User Load (4.5ms)  SELECT  `users`.* FROM `users` WHERE `users`.`account` = 'chenhanting' LIMIT 1
   ````

   

      


### Form

若觸發update時，進入 update 的 params 為

````ruby
params
=> <ActionController::Parameters {"utf8"=>"✓", "_method"=>"patch", "authenticity_token"=>"uQf057DUAZiqEEdWfUB5MAoUTzhD2b0int3o8YOc9J5GsQV3yblQWyeAYPRdNUZAixZnesLlmr0PSq+OhEOvwA==", "lab_forum"=>{"question"=>"Hello", "description"=>"", "category"=>""}, "commit"=>"Submit", "controller"=>"admin/lab_forum", "action"=>"update", "locale"=>"zh-TW", "id"=>"77"} permitted: false>
````

````html
<form class="simple_form edit_lab_forum" id="edit_lab_forum_77" novalidate="novalidate" action="/zh-TW/lab_forum/77" accept-charset="UTF-8" method="post">
  <input name="utf8" type="hidden" value="✓">
  <input type="hidden" name="_method" value="patch">
  <input type="hidden" name="authenticity_token" value="...">

  <input type="submit" name="commit" value="Submit" data-disable-with="Submitting..." class="btn btn-primary">
</form>
````

解答：

If you are not using Laravel and want to build a form manually, you cannot use PUT/PATCH – there’s just no such methods supported by forms in browsers – it’s only GET and POST. So how does Laravel make it work with {{ Form::create([‘method’ => ‘PUT’]) }}? Actually, under the hood the generated HTML looks like this:

That’s right, Laravel constructs a hidden field with name _method and then checks it upon form submittion, routing it to the correct Controller method.

So if for any reason you would need to build the FORM tag yourself, don’t put (same applied to patch and delete) – it just won’t work. Instead add hidden fields, if necessary.

開發的過程中，有時候會需要從另外一臺機器測試在local的網站及服務。最常見的情況就是當要測試網站在手機上看起來長什麽樣子的時候，或者有時候要讓別人連到自己local的sql serer的時候。如果說要測試網站，還可以透過上到一臺測試機器，但是如果是local的sql server的話，除了在同一個局域網外比較容易，其他就需要改到路由什麽，太麻煩了，今天介紹的工具，*ngrok*，就是讓這一切變得容易的工具，而且很佛心的是，*免費版就夠一般使用*。

使用方式：`./ngrok http 3000`

如此一來，`3000`port 就會對應 http://584da8a7.ngrok.io

![image-20191224104918141](/Users/hanting/Library/Application Support/typora-user-images/image-20191224104918141.png)

以下為`routes.rb` 關於api callback部分，一共有 `ebet`, `n2`, `cmd`, `xj`等需要類似的驗證

````ruby
namespace :api do
  scope path: '/v1/', module: 'v1', defaults: { format: :json } do
    resources :bet_histories, only: :index
    get  :deposits, to: 'client_orders#deposits'
    post :ebet, to: 'game#ebet'
    get  :cock_fight, to: 'cock_fight#index'
    get  :kk, to: 'game#kk_check_user'
    post :n2live, to: 'n2#callback'
    post :ea, to: 'ea#callback'
    get :cmd, to: 'cmd#create'
    post :Gamatron, to: 'Gamatron#create'
  end
end

post '/api/v1/xj/AgileBet/api/Authentication', to: 'api/v1/xj#create'
````

仿照cmd體育的做法寫`Gamatron_ocntroller.rb`

````ruby
class Api::V1::GamatronController < ApiController
  def create
    Gamatron_service = SimpleGameFactory.create_game(:Gamatron, current_user)

    render plain: Gamatron_service.authentication(request.body.read)
  end
end
````

````ruby
request.params
{
        "format" => :json,
    "controller" => "api/v1/Gamatron",
        "action" => "create"
}
````

使用 RestClient 進行 Create 操作

````ruby
user = User.find_by account:"chenhanting"
GamesV2::Gamatron.new(user)
````

進入 controller 後，headers 加入 `Content-Type = 'application-json'` 會出現 `Loading Error`，因此要加 `wrap_parameters format: []`

````ruby
class Api::V1::GamatronController < ApiController
  wrap_parameters format: []

  def create
    Gamatron_service = SimpleGameFactory.create_game(:Gamatron, current_user)

    render plain: Gamatron_service.authentication(request.params)
  end
end
````

轉入、轉出

````ruby
# 轉入
user = User.find_by account:'chenhanting'
order = Client::GameOrder.create(user: user, partner: 'Gamatron', order_type: "deposit", amount: 2000000.0, status: "new_order")
GamesV2::Gamatron.new(user).transfer(order)

# 轉出
user = User.find_by account:'chenhanting'
order = Client::GameOrder.create(user: user, partner: 'Gamatron', order_type: "withdrawal", amount: 1.0, status: "new_order")
GamesV2::Gamatron.new(user).transfer(order)
````

錢包餘額

````ruby
user = User.find_by account:'chenhanting'
GamesV2::Gamatron.new(user).balance
````

查看訂單

````ruby
user = User.find_by account:'chenhanting'
order = Client::GameOrder.create(user: user, partner: 'Gamatron', order_type: "deposit", amount: 2.0, status: "new_order")

GamesV2::Gamatron.new(user).transfer(order)
GamesV2::Gamatron.new(user).check(order)

> sty_data
{
         "playerId" => "DFCTchenhantinj",
        "timestamp" => 1577193366000,
    "transactionId" => "1577193366",
             "type" => "transfer",
         "currency" => "CNY",
           "amount" => 2.0,
          "balance" => 20.0,
           "action" => "deposit",
           "status" => "applied",
              "ref" => nil
}
````

查看訂單（大富翁棋牌）

````ruby
user = User.find_by account:'chenhanting'
order = Client::GameOrder.create(user: user, partner: 'rmg', order_type: "deposit", amount: 2.0, status: "new_order")

GamesV3::RMG.new(user).transfer(order)
GamesV3::RMG.new(user).check(order)

> sty_data
{
    "b" => "/proxyRequest",
    "a" => 4,
    "c" => {
          "code" => 0,
        "status" => "0",
         "score" => "2.00"
    }
}
````

登入Gamatron 遊戲機制為

````text
         -------------- 帶入 Token  -------------> 

         <------------ 確認 Token ----------------
我方                                                 遊戲商

         ---------- 回應 Token 對應的玩家 --------->

         <------------- 准許登入 ------------------
````

````ruby
user = User.find_by account: "chenhanting"
options = {
    params: {
      :category => :egame,
      :game_id => "beatbox",
      :trial => false,
    },
}

GamesV2::Gamatron.new(user).login("desktop", options)
````

使用`RestClient.get`

````ruby
# OK
RestClient.get "https://tw-stg.Gamatron.tech:8080/fatcat/launchGame?game=beatbox&currency=CNY&locale=en&mode=real&launchToken=FCT-GKvyhqneS3j2sw8FD22NVs8N"

# OK
url = "https://tw-stg.Gamatron.tech:8080/fatcat/launchGame"
data = {
  game: "beatbox",
  currency: "CNY",
  locale: :en,
  mode: "real",
  launchToken: "FCT-GKvyhqneS3j2sw8FD22NVs8N",
  }

RestClient.get(url, params: data)
````

遊戲紀錄

````ruby
key = GamesV2::Gamatron.keys('Gamatron')
past = '2019.12.25 16:00:00 +0800'.to_datetime
now = '2019.12.25 22:30:00 +0800'.to_datetime
BetLog::Gamatron.new.get_bet_history(key, past, now)
````

![image-20191227144308402](/Users/hanting/Library/Application Support/typora-user-images/image-20191227144308402.png)

遊戲紀錄：使用`get_bet_history`

````ruby
key = GamesV2::Gamatron.keys('gamatron')
past = '2019.12.25 16:00:00'
now = '2019.12.25 22:30:00'
GetBetLogJob.new.perform('gamatron', key, past, now, 3, 0, 'game')

# 查看 redis
namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.game_log_redis_url)
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).keys

# 清空 redis
namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.game_log_redis_url)
redis = Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection)
redis.del(redis.keys('*'))

WriteBetLogToMongoJob.new.perform
````

Mongodb ORM

````ruby
# 找某筆
GameLog.find_by(partner: "gamatron")

# 找某筆並刪除
GameLog.find_by(partner: "gamatron").delete

# 將種類為Gamatron的進行刪除
GameLog.where(partner: 'gamatron').delete_all
````

其他電子遊戲的投注紀錄

![image-20191227153307944](/Users/hanting/Library/Application Support/typora-user-images/image-20191227153307944.png)

#### CRUD

Model：將 Gamatron 遊戲新增至 `company_slots`

```ruby
g = Game::Gamatron.first
Game::CompanySlot.create(company_id: 2, hot_game: true, company_slotable: g)

h = Game::Gamatron.second
Game::CompanySlot.create(company_id: 2, hot_game: true, company_slotable: h)
```

![image-20191226171609091](/Users/hanting/Library/Application Support/typora-user-images/image-20191226171609091.png)

Q1：表`game_source` 是什麼？

A1：`company`對應公司、`game_source` 對應商品 => 多對多

````text
  公司 -------------------------- 商品
   |               |              |
   |               |              |
company ---- company_game ---- game_source
````

Q2：表 `company_slots`是放沒有大廳的電子遊戲嗎？

A2：沒錯，像`game_source`的概念，只不過是小遊戲

Q3、A3：`users`<->`user_game_infos` 好比 `users`<->`user_cards`

設定公司遊戲：http://127.0.0.1:3000/zh-TW/games/egame/gamatrons

![image-20191226151235219](/Users/hanting/Library/Application Support/typora-user-images/image-20191226151235219.png)

遊戲管理：http://127.0.0.1:3000/zh-TW/company_games/slots/qtech

![image-20191226171352019](/Users/hanting/Library/Application Support/typora-user-images/image-20191226171352019.png)

從 routes 開始看：`routes/admin.rb`

````ruby
 resources :company_games, only: [:edit, :update] do
   collection do
     GameCategories.each do |category|
       get category
     end
     get 'slots/:partner' => :slots, as: :slots
   end

   member do
     get :toggle_maintenance
     get :toggle_wallet
     get :toggle_hot_slot
     get :toggle_hot_game
     get :toggle_allowed_trial
   end
 end
````

![image-20191227095615148](/Users/hanting/Library/Application Support/typora-user-images/image-20191227095615148.png)

遊戲前台：http://127.0.0.1:3300/egame/game_list/all?auth_token=GLdNqhFUcrBCG5k8saEJMteXukUuk-BT&locale=zh-TW&partner=gamatron

![image-20191226171054742](/Users/hanting/Library/Application Support/typora-user-images/image-20191226171054742.png)

試玩

![image-20191226171257463](/Users/hanting/Library/Application Support/typora-user-images/image-20191226171257463.png)

````erb
<td class="text-center">
  <%= link_to toggle_allowed_trial_admin_company_game_path(company_game), remote: true, id: "toggle_allowed_trial_#{company_game.id}" do %>
    <% if company_game.triable? %>
      <% if company_game.allowed_trial %>
        <%= fa_icon "toggle-on 2x" %>
      <% else %>
        <%= fa_icon 'toggle-off 2x' %>
      <% end %>
    <% end %>
  <% end %>
</td>
````

| *Prefix*          | *toggle_allowed_trial_admin_company_game* |
| :---------------- | ----------------------------------------- |
| Verb              | GET                                       |
| URI Pattern       | /company_games/:id/toggle_allowed_trial   |
| Controller#Action | admin/company_games#toggle_allowed_trial  |

試玩開關

````ruby
def toggle_allowed_trial
  @company_game.update!(allowed_trial: !@company_game.allowed_trial)
  respond_to do |format|
    format.js
  end
end
````

畫面可以找到`helper`,`controller`, `model`，以此例子可以找到`triable?`方法，再藉由`triable?`找到 `company_game.rb`，找到我們只要改 `TRIABLE_EGAMES = %w[qtech mg pt gamatron].freeze` 即可。

![image-20191227100008279](/Users/hanting/Library/Application Support/typora-user-images/image-20191227100008279.png)   

### Bundle 

若專案bundle後出現無法預期的錯誤，刪除`/vendor/bundle`跟`Gemfile.lock`，再重新跑就好了

### 其他

1. `model` 若類別名稱跟表名稱不一樣，可以這樣寫

   ````ruby
   class Game::CompanySlot < ApplicationRecord
     self.table_name = 'company_slots'
     ...
   end
   ````

   

### 參考連結

1. Evan 專案：https://github.com/sankofa92/to-do-task

2. Rails 表單使用patch背後的原理：https://stackoverflow.com/questions/45061612/trying-to-use-patch-method-works-with-ajax-but-not-with-a-regular-html-form

3. ngrok：https://dashboard.ngrok.com/get-started

4. `Content-Type = 'application-json'` 錯誤修正連結：

   https://stackoverflow.com/questions/51576303/mysterious-rails-5-api-loaderror-for-model-when-posting-json-no-error-when-post

5. 部署影片：https://www.youtube.com/watch?v=vzEEfAj45zw

6. 部署：

   - https://mgleon08.github.io/blog/2017/03/30/capistrano/
   - https://mgleon08.github.io/blog/2015/12/14/ssh-no-password/
   - https://blog.frost.tw/tags/Ruby-on-Rails/page/2/

    



