# The First Week

以下為到職第1週紀錄，這週重點：

- 安裝home-brew, git, rvm, vscode, mongoldb, mysql, brew, sequel pro
- 本地端開啟肥貓、肥寶專案
  - 使用Fork
  - 什麼是PR
- 肥貓前台：選填樣式

## The First Day 

### 進度

1. 安裝homebrew

2. 安裝git

3. 安裝rvm
   - nano ~/.bash_profile
   - Add `source /Users/hanting/.rvm/scripts/rvm`
   - Type `type rvm | head -1`
   - rvm -v
   
4. 安裝vscode

5. 安裝gitlab : https://git.fatcatbet.net

6. 安裝mongodb

7. 安裝mysql

8. brew services list 
   查看安裝的套件，可以看是否為start/stop
   
9. brew services start mysql@5.7
   brew services start mongoldb-community
   brew services start influxes
   
10. 把肥貓（fatcat）從 gitlab 拉下來，並且fork。把 gitlab 中 JVD/fatcat fork下來到自己的名字 `Chen, Han-Ting/fatcat` 

11. rails db:create，產生fatcat_dev、fatcat_production後
    下載 Sequel Pro：https://sequelpro.com/test-builds    
    利用Sequel Pro將其匯入到fatcat_dev
    rails db:migrate
    將 secrets.yml 的site_stage 改成 www，並跑 rails server 進入前台管理
    將 secrets.yml 的site_stage 改成 admin，並跑 rails server 進入後台管理
    後台管理的帳號密碼都為 brohand，驗證碼可以隨便打
    
12. code . 

    You can run VS Code from the terminal by typing 'code' after adding it to the path. Open the Command Palette (⇧⌘P) and type 'shell command' to find the Shell Command: Install 'code' command in PATH command.

## The Second Day 

### 進度

1. 把肥寶（fatbao）從 gitlab 拉下來。不用fork，直接將它複製下來即可。
   git clone git@git.fatcatnet...

2. 肥寶的 ruby/rails 環境為 2.5.5/5.1.6

   肥貓的 ruby/rails 環境為 2.4.6/5.1.6
   rvm install ruby-2.5.5

   rvm gemset create faobao

   rvm use 2.5.5@fatbao

   gem install rails -v 5.1.6

   假設使用肥貓的環境，打 rvm use 2.4.6@fatcat

3. bundle install

   rails db:create

   跑完會出現錯誤：indefined method '[]' for nil:Nilclass

   將secret.yml、mongoid.yml（兩者都是要的）載進去再下一次 rails db:create

   rails db:migrate 會出現錯誤，因為 mongodb 不需要 rails db:migrate

4. 下載studio3T（不要下載robo 3T）

   新增連線後（2nd_day），基本上電腦都可以讀到mongodb內容物

   在下載資料夾內跑（ fatbao_dev 的所在地 ），跑

   mongorestore -d fatbao_dev fatbao_dev

5. rails server -p 3300 或者 export PORT=3300、rails s

   進入 admin.lvh.me:3300，其中admin代表subdomain，而lvh.me為domain

6. 肥寶為肥貓的支付寶，台灣有很多人在做支付，接下來的任務便是把肥貓跟肥寶串起來（沒有做）

7. 進去前台新增帳號密碼，再進入後台改為測試帳號

8. CORS(跨域资源共享，Cross-Origin Resource Sharing)

9. 什麼是PR：

   在 GitHub 上有個有趣的機制：

   1. 先複製（Fork）一份原作的專案到你自己的 GitHub 帳號底下。
   2. 因為這個複製回來的專案已經在你自己的 GitHub 帳號下，所以你就有完整的權限，想怎麼改就怎麼改。
   3. 改完後，先推回（Push）你自己帳號的專案。
   4. 然後發個通知，讓原作者知道你有幫忙做了一些事情，請他看一下。
   5. 原作者看完後說「我覺得可以」，然後就決定把你做的這些修改合併（Merge）到他的專案裡。

   第 4 步的「通知」，就是發一個請原作來拉回去（Pull）的請求（Request），稱之 Pull Request，簡稱 PR。

## The Third Day

### gitlab fork

如何更新fork出來的專案？

在 GitLab 的介面上，可以透過`Fork`的功能，把別人的專案建立一個 fork 到自己的帳號底下。例如原始專案的網址是 `http://gitlab/userA/project.git`，`Fork`出來的專案網址會是 `http://gitlab/userB/project.git`。不過原始專案仍然會繼續更新，而自己`Fork`下來的專案則會停在執行`Fork`當時的狀況。

上述的UserA和UserB分別代表jvd和Han :
UserA : git@git.fatcatbet.net:jvd/fatcat.git
UserB : git@git.fatcatbet.net:Han/fatcat.git

以下為從Ａ到Ｂ的指令：
git clone git@git.fatcatbet.net:Han/fatcat.git
git remote -v
git remote add upstream git@git.fatcatbet.net:jvd/fatcat.git
git remote -v
git pull upstream dev --rebase 

`--rebase` 的觀念很重要，之後再去了解

### 切換分支 

git checkout issue1
git checkout -b issue1 
git branch -d issue1  刪除分支
git branch -m <oldbranch> <newbranch> 修改分支名稱

*https://backlog.com/git-tutorial/tw/reference/branch.html*

### 修改前台：加入選填樣式  

目前的任務為將推薦碼改為選填，目前所需要的關鍵字有推薦碼、必填、選填、註冊會員。

vscode中可以用edit->find in file功能找尋所需關鍵字，locales/_.zh-TW.yml中繁體中文所對應的英文。

| 繁體中文 | 對應的英文    |
| -------- | :------------ |
| 推薦碼   | referral_code |
| 必填     | required      |
| 選填     | optional      |
| 註冊會員 | sign_up_user  |

`required: true`, `optional: true`, `_required?`, `referral_code`等關鍵字，可以幫我找到線索 

下列為目前找到的檔案，以及相關的程式碼

app/models/company.rb

````ruby
def referral_code_required?     
  register_column_set.dig(:referral_code, 'status') == 'required'
end
````

app/api/v2/user/auth.rb

````ruby
if referral_code.present?
  ...
elsif referral_code.blank? && current_company.referral_code_required?
  raise V2::Errors[2006]
end
````

如何trace code：

1. 透過 edit -> find in files 找從 _.zh-TW.yml 找到推薦碼對應的符號 referral_code

2. 昨天透過搜尋註冊會員，找到註冊會員的符號 sign_up_user，找到了三個.erb檔案

   `new.html.erb`, `_modal.html.erb`, `_vbo.html.erb`
   
3. 透過修改 _register_form.html.erb 的內容確定是不是欲修改內容

   註解該行：`<%*#* = set_column(@column_set, :referral_code, f) %>` 

   該行真的消失了

4. `app/controllers/application_controllers.rb`找@column_set

   ````ruby
   # 前台註冊, 登入的 modal 需要用到
   def load_register_column_set
     @column_set = COMPANY.reload.register_column_set
   end
   ````

5. 當我們看jvd.rb檔時，我們可以看到company和mode的身影

   使用 rails console 將推薦碼從必填改為選填。

   值得一提的是，我們看rails的必填選項，必填的紅色星號是rails內建功能

   `rails console`

   ````ruby
   > SITE
   > COMPANY
   > @column_set = COMPANY.reload.register_column_set
   > company = COMPANY
   > company.register_column_set 
   
   #出現相關的json檔案
   > company.register_column_set['referral_code']
   > company.register_column_set['referral_code']['status'] = 'optional'
   > company.register_column_set 
   
   #再重新檢查一次
   > company.save
   ````

   

   ![image-20190913120826999](/Users/hanting/Library/Application Support/typora-user-images/image-20190913120826999.png)

   ![image-20190913120922015](/Users/hanting/Library/Application Support/typora-user-images/image-20190913120922015.png)

   ![image-20190913121143430](/Users/hanting/Library/Application Support/typora-user-images/image-20190913121143430.png)

   ![image-20190913121229045](/Users/hanting/Library/Application Support/typora-user-images/image-20190913121229045.png)

   ![image-20190913121312668](/Users/hanting/Library/Application Support/typora-user-images/image-20190913121312668.png)

   ![image-20190913121403491](/Users/hanting/Library/Application Support/typora-user-images/image-20190913121403491.png)

6. 再來找set_column()的功能

7. `jvd.rb` 放置肥貓網站全域的設定檔，位置在config/initializers/jvd.rb。

8. 在`config/initializers/jvd.rb`中的 `MODE = Rails.application.secrets.site_stage` 可以控制要進入前台還是進入後台，當`secrets.yml`中的`site_stage=www`為進入前台；反之當`secrets.yml`中的`site_stage=admin`為進入後台。

9. 在`config/initializers/jvd.rb`中的 `SITE = Rails.application.secrets&.site_code&.freeze` 為...

10. 最後結果 

<img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190912151631400.png" alt="image-20190912151631400" style="zoom:50%;" />



### GItLab

1. Origin是Chen, Han-Ting的remote分支
2. Upstream是JVD的Remote分支

### 參考資料

1. Brohand's slide : slides.com/brohand
2. 什麼為 PR : https://gitbook.tw/chapters/github/pull-request.html 
3. Cors : https://www.jianshu.com/p/c54a1dbaab24 
4. Markdown Reference : https://support.typora.io/Markdown-Reference/
5. React on Rails example : https://www.sitepoint.com/react-rails-5-1/
6. `code .`指令 : *https://code.visualstudio.com/docs/setup/mac*
7. How to install Xcode, Homebrew, Git, RVM, Ruby & Rails on Mac OS X (from Snow Leopard to Mojave)
   *https://www.moncefbelyamani.com/how-to-install-xcode-homebrew-git-rvm-ruby-on-mac/*
8. Starting and Stopping Background Services with Homebrew
   *https://thoughtbot.com/blog/starting-and-stopping-background-services-with-homebrew*
9. Install MySQL 5.7 on macOS : https://medium.com/@at0dd/install-mysql-5-7-on-mac-os-mojave-cd07ec936034 