# The Second Week

以下為到職第2週紀錄，這週重點：

- 熟悉肥貓專案
- ku重構
  - 重構目的
  - 了解 views helper

1. 開前台與後台

   secrets.yml : 修改 site_stage

   前台/後台：`rails server` / `rails derver -p 3300 -P /your_path/(pid name).pid` 

   如何成功都入前台的遊戲類別：`$ rake categories:create_default`

   ````ruby
   FATCAT done!
   FengChao done!
   ````

2. 重構的目的：

   肥貓的入口網站內方法已經統一（例如：login,create...等等），不過method所帶入的參數並沒有統一。當要判斷進入哪個遊戲時，需要case...when...判斷如何進入遊戲，重構的目的即統一參數、統一流程，消除case...when...。

3. 作用範圍 (scope)
   作用範圍就是一個變數能觸及到的視野, 在變數的 scope 裡, 使用該變數都有效，每種 storage class 都有其自然視野(natural scope)。視野大致可分為 block scope, file scope 兩種，block scope 就是從變數宣告開始, 到 block 結束為止，file scope 就是從變數宣告開始, 到  file 結束為止，會發生 undeclare 的錯誤, 就是因為沒在某變數的 scope 裡使用該變數。

   rails 講如何在`model`和`routes`中使用scope，與上面的scope意思不一樣。

#### ku 重構

view helper : 

除了使用*Rails*內建的*Helper*，我們可以建立自定的*Helper*，只需要將方法定義在*app/helpers/*目錄下的任意一個檔案就可以了。在產生*Controller*的同時，*Rails*就會自動產生一個同名的*Helper*檔案，照慣例該*Controller*下的*Template*所用的*Helper*，就放在該檔案下。如果是全站使用的*Helper*，則會放在*app/helpers/application_helper_rb* 

### 其他

1. Gemfile 和 Gemfile.lock 的異同

   The `Gemfile` is where you specify which gems you want to use, and lets you specify which versions.

   The `Gemfile.lock` file is where Bundler records the exact versions that were installed. This way, when the same library/project is loaded on another machine, running `bundle install` will look at the `Gemfile.lock` and install the exact same versions, rather than just using the `Gemfile` and installing the most recent versions. (Running different versions on different machines could lead to broken tests, etc.) You shouldn't ever have to directly edit the lock file.

2. `vscode` 可以調 `erb` 的顏色 

3. 什麼為%w ?
   `%w(foo bar)` is a shortcut for `["foo", "bar"]`. Meaning it's a notation to write an array of strings separated by spaces instead of commas and without quotes around them. You can find a list of ways of writing literals in [zenspider's quickref](http://www.zenspider.com/ruby/quickref.html#types).

4. 什麼為%i ?
   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190917150011597.png" alt="image-20190917150011597" style="zoom:90%;" />

5. Chrome：訪問 DevTools

| 訪問 DevTools                       | 在 Windows 上         | 在 Mac 上       |
| :---------------------------------- | :-------------------- | :-------------- |
| 打開 Developer Tools                | F12、Ctrl + Shift + I | Cmd + Opt + I   |
| 打開/切換檢查元素模式和瀏覽器窗口   | Ctrl + Shift + C      | Cmd + Shift + C |
| 打開 Developer Tools 並聚焦到控制檯 | Ctrl + Shift + J      | Cmd + Opt + J   |
| 檢查檢查器（取消停靠第一個後按）    | Ctrl + Shift + I      | Cmd + Opt + I   |

6. 什麼是stack?

   Stack是具有「Last-In-First-Out」的資料結構(可以想像成一種裝資料的容器)，「最晚進入Stack」的資料會「最先被取出」，「最早進入Stack」的資料則「最晚被取出」。就像搬家的時候要把書(資料)裝進箱子(Stack)，假設箱子的開口大小剛剛好只能平放一本書，如果先放入《灌籃高手》，再放《笑傲江湖》，再放《搶救國文大作戰》，那麼到了新家之後，因為箱子底下沒有破洞，所以要拿到最底下的《灌籃高手》，就一定得先把上面的《搶救國文大作戰》與《笑傲江湖》拿出箱子才行。觀察以上的敘述，《灌籃高手》符合「最早放入箱子」而且「最晚拿出來」，而《搶救國文大作戰》符合「最晚放入箱子」且「最早拿出來」，其餘的資料也勢必符合此規則(每個資料至少會位於「最上面」一次)，便能夠將這個箱子視為一個Stack。由此可見，Stack是一個抽象的概念，只要符合「Last-In-First-Out」的資料結構，都可以視為Stack。
   參考資料：http://alrightchiu.github.io/SecondRound/stack-introjian-jie.html

7. 什麼是rails stack? 
   I've run into *rails stack* term for many times but I still can't get what do people call *rails stack* furthermore what is a *good knowledge of the rails stack*?

   A "rails stack" refers collectively to all the software needed to run a webapp using Ruby on Rails, most importantly:

### 參考資料

1. 重新命名 gemset : https://stackoverflow.com/questions/3833655/how-do-i-rename-a-gemset

2. `Gemfile` 和 `Gemfile.lock` 的不同：https://stackoverflow.com/questions/6927442/what-is-the-difference-between-gemfile-and-gemfile-lock-in-ruby-on-rails

3. code review 用語：https://farer.org/2017/03/01/code-review-acronyms/

4. rails server的問題

   - https://stackoverflow.com/questions/15072846/server-is-already-running-in-rails
   - https://stackoverflow.com/questions/31039998/address-already-in-use-bind2-errnoeaddrinuse
   - [Rails server says port already used, how to kill that process?](https://stackoverflow.com/questions/4473229/rails-server-says-port-already-used-how-to-kill-that-process) 

5. 網路如何運作：

   https://developer.mozilla.org/zh-TW/docs/Learn/Getting_started_with_the_web/How_the_Web_works#%E6%89%80%E4%BB%A5%E6%88%91%E8%AA%AA%E5%88%B0%E5%BA%95%E7%99%BC%E7%94%9F%E4%BA%86%E5%95%A5%EF%BC%9F

3. How to convert hash to json : https://stackoverflow.com/questions/3183786/how-to-convert-a-ruby-hash-object-to-json
4. 賭博業同行：https://www.slot.com.tw/
5. 訪問 DevTools：https://developers.google.com/web/tools/chrome-devtools/shortcuts?hl=zh-tw
6. C 語言作用範圍(scope) 與 生命期(lifetime) ：https://linch0520.pixnet.net/blog/post/36879259-c-%E8%AA%9E%E8%A8%80%E4%BD%9C%E7%94%A8%E7%AF%84%E5%9C%8D%28scope%29-%E8%88%87-%E7%94%9F%E5%91%BD%E6%9C%9F%28lifetime%29--%5B%E8%BD%89%E8%B2%BC
7. view helper：https://ihower.tw/rails/actionview-helpers.html
8. 與Ruby相關的語法，可以參考：https://www.rubyguides.com/ 
9. session是什麼？ https://railstutorial-china.org/book/chapter8.html#listing-login-form-html
10. Rails 程式碼整理術：https://railsbook.tw/chapters/26-organize-your-code-advanced.html
11. Token Based Authentication : http://jazzlion.github.io/2016/06/17/Token-Based-Authentication/ 
12. Ruby mixin : http://juixe.com/techknow/index.php/2006/06/15/mixins-in-ruby/
13. 程式碼整理術：https://railsbook.tw/chapters/26-organize-your-code-advanced.html
14. super and super() : https://medium.com/rubycademy/the-super-keyword-a75b67f46f05
15. 前端驗證：[https://codertw.com/%E5%89%8D%E7%AB%AF%E9%96%8B%E7%99%BC/243090/](https://codertw.com/前端開發/243090/)
16. 後端驗證：
    - https://ihower.tw/rails/activerecord-lifecycle.html
    - https://railsbook.tw/chapters/19-model-validation-and-callback.html
17. [numbers not allowed (0-9) - Regex Expression in javascript](https://stackoverflow.com/questions/4440286/numbers-not-allowed-0-9-regex-expression-in-javascript)
18. [Difference between \A \z and ^ $ in Ruby regular expressions](https://stackoverflow.com/questions/577653/difference-between-a-z-and-in-ruby-regular-expressions)
19. 使用rails console方法：
    - https://medium.com/@lfv89/rails-console-magic-tricks-da1fdd657d32
    - https://pragmaticstudio.com/tutorials/rails-console-shortcuts-tips-tricks
20. 使用 Ransack 幫你完成後端搜尋與 Sorting (Evan提過的gem)：https://5xruby.tw/posts/ransack-sorting/