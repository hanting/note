# The Third Week

以下為到職第3週紀錄，這週重點：

- ku 重構
- 後台出現數字的地方加入千分位符
- 銀行卡防呆寫測試

### KU重構

1. 文件中說明ku的帳號要截取前面10位，因爲文件規定Account最多只能有20個字元

   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190923144828665.png" alt="image-20190923144828665" style="zoom:75%;" />
   ````@ku_account = user.ku_account|| (prefix + user.account[0..9])````  

   重構時，當帳號前面10位字元都一樣時，會連到同一個帳號，因此在ku遊戲必須要改帳號名方能讓遊戲識別為不同的玩家

4. 利用 orm 選出特定欄位，以ku來說，欲找肥貓跟ku的帳號，即````User.select('account','ku_account')```` 
   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190923162544453.png" alt="image-20190923162544453" style="zoom:50%;" />

   ```` Ruby
   ku = User.where.not(ku_account: nil).select("ku_account","account").to_json
   ku = User.where.not(ku_account: nil).where("length(account) > 10").select("account").to_json
   ku = User.where.not(ku_account: nil).where("length(account) > 10").select("account")[0]["account"]
   
   ku.each_with_index.map do |idx, k| 
     puts("#{idx}")
   end
   
   arr = []
   ku.each do |k| 
     if k["account"].match /^chen/
       arr << k["account"]
     end
   end
   
   ku.each do |ku|
     ku["account"]
   end
   
   ku.each do |ku|
     if ku["account"].match /^chen/
       puts ku["account"]
     end
   end
   ````
   
3. 原本的版本

   ```Ruby
     def initialize(user)
       if user.nil?
       @errors = 'pls initialize with an user'
         return
       end
       prefix = gen_prefix(user)
       @keys = user.company.game_key("ku")
       @ku_account = user.ku_account || (prefix + user.account[0..9])
       @nickname = @user.account
       
       # 如果@user.ku_account不存在則新創一個
       self.create unless @user.ku_account.present?
       # 找ku值不等於空值且長度大於10的account存進ku內
       # 類似 SELECT ku_account FROM `users` WHERE account REGEXP '^<user.account[0..9]>';
       ku = User.where.not(ku_account: nil).where("length(account) > 10").select("ku_account")
       account_prefix_arr = []
       account_prefix = user.account[0..9]
       ku.each do |k| 
         account_prefix_arr << k["ku_account"] if k["ku_account"].match /^#{account_prefix}/
       end
     
       # 如果account_prefix_arr.size串列大於1，則重新存取ku_account
       if account_prefix_arr.size > 1
         source=("a".."z").to_a + ("A".."Z").to_a + (0..9).to_a
         @ku_account = prefix + user.account[0..4]
         5.times{ @ku_account += source[rand(source.size)].to_s }
         self.create
         puts("Rename Success!")
       end
     end
   
     def login(device , _options={})
       super(device)
     end
   
     def create
       super
     end
   ```

   後來的版本：Peter告訴我若要看帳號會不會衝突，要看api確認，若有衝突則利用遞迴方式對@ku_account重新命名並重打一次api看會不會錯誤。

   ```Ruby
     RETRY_COUNT = 5
     ERROR_MESSAGES = {
       '0' => '成功',
       '1001' => '要求次數過於頻繁',
       '1002' => '時間參數逾時失效',
       '2001' => '會員不存在',
       '2002' => '會員已存在',
     }.freeze
   
     def initialize(user)
       if user.nil?
       @errors = 'pls initialize with an user'
         return
       end
       prefix = gen_prefix(user)
       @keys = user.company.game_key("ku")
       @ku_account = user.ku_account || (prefix + user.account[0..9])
       @nickname = @user.account
       
       # 如果@user.ku_account不存在則新創一個
       self.create unless @user.ku_account.present?
     end
   
     def login(device , _options={})
       super(device)
     end
   
     def create
       token = get_token('user')
       user_url = @keys[:user_url]
       game_account = create_game_account(@ku_account)
       @user.update(account: @ku_account) if errCode == 0
     end
   
     def create_game_account(account, retry_count: RETRY_COUNT)
       params = %(data={
                         'action':'add',
                         'token':'#{token}',
                         'account':'#{account}-35',
                         'nickname':'#{@nickname}'
                       })
       response = api_call(user_url, params, 'create')
       errCode = JSON.parse(response.body)['errCode']
   
       if errcode != 0
         # 錯誤碼2002為用戶已存在。若用戶已經存在，則自動重試
         # 這段程式碼參照Peter的xj小金遊戲的重試寫法
         return create_game_account(account.next, retry_count: retry_count - 1) if retry_count.positive? && code == 2002
         error_message = "KU create user error!, error_code: #{ERROR_MESSAGES[code]}, message: #{response_body['msg']}"
         raise CustomError.new(:create_error, error_message)
       end
   
       return account
     end
   ```

### 銀行卡防呆：測試

完整的測試流程

- 匯入Sequal Pro的測試環境

- 安裝chromedriver：brew cask install chromedriver
  $ which chromedriver
  =>  /usr/local/bin/chromedriver

- Chrome和Chrome driver可能有版本不同的問題，所以可以手動調動chrome的版本，以下為說明如何找出並調整chrome driver的方法
  

- 在命令提示字元出現以下錯誤（解決了）

  問題出在如果測試是給前台，而我們測了後來之後就會出現以下錯誤

  ```cmd
  Capybara::ElementNotFound:
    Unable to find link "Create"
  ```
  遇到這種問題如何解決，改`secret.yml`的`site_stage`

  ````yaml
  test:
    secret_key_base: 太長省略
    api_key_for_agent: '12345678'
    redis_host: localhost
    action_cable:
      host: localhost
      allowed_origin: localhost
    mailer_default_url: http://localhost:3000
    mail_sender: test@test.com
    site_code: TTT
    site_stage: www
    slack_webhook_url: https://hooks.slack.com/services/TEST/TEST
  ````

  如此一來便可以測試成功。
  
  順道一提，若測model則不用改變測試環境的前台後台。
  
  ```ruby
  describe 'Validations' do
    it { should validate_uniqueness_of(:bank_account) }
    it { should validate_presence_of(:user_name) }
    it { should validate_presence_of(:bank_account) }
    it { should validate_presence_of(:bank_code) }
    it { should validate_presence_of(:bank_name) }
    it { should validate_presence_of(:sub_branch) }
    it { should validate_presence_of(:province) }
    it { should validate_presence_of(:area) }
    it { should validate_numericality_of(:bank_account) }
  
    subject do
      FactoryBot.build :user_card, user: user
    end
  
    it "is not valid with invalid username" do
      is_expected.not_to allow_value('1234').for(:user_name)
    end
  
    it "is not valid with invalid subbranch" do
      is_expected.not_to allow_value('1234').for(:sub_branch)
    end
  ```

寫了新的驗證，開始寫新的測試

1. 測試指令（範例）：$ rspec spec/models/user_card_spec.rb:50

2. 切換測試、開發環境：
   rails db:migrate RAILS_ENV=development 
   rails db:migrate RAILS_ENV=test


3. (經驗) 渲染html時，html讀取"數字"、"字串"不一樣

#### 會員列表排序

- 預設排序
- 建立時間
- 最後登入時間

last_active_at 與 `ahoy` 的 gem 有關係。

`model/user.rb`

````ruby
  ...
  scope :order_by_create_time, lambda {
    left_joins(:events)
      .order('created_at desc')
      .group('users.id')
  }
   ...
   ...
  def self.order_by(time)
    return order_by_create_time if time == 'create_time'
    order_by_last_event_time
  end
  ...
  ...

````

Jack提到：events 在這個query沒有任何作用，可以不用join，created_at 是user自己table裡面的欄位，可以用一般order 就好，參考一些其他地方的排序。同樣想一下group有什麼作用？還需不需要。

- `group`運用了資料庫的`group_by`功能，讓我們可以將*SQL*計算後(例如*count*)的結果依照某一個欄位分組後回傳，例如說今天我有一批訂單，裡面有分店的銷售金額，我希望能這些金額全部加總起來變成的各分店銷售總金額
- group by可以參照面試曾經寫過的sql

- model/user.rb 後來版本

  ````ruby
    def self.order_by(time)
      return self.order('created_at desc') if time == 'create_time'
      order_by_last_event_time
    end
  ````


#### 遊戲紀錄未結算，加入篩選條件

controller的部分：

- 原先版本

````ruby
# 查詢總筆數
@bets_without_page =  if unsettled.present?
                         GameLog.where({ company_id: @company.id })
                                .unsettled
                                .order_by(:start_time => 'desc')

                      elsif time_type == 'bet_time'
                         GameLog.where({ company_id: @company.id })
                                .with_agents(agent_ids)
                                .category(category)
                                .partner(partner)
                                .keyword(keyword)
                                .settled
                                .bet_time_query(start_time, end_time)
                                .order_by(:start_time => 'desc')
                      else
                         GameLog.where({ company_id: @company.id })
                                .with_agents(agent_ids)
                                .category(category)
                                .partner(partner)
                                .keyword(keyword)
                                .settled
                                .confirmed_time_query(start_time, end_time)
                                .order_by(:confirmed_at => 'desc')
                      end
````

- Evan改的版本

````ruby
def index
  ...

  #把代理或總代查詢查到的代理資訊撈出來
  if [agent, superagent].any?
    agents = Agent.find_agent(agent)
                  .find_superagent(superagent)
                  .pluck(:id, :account, :ancestor_account)
    agent_ids = agents.map &:first
  end
  
  # 查詢總筆數
  @bets_without_page = GameLog.where({ company_id: @company.id })
                              .with_agents(agent_ids)
                              .category(category)
                              .partner(partner)
                              .keyword(keyword)
                              .settled?(unsettled)   # 條件寫在這邊
                              .time_query(unsettled, time_type, start_time, end_time)
                              .order_by(:start_time => 'desc')

  @bet_histories =  @bets_without_page.page(current_page).per(50)
````

model部分：新增.settled?和time_query兩個方法

```ruby
class GameLog
  include Mongoid::Document
  include Mongoid::Timestamps
  ...
  ...
  def self.settled?(result)
    result ? unsettled : settled
  end

  def self.time_query(result, time_type, start_time, end_time)
    return bet_time_query(start_time, end_time) if time_type == 'bet_time' || result
    confirmed_time_query(start_time, end_time)
  end
  ...
end
    
```

### 參考資料

1. ORM

   - https://railsbook.tw/chapters/16-model-basic.html
   - https://stackoverflow.com/questions/3825028/activerecord-select-a-string-field-has-a-certain-length-in-rails-3

2. 千分位符：https://api.rubyonrails.org/classes/ActionView/Helpers/NumberHelper.html#method-i-number_with_delimiter

3. Ruby 字串：[https://blog.xuite.net/yschu/wretch/104912805-Ruby+-+Chapter+12+%E5%AD%97%E4%B8%B2%28String%29](https://blog.xuite.net/yschu/wretch/104912805-Ruby+-+Chapter+12+字串(String))

4. 正規表示法：https://larry850806.github.io/2016/06/23/regex/

5. super和super()：https://www.rubyguides.com/2018/09/ruby-super-keyword/

6. 測試（by Peter）：https://hackmd.io/@haB1MMg3Q_-pvvUi5c46SA/B18rapJ6V

7. rebocop 安裝：https://github.com/misogi/vscode-ruby-rubocop/issues/23

8. mac 設定命令列：

   https://medium.com/statementdog-engineering/prettify-your-zsh-command-line-prompt-3ca2acc967f
   vscode設定命令列：
   [https://medium.com/@h86991868/%E7%9C%8B%E8%86%A9%E4%BA%86%E4%B8%80%E6%88%90%E4%B8%8D%E8%AE%8A%E7%9A%84%E5%B0%8F%E9%BB%91%E7%AA%97-%E6%94%B9%E7%94%A8iterm2-oh-my-zsh%E5%90%A7-cc2b0683acb](https://medium.com/@h86991868/看膩了一成不變的小黑窗-改用iterm2-oh-my-zsh吧-cc2b0683acb)

9. Javascript to coffeescript
   參考資料：http://js2.coffee/ 
   
10. 如何在忽略本地更改的情况下执行git pull？
    參考資料：https://cloud.tencent.com/developer/ask/49644
    
11. select_tag 下拉式選單（銀行卡防呆）
    參考網址：https://rails.ruby.tw/form_helpers.html
    
12. ORM 如何使用 `group`：https://ihower.tw/rails/activerecord-query.html

13. Chrome 看版本：https://help.zenplanner.com/hc/en-us/articles/204253654-How-to-Find-Your-Internet-Browser-Version-Number-Google-Chrome

