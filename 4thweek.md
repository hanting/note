# The Forth Week

以下為到職第4週的紀錄，這週重點：

- 匯出excel
- 銀行卡防呆
- 使用`job_done!`
- 使用`instance_variable_set`
- 工作學經驗
  - 常用 console：尤其是未來前後分離的狀況，後端工程師幾乎用不到view

#### 匯出excel

Ruby on Rails 預設有註冊在mime_type裡面的格式沒有xls，所以要將xls加入註冊的格式中，加入方法為，在`config/initializers/mime_types.rb`檔案加入以下程式碼

```ruby
# Be sure to restart your server when you modify this file.

# Add new mime types for use in respond_to blocks:
# Mime::Type.register "text/richtext", :rtf

Mime::Type.register "application/xls", :xls
```

`routes.rb`

````shell
$ rails routes -g adjustment

Prefix Verb  URI Pattern  Controller#Action
histories_admin_adjustment_index GET   (/:locale)/adjustment_orders/histories(.:format)             admin/adjustment_orders#index {:layout=>:histories, :locale=>/zh-TW|zh-CN|en/}
````

調帳紀錄的控制器的index action，從中可以學習respond_to與render的用法差別。

index控制器為什麼可以控制histories.html.erb與histories.xls.erb，原因是 http://127.0.0.1:3000/zh-TW/adjustment_orders/histories 給了params[:layout] == histories 的參數讓它轉到 histories.html.erb 與 histories.xls.erb 中。

````ruby
module Admin
  #:nodoc:
  class AdjustmentOrdersController < AdminController
    authorize_resource class: 'Client::AdjustmentOrder'
    before_action :set_order, only: %i[show
                                       confirm_approval
                                       approve
                                       confirm_rejection
                                       reject]

    def index
      @orders = Client::AdjustmentOrder.includes(:audits, user: :agent)
                                       .accessible_by(
                                         current_ability,
                                         params[:accessibility] || :read
                                       )
                                       .filter(filtering_params)
                                       .order(created_at: :desc)
                                       .page(params[:page]).per(50)
      return render_in_histories if params[:layout] == :histories
    end

    def render_in_histories
      @plus_sum_orders = @minus_sum_orders = 0

      # call except(:limit, :offset) to unscope 'page' and 'per'
      unpaged_orders = @orders.except(:limit, :offset)
      unpaged_orders.each do |order|
        if order.gp.positive?
          @plus_sum_orders += order.gp
        elsif order.gp.negative?
          @minus_sum_orders += order.gp
        end
      end

      @orders_size = unpaged_orders.size
      # respond_to :xls, :html
      respond_to do |format|
        format.html  { render :histories } 
        format.xls  { render :histories }
      end
    end
````

`histories.html.erb` 需要新增轉 excel 的按鈕

histories.xls.erb

````html
<?xml version='1.0'?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:o="urn:schemas-microsoft-com:office:office"
  xmlns:x="urn:schemas-microsoft-com:office:excel"
  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:html="http://www.w3.org/TR/REC-html40">
  <Worksheet ss:Name="Bets">
    <Table>
      <Row>
        <Cell><Data ss:Type="String">訂單編號交易流水號</Data></Cell>
        <Cell><Data ss:Type="String">玩家帳號</Data></Cell>
        <Cell><Data ss:Type="String">代理/總代</Data></Cell>
        <Cell><Data ss:Type="String">調帳類型</Data></Cell>
        <Cell><Data ss:Type="String">調整額度</Data></Cell>
        <Cell><Data ss:Type="String">轉碼倍率</Data></Cell>
        <Cell><Data ss:Type="String">調帳狀態</Data></Cell>
        <Cell><Data ss:Type="String">建立者</Data></Cell>
        <Cell><Data ss:Type="String">建立時間</Data></Cell>
        <Cell><Data ss:Type="String">審核者</Data></Cell>
        <Cell><Data ss:Type="String">審核時間</Data></Cell>
      </Row>
      <% @orders.each do |order| %>
        <Row>
          <Cell><Data ss:Type="String"><%= order.order_num %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.user.account %></Data></Cell>
          <Cell><Data ss:Type="String"><%= get_agent_and_superagent(order.user) %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.subtype_i18n %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.diff %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.valid_times %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.status %></Data></Cell>
          <Cell><Data ss:Type="String"><%= adjutement_order_creator(order) %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.created_at.strftime("%Y-%m-%d %H:%M:%S") %></Data></Cell>
          <Cell><Data ss:Type="String"><%= adjutement_order_approver(order) %></Data></Cell>
          <Cell><Data ss:Type="String"><%= order.transaction_time&.strftime("%Y-%m-%d %H:%M:%S") %></Data></Cell>
        </Row>
      <% end %>
    </Table>
  </Worksheet>
</Workbook>
````

### QA問答

限制內容字數：使用text-overflow: ellipsis; 

### 其他

1. 前端定時連到channel確認使用者是否login timeout，使用者已經被登出時，websocket發送alert並refresh page

   ```ruby
     def check_user_login_status(data)
       token = data['token']
       session_data = Rails.cache.read(token)
       last_request_at = session_data['warden.user.user.session']['last_request_at']
   ```

2. 卉儀PR

   ```ruby
   # 改版前
   def self.cal_active_users(_day = Date.yesterday, com)
       # 讀取某天的 active user 名單
       users = User.my_company(com.id).where.not(last_sign_in_at: nil).group_by{|_r| _r.last_sign_in_at.to_date}[_day] || []
       # return
       users.count
   
   # 改版中
   def self.cal_active_users(day = Date.yesterday, com)
       start_time = day.to_time
       end_time = (day + 1.day).to_time
       Ahoy::Event.joins(:user).where.not(user_id: nil)
                  .where('users.user_type = ?', 1)
                  .where("time >= ? and time < ?", start_time, end_time)
                  .pluck(:user_id).uniq.count
   
   # 改版後  
   def self.cal_active_users(day = Date.yesterday, com)
       start_time = day.to_time
       end_time = (day + 1.day).to_time
       Ahoy::Event.joins(:user).where.not(user_id: nil)
                  .where('users.user_type = ?', 1)
   							 .where(time: beginning_of_day..end_of_day) # Range的用法 0,,2（0到2）
                  .count('DISTINCT user_id')
   
         
   # 如果我們要找的 orders_count，不一定固定是 2，可能是不定的數字：
   Client.where("orders_count = ?", params[:orders])
   # Active Record 會將 ? 換成 params[:orders] 做查詢。
   # 可以宣告多個條件，條件式後的元素，對應到條件裡的每個 ?。
   Client.where("orders_count = ? AND locked = ?", params[:orders], false)
         
   # 替換除了可以使用 ? 之外，用符號也可以。以 Hash 的鍵值對方式，傳入陣列條件：
   Client.where("created_at >= :start_date AND created_at <= :end_date", {start_date: params[:start_date], end_date: params[:end_date]})
         
   # distinct 用法
   # 
   ```

   從結果中刪除重複項，可以使用`.distinct()` `.uniq()`具有相同的效果。使用Rails 5.0，它已被棄用，它將從版本5.1的Rails中刪除。原因是，“ `unique`這個詞不具有相同的含義，並且可能具有誤導性。此外， `distinct`更接近SQL語法。

3. ActiveJob 與 Sidekiq 兩者都為使用排程的工具，ActiveJob為原生的，而Sidekiq（使用Redis）為第三方套件，但SideKiq對rails的相容性很強

4. 使用`job_done!`

   ```ruby
   # Client::GameWallet：在Client資料夾底下的GameWallet
   user = User.find_by account:'chenhanting'
   Client::GameWallet.find_by(user_id: 1803) #只能找到一筆
   Client::GameWallet.where(user_id: 1803)   #可以找到好幾筆
   Client::GameWallet.where(user_id: 1803, name: 'ku') #只能找到一筆
   Client::GameWallet.find_by(user_id: 1803, name: 'ku').job_done! # 強制結束工作
   
   # 什麼是job_done!
   # 這是brohand寫的方法
     def job_done!
       update!(job: nil, game_order_id: nil)
       Client::GameWalletTransferDoneWorker.perform_async id
     end
   ```

5. 使用`instance_variable_get`

   ```ruby
   class Fred
     def initialize(p1, p2)
       @a, @b = p1, p2
     end
   end
   fred = Fred.new('cat', 99)
   fred.instance_variable_get(:@a)    #=> "cat"
   fred.instance_variable_get("@b")   #=> 99
   ```

   除了這個之外，可以這樣

   ```ruby
   class Toy
     def play
       @cart = 123
     end
   end
   
   toy = Toy.new
   toy.play
   toy.instance_variable_get(:@cart) #=> 123
   # 以下2種方式取不出實例變數
   toy = Toy.new.play
   toy = Toy.new
   ```

6. 使用`instance_variable_set`

   ```ruby
   # example-01
   class Fred
     def initialize(p1, p2)
       @a, @b = p1, p2
     end
   end
   fred = Fred.new('cat', 99)
   fred.instance_variable_set(:@a, 'dog')   #=> "dog"
   fred.instance_variable_set(:@c, 'cat')   #=> "cat"
   fred.inspect                             #=> "#<Fred:0x401b3da8 @a=\"dog\", @b=99, @c=\"cat\">"
   
   
   # example-02
   class Klass
     def set(string)
       var_name = "@#{string}"  # the '@' is required
       self.instance_variable_set(var_name, 'bar')
     end
     def puts_foo
       puts @foo
     end
   end
   k = Klass.new
   k.puts_foo  # nil
   k.set('foo')
   k.puts_foo  # 'bar'
   ```

### 參考網址

1. 使用Ruby on Rails 內建respond_to 方法匯出excel格式資料：https://medium.com/@icelandcheng/%E4%BD%BF%E7%94%A8ruby-on-rails-%E5%85%A7%E5%BB%BArespond-to-%E6%96%B9%E6%B3%95%E5%8C%AF%E5%87%BAexcel%E6%A0%BC%E5%BC%8F%E8%B3%87%E6%96%99-6fb750a11e88
2. 如何使用CSS的「text-overflow: ellipsis;」屬性限制內容字數：https://www.astralweb.com.tw/css-ellipsis/
3. .min.js 與 .js的差別：https://segmentfault.com/a/1190000011285784
4. number_to_currency：https://api.rubyonrails.org/v3.2/classes/ActionView/Helpers/NumberHelper.html
5. pluck 的用法：https://stackoverflow.com/questions/45926450/activerecord-pluck-to-sql
6. rails query：https://rails.ruby.tw/active_record_querying.html
7. rails cache： [https://www.scrivinor.com/article/rails-%E7%9A%84-cache-%E4%BB%8B%E7%B4%B9%E4%B8%80cache-stores?locale=zh-TW
8. 使用ahoy：https://github.com/ankane/ahoy
9. instance_variable_get：https://apidock.com/ruby/Object/instance_variable_get