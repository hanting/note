# The Fifth Week

以下為到職第5週的紀錄，這週重點

- 「未結算」時，只提供「投注時間」選擇
- 大富翁棋牌對接：使用 Postman

#### 未結算時，只能選擇投注時間

```javascript
// admin.js
function change_state_select(){
  var settle_collection = $("#settled-or-unsettled");
  var bet_time_option = $('#startime-and-betime option:eq(' + 1 + ')');  // 「結算時間」的選項
  if (settle_collection.val() == 'unsettled'){
    bet_time_option.hide();
  }else{
    bet_time_option.show();
  };
};
```

```html
<div class="form-group col-md-2">
   <%= select_tag :settle_type, options_for_select([[t('bet_log.settled'), 'settled'], [t('bet_log.unsettled'), 'unsettled']], params[:settle_type]), class: 'form-control',id: "settled-or-unsettled" ,onchange: "change_state_select()" %>
</div>
  
<div class="col-md-2">
  <%= select_tag :time_type, options_for_select({t('bet_log.confirmed_at') => :confirmed_at, t('admin.page_content.bet.time') => :bet_time}, params[:time_type]), class: 'form-control', id: "startime-and-betime" %>
</div>  
  
```

### 參考資料

1. 白名單（whitelist）：https://ihower.tw/rails/routing.html

2. 時間戳記（線上）：https://cloud.magiclen.org/tw/timestamp

3. md5加密：https://stackoverflow.com/questions/4243089/ror-md5-generation

4. 千分位符（使用js）：https://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-currency-string-in-javascript

5. 取得select_tag的值：https://stackoverflow.com/questions/45462357/how-use-jquery-to-get-selected-value-using-simple-form-of-ruby-on-rails  

6. 刪除select_tag的選項：https://stackoverflow.com/questions/3413188/how-to-remove-a-selected-item-from-a-dropdown-list-using-jquery

7. 隱藏select_tag的選項：   http://www.dovov.com/jqueryselect-6.html

8. 預設select_tag：https://stackoverflow.com/questions/623458/rails-select-helper-default-selected-value-how

9. ruby 變數：[https://pjchender.github.io/2017/09/26/ruby-%E8%AE%8A%E6%95%B8%EF%BC%88variable%EF%BC%89%E8%88%87%E5%B8%B8%E6%95%B8%EF%BC%88constant%EF%BC%89/](https://pjchender.github.io/2017/09/26/ruby-變數（variable）與常數（constant）/)

10. Time相關：https://www.rubyguides.com/2015/12/ruby-time/

11. JS的===為值與型態相等

12. Ruby的===為case...when

    參考資料：https://ithelp.ithome.com.tw/articles/10205276