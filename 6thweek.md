# The 6th Week

以下為到職第6週的紀錄，這週重點

- 大富翁棋牌對接 - 5個動作：initialize, create, login, balance, transfer
- 使用UserGameInfo，棄用原本依附在主帳號的遊戲帳號



### 大富翁棋牌

打成功的api

```txt
# 請求
https://demorecord.rmg968.com:190/RecordRequest?proxy=800040&requestTime=1571392351977&d=8s9gYC0YzmwEG8xCk%2FaAJ%2FlsSBGBF9DXHcFq8YDSuEQ25fxBsmlahDW3PNpt%0Azz4mmP8TnGfSTJzBJYoZkwFpwg%3D%3D%0A&key=14e5d86417705dee7361be5a5c6ac861

# 回應 (21點)
{"b":"/recordRequest","a":6,"c":{"code":0,"starttimes":1571393643759,"endtimes":1571393748120,"total":1,"data":{"gameNo":["50-1571393688-7580036-2"],"PlayerFullName":["800040_DFCTrmg1803-chenhanting"],"classNo":[6001],"TypeId":[600],"deskNo":[120020003],"seatNo":[2],"playerNum":[2],"validScore":["0.00"],"totalScore":["600.00"],"profitScore":["0.00"],"shareScore":["0.00"],"startTime":["2019-10-18 18:14:48"],"endTime":["2019-10-18 18:15:31"],"cardValue":["02a1b,1152905,2143115"],"channelNo":[800040],"siteCode":["800040_DFCTrmg"]}}}

# 開始跟結束時間
startTime=#{1571393643759}
endTime=#{1571393748120}
```

遊戲轉入金額：不成功

```txt
# 請求
https://demoapi.rmg968.com:189/proxyRequest?proxy=800040&requestTime=1571394926756&d=fJmjyFU4WURT4TVvrtC7YEQslVeYhyta4ka56Ssq8oZLzgviE3%2Bojq%2BVg76n%0AEp5abuAm6S6%2BOPT4a9egh5hzcGXePYhXPpHB3IEC82mErsyihopRur7r%2BWs%2B%0A6BREIlRRbFiv%2Bv6Q1uk9LT7JLUdS5w%3D%3D%0A&key=ffd98e2f0ab4b32a02f7d13169d82f5b

# 回應
{"b":"/proxyRequest","a":2,"c":{"code":1002}}
```



在transfer方法中，進入點從transfer_job.rb中的game_object.transfer

   ````ruby
   class WalletOrder::TransferJob < WalletOrderJob
     def self.create(game_wallet, game_order)
       ...
     end
   
     def perform(game_wallet_id)
       ...
       begin
         # define game object
         ...
         result = game_object.transfer(game_order) # call api 轉帳
         ...
       rescue
         ...
       end
     end
   end
   ````

在check方法中，進入點從check_job.rb中的game_object.check

在self.CheckState中，進入點從check_job.rb中的game_object.class.method(:CheckState)

   ````ruby
begin
  game_object = SimpleGameFactory.create_game(game_wallet.name, game_order.user)
  check_method = game_object.class.method(:CheckState)
  check = game_object.respond_to?(:check) ? game_object.check(game_order) : check

  case check_method.call(check, game_order)
   ````

rails console

````ruby
user = User.find_by account:'chenhanting'

# 查詢餘額
GamesV2::RMG.new(user).balance    

# 找尋訂單
Client::GameOrder.where(partner: "rmg")      
Client::GameOrder.where(partner: "rmg")[13]  

# 利用重覆的訂單再打一次
GamesV2::RMG.new(user).transfer(Client::GameOrder.where(partner: "rmg")[13]) 
GamesV2::RMG.new(user).transfer(Client::GameOrder.where(partner: "rmg")[58])
GamesV2::RMG.new(user).check(Client::GameOrder.where(partner: "rmg")[57])   

# 使用self.CheckState查看結果
check = GamesV2::RMG.new(user).check(Client::GameOrder.where(partner: "rmg")[57])
order = Client::GameOrder.where(partner: "rmg")[57]
GamesV2::RMG.CheckState(check, order)

# 刪除正在處理中（轉入、轉出）的訂單
Client::GameWallet.where(user_id: 1803, name: 'rmg')[0].job_done! 

# 創立新訂單
order = Client::GameOrder.create(user_id: 1803, partner: "rmg", order_type: "deposit", order_num: %(han#{Random.new_seed}), amount: 8000.0, status: "new_order")

# 轉入訂單
GamesV2::RMG.new(user).transfer(order)

# 時間戳
Time.now - 1.day
(Time.now - 1.day).to_f
(Time.now - 1.day).to_f*1000

````

### 使用 UserGameInfo

將rmg_account的欄位棄用

````ruby
# jvd.rb

GameStatPartner = {
  ...
  :rmg => 36,
}
````

````ruby
# game_stat.rb

class GameStat < ApplicationRecord
  ...
  enum category: GameStatCategory # from jvd.rb
  enum partner: GameStatPartner # from jvd.rb
  ...
end
````

````ruby
# models/user_game_info.rb

class UserGameInfo < ApplicationRecord
  belongs_to :user
  enum partner: GameStatPartner # from jvd.rb
end
````

enum 語法

````ruby
@user.user_game_infos.ids
@user.user_game_infos.partners
# 參考網站：https://www.justinweiss.com/articles/creating-easy-readable-attributes-with-activerecord-enums/

@user.user_game_infos.partners.class
# ActiveSupport::HashWithIndifferentAccess < Hash
````

