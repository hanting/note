# The 7th Week

到職第七週的紀錄，這週重點

- 了解 `transfer_job`,` check_job` 排程
- 了解 `write_bet_log_to_mongo_job.rb`, `get_bet_log_job.rb` 排程
- 使用redis, mongoDB 指令
-  `rails console` 進行登入、錢包、遊戲紀錄等動作
- 了解接遊戲紀錄流程
  - @log, @history 扮演的角色
  - UserGameInfo 使用 enum
  - cron_jobs

### 遊戲紀錄

sidekiq.yml：用來設定進入排程的job

`write_bet_log_to_mongo_job.rb`, `get_bet_log_job.rb`：betlog的排程

GetBetLogJob：將遊戲記錄存入redis

WriteBetLogToMongoJob：將存入redis的記錄存進mongodb

````ruby
user = User.find_by account:"chenhanting"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.10.18 18:14:00', '2019.10.18 18:15:50', 3, 'poker')

WriteBetLogToMongoJob.new.perform
````

取用、刪除 redis 裡面的值

````ruby
redis_bet_log.get 'test'  # 取用
redis_bet_log.del 'test'  # 刪除

redis_bet_log.get "rmg-poker-1571364843"
JSON.parse _
_
_
````

第一場：2場21點的遊戲

````ruby
# start time: 1571711296489
# end time: 1571711626953

Time.at(1571711296) # 2019-10-22 10:28:16 +0800
Time.at(1571711626) # 2019-10-22 10:33:46 +0800

user = User.find_by account:"chenhanting"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.10.22 10:28:00', '2019.10.22 10:33:50', 3, 'poker')
````

第二場：（不知道自己在打什麼遊戲）

````ruby
# start time: '2019.10.22 10:36:00' 
# end time: '2019.10.22 10:51:00'

user = User.find_by account:"chenhanting"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.10.22 10:36:00', '2019.10.22 10:51:50', 3, 'poker')

WriteBetLogToMongoJob.new.perform
````

第三場：

````ruby
user = User.find_by account:"chenhanting"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.10.22 15:21:00', '2019.10.22 15:26:50', 3, 'poker')

WriteBetLogToMongoJob.new.perform
````

遊戲紀錄：

````json
{
            "gameNo" => [
        [0] "50-1571711478-8082825-2",
        [1] "50-1571711536-8082905-1",
        [2] "50-1571712504-8084445-3",
        [3] "50-1571712558-8084528-1"
    ],
    "PlayerFullName" => [
        [0] "800040_DFCTrmg1803-chenhanting",
        [1] "800040_DFCTrmg1803-chenhanting",
        [2] "800040_DFCTrmg1803-chenhanting",
        [3] "800040_DFCTrmg1803-chenhanting"
    ],
           "classNo" => [
        [0] 6001,
        [1] 6001,
        [2] 6001,
        [3] 6001
    ],

    ...

           "endTime" => [
        [0] "2019-10-22 10:32:00",
        [1] "2019-10-22 10:33:00",
        [2] "2019-10-22 10:49:07",
        [3] "2019-10-22 10:50:12"
    ],
         "cardValue" => [
        [0] "00c0b,127343b,2231423,3153a19",
        [1] "0050716,116040d,2261c19,3383a",
        [2] "01a0335,13a38,216140d,3033d1337",
        [3] "03b1b,10d3b,2223b26,32c1a"
    ],
         "channelNo" => [
        [0] 800040,
        [1] 800040,
        [2] 800040,
        [3] 800040
    ],
          "siteCode" => [
        [0] "800040_DFCTrmg",
        [1] "800040_DFCTrmg",
        [2] "800040_DFCTrmg",
        [3] "800040_DFCTrmg"
    ]
}
````

如何處理遊戲紀錄：

````ruby
> bets.keys   # 所有投注記錄的key值
> bets.values # 所有投注記錄的value值

def gen_kv(hash)
  new_list = []
  hash.values.each_with_index do |value, index|
    value.each_with_index do |v, i|
      new_list << {} if new_list[i].nil? 
      key = hash.keys[index]   # key 值
      new_list[i][key] = v     # 給予hash一對key,value
    end
  end
  new_list
end

# 更好的寫法(Brohand)
def gen_kv(hash)
  new_list = []
  hash.values.each_with_index do |value, index|
    value.each_with_index do |v, i|
      new_list[i] ||= {}
      key = hash.keys[index]   # key 值
      new_list[i][key] = v     # 給予hash一對key,value
    end
  end
  new_list
end

# 更好的寫法(Peter)
# data = { a: [1, 2, 3, 4], b: [5, 6, 7, 8], c: [9, 10, 11, 12] }
# result = [{ a: 1, b: 5, c: 9 }, { a: 2 , b: 6, c: 10 }, { a: 3, b: 7, c: 11 }, { a: 4, b: 8, c: 12 }]
values = data.values # [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
first = values.shift # values = [[5, 6, 7, 8], [9, 10, 11, 12]], first = [1, 2, 3, 4]
keys = data.keys # [:a, :b, :c]

# [1, 2, 3, 4].zip([5, 6, 7, 8], [9, 10, 11, 12])
# [[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]
final = first.zip(*values).map do |arr|
  # [:a, :b, :c].zip([1, 5, 9])
  # [[:a, 1], [:b, 5], [:c, 9]]
  # { a: 1, b: 5, c: 9 }
  keys.zip(arr).to_h
end

````

有時登入網站時，會出現mysql的錯誤，重新啟動mysql即可，附上重啟`mysql`的參考網址：https://thoughtbot.com/blog/starting-and-stopping-background-services-with-homebrew

投注記錄(mongodb)：

````ruby
GameLog.methods
GameLog.delete_all

user = User.find_by account:"chenhanting"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.10.22 15:21:00', '2019.10.22 15:26:50', 3, 'poker')
WriteBetLogToMongoJob.new.perform
````

顯示後台的投注記錄：`admin/mongo_bet_histories/index.html.erb`

bet_log.rb中

- @log為進入mongodb前的值

- @history為進入mongodb後的值

````ruby
# :_id => BSON::ObjectId('5dafcbf39571903f276ea12a')
GameLog.find('5dafcbf39571903f276ea12a')
obj = BetLog::RMG.new history: GameLog.find('5dafcbf39571903f276ea12a')
obj.parse_game_name # 德州撲克

GameLog.find_by(category: "poker")
````

record_to_mongoDB方法：

````ruby
def record_to_mongoDB(bets)
  mongo_count, not_existing_users = 0, {}
  if bets.present?
    puts "\n====== #{bets.size} IBC Records Need to Process ======\n"
    bets.each do |bet|
      game_account = get_account(bet)
      user = UserGameInfo.rmg.find_by(account: game_account)&.user
      mongo_bet = GameLog.find_or_create_by(partner: 'rmg', bet_num: parse_game_id(bet))
      mongo_params = {
        account: user.account,
        ...
        win_or_loss: bet['profitScore'],
      }
      
      mongo_count += 1 if mongo_bet.update(mongo_params)
    end
  end
  
  [mongo_count, not_existing_users]
end
````

看`user_game_info.rb`使用enum

````ruby
# user_game_info.rb
class UserGameInfo < ApplicationRecord
  belongs_to :user
  enum partner: GameStatPartner # from jvd.rb
end

# user_game_infos

# jvd.rb
GameStatPartner = {
  :allbet        => 1,
  :ebet          => 2,
  ...
  :rmg           => 36,
}
````

rails console

````ruby
UserGameInfo.partners
{
           "allbet" => 1,
             "ebet" => 2,
              ...
              "rmg" => 36
}

UserGameInfo.rmg

UserGameInfo Exists (0.4ms)  SELECT  1 AS one FROM `user_game_infos` WHERE `user_game_infos`.`partner` = 36 LIMIT 1
UserGameInfo Load (0.3ms)  SELECT `user_game_infos`.* FROM `user_game_infos` WHERE `user_game_infos`.`partner` = 36
[
    [0] #<UserGameInfo:0x00007fe604601660> {
                :id => 294,
           :user_id => 1803,
           :partner => "rmg",
           :account => "DFCTrmg1803-chenhanting",
          :password => nil,
             :token => nil,
        :created_at => Fri, 18 Oct 2019 16:15:23 CST +08:00,
        :updated_at => Fri, 18 Oct 2019 16:15:23 CST +08:00
    },

    ...
  
    [3] #<UserGameInfo:0x00007fe604601250> {
                :id => 298,
           :user_id => 1840,
           :partner => "rmg",
           :account => "DFCTrmg1840-chenhan789",
          :password => nil,
             :token => nil,
        :created_at => Fri, 18 Oct 2019 22:29:40 CST +08:00,
        :updated_at => Fri, 18 Oct 2019 22:29:40 CST +08:00
    }
]

UserGameInfo.partners[:rmg]  #36
````

`UserGameInfo`：圖中的36代表大富翁棋牌遊戲

![image-20191024103247581](/Users/hanting/Library/Application Support/typora-user-images/image-20191024103247581.png)

強制停止"處理中"狀態

````ruby
user = User.find_by account:'jjjjjjjjjiiiiiiiii'

# 查看正在處理中（轉入、轉出）的訂單
Client::GameWallet.where(user_id: 1841)

# 刪除正在處理中（轉入、轉出）的訂單
Client::GameWallet.where(user_id: 1841, name: 'rmg')[0].job_done! 
````

存入大筆的金額

````shell
# 會進入到
/Users/hanting/fatcat/app/jobs/wallet_order/transfer_job.rb

/Users/hanting/fatcat/app/jobs/wallet_order/check_job.rb 
WalletOrder::RescueJob.create(game_wallet, game_order)
````

註冊、登入：

````ruby
user = User.find_by account:'jjjjjjjjjiiiiiiiii'
GamesV2::RMG.new(user).create
GamesV2::RMG.new(user).login(1234)
````

查詢餘額

````ruby
user = User.find_by account:'jjjjjjjjjiiiiiiiii'
GamesV2::RMG.new(user).balance
````

轉入、轉出

````ruby
user = User.find_by account:'jjjjjjjjjiiiiiiiii'

# 創立新訂單
order = Client::GameOrder.create(user_id: 1841, partner: "rmg", order_type: "deposit", order_num: %(han#{Random.new_seed}), amount: 1.0, status: "new_order")

# 轉入訂單
GamesV2::RMG.new(user).transfer(order)
````

確定訂單狀態

````ruby
# 使用self.CheckState查看結果
check = GamesV2::RMG.new(user).check(Client::GameOrder.where(partner: "rmg")[57])
check = GamesV2::RMG.new(user).check(order)
order = Client::GameOrder.where(partner: "rmg")[57]
GamesV2::RMG.CheckState(check, order)
````

強制停止"處理中"狀態

```ruby
user = User.find_by account:'jjjjjjjjjiiiiiiiii'

# 查看正在處理中（轉入、轉出）的訂單
Client::GameWallet.where(user_id: 1841)

# 刪除正在處理中（轉入、轉出）的訂單
Client::GameWallet.where(user_id: 1841, name: 'rmg')[0].job_done! 
```

創立新帳號

````ruby
user = User.create(account: "chenhantingilove", balance: "99999999.00", company_id:2, status:1, user_type:1, password: "1234567890")
````

遊戲紀錄

````ruby
user = User.find_by account: "chenhanting"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.10.25 12:07:00', '2019.10.25 12:10:00', 3, 'poker')
WriteBetLogToMongoJob.new.perform
````

自動排程：`http://127.0.0.1:3000/cron_jobs` 

| Name       | Klass                   | Queue | Cron        | Status  | Args              |
| ---------- | ----------------------- | ----- | ----------- | ------- | ----------------- |
| mg BetLog  | Scheduled::GetBetLogJob | mg    | */5 * * * * | enabled | {"partner":"mg"}  |
| rmg BetLog | Scheduled::GetBetLogJob | rmg   | */5 * * * * | enabled | {"partner":"rmg"} |

控制器：`controllers/cron_jobs_controller.rb`

````ruby
class CronJobsController < AdminController
  before_action :set_cron_job, only: %i[edit update destroy]
  before_action :fetch_jobs, only: %i[fetch]
  layout 'main_for_admin'

  # GET /cron_jobs
  def index
    @cron_jobs = CronJob.all
  end

  # GET /cron_jobs/new
  def new
    @cron_job = CronJob.new
  end

  # GET /cron_jobs/1/edit
  def edit; end

  # POST /cron_jobs
  def create
    @cron_job = CronJob.new(cron_job_params)
    if @cron_job.save
      redirect_to cron_jobs_url, notice: 'Cron job was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cron_jobs/1
  def update
    if @cron_job.update(cron_job_params)
      redirect_to cron_jobs_url, notice: 'Cron job was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cron_jobs/1
  def destroy
    @cron_job.destroy
    redirect_to cron_jobs_url, notice: 'Cron job was successfully destroyed.'
  end

  def sync
    SyncCronJobService.sync_jobs!
    redirect_to cron_jobs_url, notice: 'Cron job was successfully Synced.'
  end

  # def fetch
  #   redirect_to cron_jobs_url, notice: 'Cron job was successfully Fetched.'
  # end

  private

  def set_cron_job
    @cron_job = CronJob.find(params[:id])
  end

  def cron_job_params
    params.require(:cron_job)
          .permit(:name, :klass, :queue, :cron, :status, :args, :last_enqueue_time)
  end

  def fetch_jobs
    Sidekiq::Cron::Job.all.each do |job|
      local_job = CronJob.find_by(name: job.name)
      next if local_job.nil?
      local_job.update last_enqueue_time: job.last_enqueue_time,
                       status: job.status
    end
  end
end
````

畫面：`views/cron_jobs/index.html.erb`

````erb
<%= link_to 'New Cron Job', new_cron_job_path, class: 'btn' %>
<%= link_to 'Sync Cron Job', sync_cron_jobs_path, method: :post, class: 'btn' %>
<%#= link_to 'Fetch Cron Job', fetch_cron_jobs_path, method: :post, class: 'btn' %>
````

### 其他

1. 千分位符：使用javascript 

   ```coffeescript
   // 轉千分位符
    toCurrency = (num) ->
      parseFloat(num).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
   
   // 將逗點去除
    removeCommon = (text) ->
      text.replace(/[,]+/g,"");
   ```

2. enum用法：https://rubyinrails.com/2019/04/12/how-to-get-integer-value-from-enum-in-rails/
3. 若出現 ActionController::InvalidAuthenticityToken in Admin::GameOrdersController#retry，則重新啟動 redis：`brew services restart redis`