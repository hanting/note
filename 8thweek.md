# The 8th Week

以下為到職第8週的紀錄，這週重點

- 了解IMOne的：PT遊戲
- 了解MG, Qtech遊戲並重構MG遊戲
- 了解Strategy流程
- 本地安裝influxdb
- 大富翁棋牌上測試站

### PT遊戲：

PT 遊戲 set_merchant_code

````ruby
# services/games/imone.rb

def set_merchant_code(user_or_key)
  if user_or_key.is_a? User
    # ! 有 user 拿 user.company 的 key
    user = user_or_key
    @keys = user.company.game_key(@partner)
  else
    # ! 沒 user 拿帶入的 key
    user = nil
    @keys = user_or_key
  end
  [user, @keys[:merchant_code]]
end
````

pt 有帳號密碼：user.pt_pwd

````ruby
class AddColumnsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :pt_account, :string  # 帳號
    add_column :users, :pt_pwd, :string      # 密碼
  end
end
````

從畫面進入 login(device, options = {})，取得 options 為以下數值。若 options 沒有 game_id 則進入遊戲大廳

````ruby
options = {
    :category => :egame,
      :params => {
          :auth_token => "3vY9txgP3JKWDjzj5P6dQWJKhxCWVRdJ",
              :device => "desktop",
              :locale => "zh-TW",
          :user_agent => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
        :company_name => "FATCAT"
    }
}

user = User.find_by account: "chenhanting"
pt = GamesV2::PT.new(user)
pt.login("desktop", options)
````

如何進入遊戲大廳（目前自建大廳的遊戲共有`mg`, `pt`, `qt`, `gamatron`）

```ruby
# games_v3/mg.rb
def login(device = nil, options = {})
  @opts = options
  # 如果沒有 game_type，則先導入自己的 game_list，意即進入遊戲大廳
  # opt_game_id 讀取game_id
  return gems_list(@user, device, opt_auth_token) if opt_game_id.blank?

  begin
    # 進入遊戲
    sty_flow(__method__)
  rescue => e
    Rails.logger.egame_qtech.debug(e.message)
  end
end

# app/services/games_v2/helper.rb 進入遊戲大廳
def gems_list(user, device, auth_token, game_key = const_game_key)
  egame = user.company.company_games.includes(:game).find_by(game_sources: { category: :egame, partner: game_key })

  return if egame.blank?

  query = {
    auth_token: auth_token,
    locale: I18n.locale,
    device: device
  }.to_query

  "#{user.company.game_host}/egame/#{game_key}?#{query}"
end

# app/services/games_v2/common/unit.rb
def opt_auth_token
  @opts.dig(:params, :auth_token)
end
```

執行成功之後傳出去的值為 => "https://www.fatcatbet.net/egame/pt?auth_token=3vY9txgP3JKWDjzj5P6dQWJKhxCWVRdJ&device=desktop&locale=en"

@opts.dig(:params, :auth_token) 找不到auth_token，需改為 @opts.dig(:params, "auth_token")

egame：

````ruby
egame = user.company.company_games.includes(:game).find_by(game_sources: { category: :egame, partner: game_key })

> egame 
#<Game::CompanyGame:0x00007fe081291148> {
                :id => 90,
              :name => "PT电子",
        :company_id => 2,
    :game_source_id => 77,
     :secret_key_id => 11,
            :status => "active",
             :order => 1,
     :wallet_active => true,
          :hot_game => nil,
             :image => #<ImageUploader:0x00007fe081152b38 ...,
      :image_mobile => nil,
         :image_app => #<ImageUploader:0x00007fe081152610 ...>,
        :created_at => Thu, 08 Mar 2018 18:34:39 CST +08:00,
        :updated_at => Wed, 22 May 2019 15:59:50 CST +08:00,
     :allowed_trial => true,
    :is_maintenance => false
}
````

後來發現創建新帳號

````ruby
user = User.create(account: "xxxjjjjjj", balance: "99999999.00", company_id:2, status:1, user_type:1, password: "1234567890")

options = {
    :category => :egame,
      :params => {
          :auth_token => "3vY9txgP3JKWDjzj5P6dQWJKhxCWVRdJ",
              :device => "desktop",
              :locale => "zh-TW",
          :user_agent => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36",
        :company_name => "FATCAT"
    }
}

pt = GamesV2::PT.new(user)
pt.login("desktop", options)
````

最主要要改的為語系跟幣別

````ruby
# qtech.rb

def currency
  @currency ||= @user.company.currency
end
````

````ruby
# games/imone.rb 
# imone:ag,esportsbull,imsb,pt,sunbet < GamesV2::Imone < Games::Imone

class Games::Imone
  require 'rest-client'
  # 飛雷神
  CURRENCIES = [
    :CNY, # 0 人民幣
    :USD, # 1 美元
    ...
    :KRW, # 8 韓元
  ]
  CURRENCY = CURRENCIES[0].to_s # 人民幣先
  
  ...
    
  def set_language
    %i(zh-CN zh-TW).include?(I18n.locale) ? "ZH-CN" : "EN"
  end

  ...
    
end
````

表格：使用語系、幣別

|      | mg              | pt(繼承自Games::Imone) | qtech                    |
| ---- | --------------- | ---------------------- | ------------------------ |
| 語系 | v @lang（壞了） | v @language(繼承)      | v hardcode               |
| 幣別 | v hardcode      | v hardcode             | v @currency（缺mapping） |

表格：使用語系、幣別

|      | mg                                                           | pt(繼承自Games::Imone)                                       | qtech |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | ----- |
| 語系 | en: English,<br />zh: Simplified Chinese, <br />zh_TW: Traditional Chinese,<br />ko: Korean, <br />ja: Japanese, <br />th: Thailand | en: English,<br />zh-cn: Simplified Chinese, <br />ch: Traditional Chinese,<br /> | zh_CN |
| 幣別 | USD <br />SGD <br />CNY <br />THB <br />VND <br />IDR <br />KRW <br />JPY <br />TWD <br />EUR <br />GBP <br />INR <br />MYR <br />ZAR | 沒有                                                         | CNY   |

GameV2所使用的module

````ruby
include GamesV2::Helper
include GamesV2::Common::Unit
include GamesV2::Common::Strategy
include GamesV2::QtechObj::Api

# 我判斷一該要寫在GamesV2::Helper裡面
````

`app/services/games_v2/helper.rb`的語系、幣別

````ruby
module GamesV2
  module Helper
    ...
    def currency(user, option = {})
      currency = option&.dig(user.company.currency) || user.company.currency
      @currency ||= currency
    end

    def lang(option = {})
      lang = option&.dig(I18n.locale) || I18n.locale
      @lang ||= lang
    end
  end
end
````

mg 遊戲內使用 games_v2 helper

````ruby
class Mg
  include GamesV2::Helper

  MG_DOMAIN = 'https://ag.adminserv88.com'
  MG_MEMBER_BASE_URL = MG_DOMAIN + '/member-api-web/member-api'
  LANG = {
    :'zh-TW' => 'zh_TW',
    :'zh-CN' => 'zh',
  }.freeze

  def initialize(user_or_key = nil)
    ...
    build_lang LANG                       # 裏頭含 @lang
    build_currency @user.company.currency # 裏頭含 @currency
    ...
  end
end
````

進入遊戲

````ruby
user = User.find_by account: "chenhanting"
options = {
    :category => :egame,
      :params => {
        ...
        :game_id => "HAB-koigate",
    }
}
GamesV3::Qtech.new(user).login("desktop", options)
````

程式進入login，__method\__為 :login。

````ruby
    40: def login(device = nil, options = {})
    41:   @opts = options
    42:   # 如果沒有 game_type，則先導入自己的 game_list。意即進入自己血的遊戲大廳
    43:   return gems_list(@user, device, opt_auth_token) if opt_game_id.blank?
    44: 
    45:   begin
    46:     # 進入遊戲
=>  47:     sty_flow(__method__)
    48:   rescue => e
    49:     Rails.logger.egame_qtech.debug(e.message)
    50:   end
    51: end
````

程式進入點為sty_flow，`sty_flow`為 function為QTech的核心

````ruby
# games_v2/common/strategy.rb

    45: def sty_flow(action, sty_opt = {})
 => 46:   sty_init(action)
    47:   get_sty_flow(sty_opt).each do |k, v|
    48:     go_method = "sty_#{action}_#{k}".to_sym
    49:     go_method = v if !self.respond_to?(go_method)
    50:     self.send(go_method) if go_method.present?
    51:   end
    52:   sty_data
    53: end
````

回傳值為 sty_data

```ruby
def sty_data
  self.sty["sty_#{sty_action}".to_sym][:data]
end

def sty_data=(data = nil)
  self.sty["sty_#{sty_action}".to_sym][:data] = data
end
```

sty_init:

````ruby
# === === ===
# 初始化參數
def sty_init(action)
  self.sty ||= init_deep_hash
  self.sty[:current_action] = action
  self.sty["sty_#{action}".to_sym] = init_deep_hash
end

def init_deep_hash
  Hash.new{ |h,k| h[k] = Hash.new(&h.default_proc) }
end
````

經過sty_init後，self.sty 變為以下hash。self.class 為 GamesV3::Qtech 

````ruby
{
    :current_action => :login,
         :sty_login => {}
}
````

接著會進入到get_sty_flow的方法，若沒有執行過，sty_opt為空值，傳出 DEFAULT_STRATEGY

```ruby
def get_sty_flow(sty_opt)
  return sty_opt[:strategy] if sty_opt[:strategy].present?
  return self.class::STRATEGY if self.class::const_defined?(:STRATEGY)
  DEFAULT_STRATEGY
end
```

由於找得到 self.class::const_defined?(:STRATEGY)，最後回傳的值為

````ruby
STRATEGY = {
  token: :qtech_token,
  params: nil,
  perform: :strategy_perform,
  receiver: :strategy_receiver,
  handler: nil,
  cancel: :qtech_cancel,
}.freeze
````

### Strategy 流程：以`token`方法為例

進去 sty_flow，此時 go_method 為 :sty_token_token

```ruby
    45: def sty_flow(action, sty_opt = {})
    46:   sty_init(action)
 => 47:   get_sty_flow(sty_opt).each do |k, v|
    48:     go_method = "sty_#{action}_#{k}".to_sym
    49:     go_method = v if !self.respond_to?(go_method)
    50:     self.send(go_method) if go_method.present?
    51:   end
    52:   sty_data
    53: end
```

由於找不到 :sty_token_token ，轉而進入第一個方法 :qtech_token

````ruby
 # games_v2/qtech_obj/api.rb

     7: def qtech_token
 =>  8:   @token = sty_flow(:token, {
     9:     strategy: {
    10:       params: nil,
    11:       perform: :strategy_perform,
    12:       receiver: :strategy_receiver,
    13:       handler: nil,
    14:     }
    15:   })
    16: end
````

再進去 sty_flow，此時 go_method 為 :sty_token_params

````ruby
# games_v2/qtech_obj/api.rb

    73: def sty_token_params
 => 74:   @params = {
    75:     grant_type: 'password',
    76:     response_type: 'token',
    77:     username: key_username,
    78:     password: key_password,
    79:   }
    80: 
    81:   self.sty_params = {
    82:     method: :get,
    83:     url: "#{key_api_server_url}/v1/auth/token",
    84:     headers: {
    85:       params: @params,
    86:     },
    87:   }
    88: end
````

@params

````ruby
{
       :grant_type => "password",
    :response_type => "token",
         :username => "api_fatcat",
         :password => "aTidYS3V"
}
````

:sty_token 為 pars

````ruby
# games_v2/common/strategy.rb
    100: def sty_params=(pars = {})
 => 101:   self.sty["sty_#{sty_action}".to_sym][:params] = pars
    102: end
````

pars 為

````ruby
{
     :method => :get,
        :url => "https://api.qtplatform.com/v1/auth/token",
    :headers => {
        :params => {
               :grant_type => "password",
            :response_type => "token",
                 :username => "api_fatcat",
                 :password => "aTidYS3V"
        }
    }
}
````

進去 sty_flow，此時 go_method 為 :sty_token_perform，但因為找不到 :sty_token_perform，轉而進入 :strategy_perform

````ruby
    56: def strategy_perform
 => 57:   self.sty_res = RestClient::Request.execute(sty_params)
    58:   api_caller_save
    59: rescue RestClient::ExceptionWithResponse => err
    60:   raise CustomError.new(:rest_client_exception_with_response, err.response)
    61: end
````

step 進去

````ruby
    96: def sty_params
 => 97:   self.sty["sty_#{sty_action}".to_sym][:params]
    98: end
````

sty_params 為

````ruby
{
     :method => :get,
        :url => "https://api.qtplatform.com/v1/auth/token",
    :headers => {
        :params => {
               :grant_type => "password",
            :response_type => "token",
                 :username => "api_fatcat",
                 :password => "aTidYS3V"
        }
    }
}
````

因為game_id隨便代的關係，最後卡在 :strategy_perform 的方法裡面

```ruby
STRATEGY = {
  token: :qtech_token,
  params: nil,
  perform: :strategy_perform,
  receiver: :strategy_receiver,
  handler: nil,
  cancel: :qtech_cancel,
}.freeze
```

卡住的地方

````ruby
    56: def strategy_perform
 => 57:   self.sty_res = RestClient::Request.execute(sty_params)
    58:   api_caller_save
    59: rescue RestClient::ExceptionWithResponse => err
    60:   raise CustomError.new(:rest_client_exception_with_response, err.response)
    61: end
````

錯誤訊息

````shell
RestClient::ServerBrokeConnection: Server broke connection
from /Users/hanting/.rvm/gems/ruby-2.4.6/gems/rest-client-2.0.2/lib/restclient/request.rb:729:in `rescue in transmit'
````

`:strategy_receiver`

````ruby
def strategy_receiver(&block)
  if block_given?
    st = yield(sty_res.code, sty_res)
  else
    st = (sty_res.code == 200) ? :ok : :error
  end

  if st == :error
    raise CustomError.new(:sty_response_code_not_200, "response.code != 200")
  else
    self.sty_status = st
  end
end
````

`:qtech_cancel`

````ruby
# games_v2/qtech_obj/api.rb

# 撤銷 token
def qtech_cancel
  sty_flow(:cancel, {
    strategy: {
      params: nil,
      perform: :strategy_perform,
      receiver: :strategy_receiver,
      handler: nil,
    }
  })
end
````

qt_games 的 `model`：電子遊戲都有類似的資料表：詳情請看16thweek

````ruby
# xxxx_create_qt_games.rb
class CreateQtGames < ActiveRecord::Migration[5.1]
  def change
    create_table :qt_games do |t|
      t.string :name
      t.string :en_name
      t.string :game_id
      t.string :image
      t.timestamps
    end
  end
end

# schema.rb
create_table "qt_games", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
  t.string "name"
  t.string "en_name"
  t.string "game_id"
  t.string "image"
  t.datetime "created_at", null: false
  t.datetime "updated_at", null: false
end
````

以下為所有 QT 遊戲，寫在 qt.yml 中。

````yaml
_1x2-eraofgods: 众神时代
_1x2-neonfruitcityscape: 霓虹水果fourdivinebeasts: 四大神兽
_HAB-gangsters: 黑手党
...
_AUG-samuraiwarrior: 武士道
````

![image-20191031185658029](/Users/hanting/Library/Application Support/typora-user-images/image-20191031185658029.png)

### mg遊戲重構

首先先取得 mg 的 option 值：

從console 看 mg 的 log：

````powershell
Started GET "/loading?category=egame&locale=zh-TW&partner=mg" for 127.0.0.1 at 2019-11-01 10:42:32 +0800
Processing by Client::LoadingController#game as HTML
Parameters: {"category"=>"egame", "locale"=>"zh-TW", "partner"=>"mg"}

Rendering client/loading/game.html.erb within layouts/blank
Rendered client/loading/_loading_style.html.erb (1.8ms)
Rendered client/loading/game.html.erb within layouts/blank (34.8ms)
````

跟 pt 做比較

`````po
Started GET "/loading?category=egame&locale=zh-TW&partner=pt" for 127.0.0.1 at 2019-11-01 10:53:25 +0800
Processing by Client::LoadingController#game as HTML
Parameters: {"category"=>"egame", "locale"=>"zh-TW", "partner"=>"pt"}
  
Rendering client/loading/game.html.erb within layouts/blank
Rendered client/loading/_loading_style.html.erb (1.4ms)
Rendered client/loading/game.html.erb within layouts/blank (38.4ms)
Completed 200 OK in 296ms (Views: 56.6ms | ActiveRecord: 11.0ms)
`````

去 production 看進去 mg, pt, qt的URL

````tiddlywiki
mg：
https://www.fatcatbet.net/egame/game_list/hot?auth_token=CYVDGAAeor4rd9ZR5Rkessve4D3riSUn&locale=zh-TW&partner=mg

qt:
https://www.fatcatbet.net/egame/game_list/hot?auth_token=18nGsuYgVn8tzy1ND4yYxKyep7yyf45j&locale=zh-TW&partner=qtech

pt:
https://www.fatcatbet.net/egame/game_list/hot?auth_token=cM5xtAxvRN43ybzFzrPCHi2ywjsHLucg&locale=zh-TW&partner=pt
````

mg 大廳：

`````txt
http://127.0.0.1:3300/egame/game_list/hot?auth_token=9w4MsMK7oXeEiEq57aRadSY5ajNXgKQo&locale=zh-TW&partner=mg
`````

必須要將上面的host改成localhost才可以進去，egame大廳是我們肥貓自己寫的，所以一定可以進去，但可能host寫死導致localhost進不去！

### RMG上測試站

1. 首先在測試環境上，要去[KEY管理]新增遊戲key，並且在設定遊戲key上設定遊戲對應哪個key

   接著再去JV Diamand的遊戲管理 -> 設定遊戲公司去新增自己的遊戲， 如此一來新增遊戲便完成了

   再來就是sidekick.yml必須設定排程，以及在host/cron_jobs.rb必須新增自己遊戲的get_bet_log_hostory，並且去host/sidekiq加入自己的排程。

2. git clone git@git.fatcatbet.net:jvd/server-login.git  將其放到自己電腦中

3. cd ~ : 回到自己的家目錄（home）

4. sidekiq 網址：https://admin.1a2b333.com/sidekiq

   cron_jobs 網址：https://admin.1a2b333.com/cron_jobs

4. 看 log 網址：[https://log.1a2b333.com](https://log.1a2b333.com/)

5. 遊戲紀錄(staging)

   ```ruby
   user = User.find_by account: "chenhanting"
   key = user.company.game_key('rmg')
   job = GetBetLogJob.new.perform('rmg', key, '2019.11.01 18:30:00', '2019.11.01 18:50:00', 3, 5, 'poker')
   WriteBetLogToMongoJob.new.perform
   ```

   後來發現是 Ohm 改了 jobs/get_bet_log_jobs.rb

   ````ruby
   # jobs/get_bet_log_jobs.rb
   # 多了 key_id，所以在 staging 必須要多帶 key_id 值
   
   class GetBetLogJob < GameApplicationJob
     queue_as do
       "betlog_#{arguments.first}"
     end
   
     def perform(partner, key, start_time, end_time, retry_times, key_id, category)
       ...
     end
     ...
   end
   ````

6. 遊戲紀錄(localhost)：dev 分支

   ````ruby
   user = User.find_by account:"chenhanting"
   key = user.company.game_key('rmg')
   job = GetBetLogJob.new.perform('rmg', key, '2019.10.22 15:21:00', '2019.10.22 15:26:50', 3, 'poker')
   
   WriteBetLogToMongoJob.new.perform
   ````

7. 進入staging的rails console請下指令

   perl run.pl list_servers

   ````shell
   Username: han
   Password: 12345
   fatcat staging backend
   fatcat staging frontend
   fatcat staging frontend2
   vbo staging betlog
   vbo staging staging
   ````

   其他必要指令

   ````shell
   perl run.pl server_login fatcat staging backend
   cd rails/admin/current
   bundle exec rails c staging
   ````

8. 查看測試站的投注記錄（2筆）

   ![image-20191101203701904](/Users/hanting/Library/Application Support/typora-user-images/image-20191101203701904.png)

9. 投注記錄寫不進去是因為卡在 get_bet_log_job.rb 的 companies，值為 nil

   From: /Users/hanting/fatcat/app/jobs/get_bet_log_job.rb @ line 46 GetBetLogJob#perform:

   ````ruby
       41: 
       42:  if bets.present?
       43:    puts "\n#{Time.now.in_time_zone} Start updating #{partner} bet history..."
       44:    # write bet history to redis namespace
       45:    companies = Rails.application.secrets.company_group
    => 46:    if companies.present?
       47:      all_company_bets = CompaniesBetLog.new(bets, partner, companies).call
       48:      all_company_bets.each do |company_bets|
       49:        unless company_bets[1].blank?
       50:          redis_bet_log(company_bets[0].to_s).set("#{partner}-#{category}-#{time_flag}", company_bets[1].to_json)
       51:        end
   ````

10. influxdb 紀錄（以下畫面好像需要帳號密碼登入）

    ![image-20191101204524770](/Users/hanting/Library/Application Support/typora-user-images/image-20191101204524770.png)

    metrics 的 query：

    ````sql
    SELECT time, partner, action, http_method, params, return_code, response_data, url FROM "autogen"."api_log_test" WHERE $timeFilter AND partner = 'rmg'
    ````

    抓取紀錄的區間：

    ![image-20191101205042729](/Users/hanting/Library/Application Support/typora-user-images/image-20191101205042729.png)

### 其他

1. 肥貓代理系統要發PR：https://git.fatcatbet.net/jvd/agent-system/commits/dev

2. binding.pry 請善用next, step, finish

3. `step`指令：不斷進入一行ruby code的每一個方法

   舉例來說，他會先進去opt_game_id，再進去opt_auth_token，最後進去gems_list

   ```ruby
   return gems_list(@user, device, opt_auth_token, GAME_KEY) if opt_game_id.blank?
   ```

4. 創立新帳號

   ```ruby
   user = User.create(account: "ghghghghghghgh", balance: "99999999.00", company_id:2, status:1, user_type:1, password: "ghghghghghghgh")
   ```

5. 如何取得當前的語系

   ```ruby
   I18n.locale 
   # Get and set the current locale
   ```

   如何取得當前的幣別

   ```ruby
   user.company.currency
   # user 指的是玩家帳號
   ```

6. InfluxDB抓不到資料庫

   <InfluxDB::Error: {"error":"database not found: \"fatcat_test_db\""}

   安裝：https://docs.influxdata.com/influxdb/v1.7/introduction/installation/

   使用：https://docs.influxdata.com/influxdb/v1.7/introduction/getting-started/

   進入畫面：influx -precision rfc3339  

   ```sql
   CREATE DATABASE fatcat_test_db
   SHOW DATABASES
   USE fatcat_test_db
   
   SELECT * FROM /.*/ LIMIT 90 # 秀出90筆
   ```

   截取片段資訊：

   ```sql
   2019-10-30T09:54:02.857Z                 查詢帳號餘額(1003)                                                                                                                 GET  {:action=>1003, :data=>"{\"acc\":\"DFCT1803_03\"}", :channel=>"5", :tick=>1572429242, :secret=>"*****"}                                                                                                                                                                                                   gg           {"result":"1103.00000","secret":"ab35ec03ba9bc526a3e8de9b65d8e720"}                                                                                          200 https://sock3.gg137.com/api/v3?action=1003&data={%22acc%22:%22DFCT1803_03%22}&channel=5&tick=1572429242&secret=7179f07f606f4a2ae8853e1df10c78be
   2019-10-30T09:54:02.995Z                 /MemberBalance    {"carrier"=>{}, "code"=>"COMM0000", "msg"=>"Success.", "data"=>{"balance"=>1098.0, "currencyCode"=>"CNY"}} POST {:carrier=>{}, :data=>{:memberCode=>"DFCTchenhanting"}}  
   
   ...
   
   {"uid":"DFCTchenhanting","status":0,"balance":1097},"msg":"success"}                                                                        200 http://api.esport-bet.net/v1/user/info
   2019-10-30T09:54:03.484Z                 create                                                                                                                       
   ```

7. 與MG遊戲重構的程式碼，但已忘記為什麼會看

   app/api/helpers/common.rb

   ```ruby
   module Helpers::Common
     ...
     def current_user
       User.find_by_auth_token(params[:auth_token])
     end
   
     def current_company
       return @company if @company.presence || set_company(params[:company_name])
   
       error!({ status: 'ERROR', message: 'Company Not Found.' }, 404)
     end
     ...
   end
   ```

   games_v2/common/unit.rb

   ```ruby
   module GamesV2
     module Common
       module Unit 
         ...
         # === game_account === ===
         # 檢查 user 的值 game_account 是否存在
         def game_account?
           game_account.present?
         end
        
         def game_account
           @user&.send(const_game_account_cloumn)
         end
         ...
       end
     end
   end 
   ```

### 參考資料

1. grape：https://niclin.gitbooks.io/grape-on-rails-101/implement-access-token.html

