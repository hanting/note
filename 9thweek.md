# The 9th Week

以下為到職第9週的紀錄

- RMG 上測試站問題
  - 遊戲錢包圖片找不到的問題
  - 處理遊戲紀錄抓不到的問題
  - 遊戲紀錄開始時間、結束時間顯示錯誤
  - 遊戲紀錄抓取重複
    - 方法1：讓他不寫進redis
    - 方法2：讓它不寫進mogodb
- MG 遊戲重構
  - 登入遊戲或遊戲大廳
  - 了解 strategy 架構
  - 了解使用 RestClient



### RMG 遊戲(測試站)

#### 遊戲錢包圖片找不到的問題

大富翁棋牌的遊戲錢包圖片位置：`games/logo/rmg.png`

scss 顯示大富翁棋牌的錢包位置：

````scss
@each $name in $gwName {
  .#{$name} {
    .wallet-img {
      background-image: image-url("games/logo/#{$name}.png");
    }
  }
}
````

#### 處理遊戲紀錄抓不到的問題

使用測試站前，要先把電腦的ssh key交給Spensor，讓Spensor開啟使用遠端測試站的權限。至於正式站的權限，目前我們暫時先用不到

````shell
cat ~/.ssh/id_rsa.pub

# ssh key(my computer)
````

使用肥貓測試站的指令

````shell
perl run.pl server_login fatcat staging backend
cd rails/admin/current
bundle exec rails c staging
````

抓取遊戲紀錄，並將遊戲紀錄存放至mongodb

````ruby
user = User.find_by account:"chenhanting"
job = GetBetLogJob.new.perform('rmg', '隨便打', '2019.11.04 16:41:00', '2019.11.04 16:43:00', 3, 6, '隨便打')

WriteBetLogToMongoJob.new.perform
````

在測試站的 rails console 下指令，馬上就可以抓到遊戲紀錄，不過在cron_jobs卻無法抓到遊戲紀錄，原因是cron_jobs傳進sidekiq的格式錯誤，最後為 Peter 修正了 cron_jobs 無法抓到遊戲紀錄的狀況

透過以下指令可以看到大富翁棋牌遊戲所有的帳號密碼。其實egame 的 mg 遊戲帳密可以移至 UserGameInfo

````ruby
> UserGameInfo.rmg

  UserGameInfo Exists (0.8ms)  SELECT  1 AS one FROM `user_game_infos` WHERE `user_game_infos`.`partner` = 36 LIMIT 1
  UserGameInfo Load (0.6ms)  SELECT `user_game_infos`.* FROM `user_game_infos`
WHERE `user_game_infos`.`partner` = 36

[
    [0] #<UserGameInfo:0x000055b70e5acd08> {
                :id => 144,
           :user_id => 605,
           :partner => "rmg",
           :account => "TFCTrmg605-chenhanting",
          :password => nil,
             :token => nil,
        :created_at => Fri, 01 Nov 2019 17:59:09 CST +08:00,
        :updated_at => Fri, 01 Nov 2019 17:59:09 CST +08:00
    },
    [1] #<UserGameInfo:0x000055b70e5acbc8> {
        ...
    },
    [2] #<UserGameInfo:0x000055b70e5aca88> {
        ...
    }
]
````

Redis 相關指令

````ruby
> Rails.application.secrets.site_code
"FATCAT"

> Rails.env
"staging"

> Rails.application.config.redis_url
"redis://happycatproduction.kud4ml.0001.apne1.cache.amazonaws.com:6379/8"

> redis_connection = Redis.new(url: Rails.application.config.redis_url)
#<Redis client v4.0.1 for redis://happycatproduction.kud4ml.0001.apne1.cache.amazonaws.com:6379/8>

> Rails.application.secrets.company_group
{
       :VBO => "VBO",
    :FATCAT => "FCT"
}

> Redis::Namespace.new("#{Rails.env}-FATCAT-betlog", redis: redis_connection)
"<Redis::Namespace v1.6.0 with client v4.0.1 for redis://happycatproduction.kud4ml.0001.apne1.cache.amazonaws.com:6379/8/staging-FATCAT-betlog>"

> Redis::Namespace.new("#{Rails.env}-FATCAT-betlog", redis: redis_connection)
"<Redis::Namespace v1.6.0 with client v4.0.1 for redis://happycatproduction.kud4ml.0001.apne1.cache.amazonaws.com:6379/8/staging-FATCAT-betlog>"

> Redis::Namespace.new("staging-FATCAT-betlog", redis: redis_connection).keys
[]

# 如果要查看redis遊戲紀錄，也可以使用
namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.redis_url)
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection)

# 查看keys
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).keys

# 取得某一筆
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).get("rmg-game-1572855300")

# 刪除某一筆
Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection).del("rmg-game-1572922800")

# 刪除全部
namespace = Rails.application.secrets.site_code
redis_connection = Redis.new(url: Rails.application.config.redis_url)
redis = Redis::Namespace.new("#{Rails.env}-#{namespace}-betlog", redis: redis_connection)
redis.del(redis.keys('*'))
````

#### 遊戲紀錄開始時間、結束時間顯示錯誤

目前遇到的問題是在測試站和本地端，遊戲紀錄顯示的時間不一樣，所以要尋找是什麼樣的問題。先把遊戲紀錄全部刪除，以便觀察

````ruby
> GameLog.delete_all
````

查看 mongo_bet_histories/index.html.erb

````erb
<td class="text-center"><%= Time.zone.at(bet['start_time']).strftime("%Y-%m-%d %H:%M:%S") rescue '' %></td>
<td class="text-center">
  <% if is_confirmed?(bet) %>
    <%= Time.zone.at(bet['confirmed_at']).strftime("%Y-%m-%d %H:%M:%S") %>
  <% end %>
</td>
````

 問題為 bet['start_time'] 和 bet['confirmed_at']：時區

mongodb: Query by attribute

````ruby
> GameLog.first                  # 第一筆
> GameLog.where(partner: "rmg")  # 找所有partner為rmg的資料
"#<Mongoid::Criteria
  selector: {"partner"=>"rmg"}
  options:  {}
  class:    GameLog
  embedded: false>"
  
> GameLog.where(partner: "rmg").count # 看總比數
> GameLog.where(partner: "rmg")[0]    # 看第一筆

# 參考資料：https://stackoverflow.com/questions/9747224/rails-mongoid-querying-by-attribute

MONGODB | localhost:27017 | fatcat_dev.find | STARTED | {"find"=>"game_logs", "filter"=>{}, "sort"=>{"_id"=>1}, "limit"=>1, "singleBatch"=>true, "lsid"=>{"id"=><BSON::Binary:0x70101867223200 type=uuid data=0xa41b8dc2e2d4440f...>}}
MONGODB | localhost:27017 | fatcat_dev.find | SUCCEEDED | 0.001s

#<GameLog:0x00007f83ba439988> {
              :_id => BSON::ObjectId('5dc115989571907fffac229c'),
          :account => "chenhanting",
    :agent_account => "ping",
         :agent_id => 35,
       :bet_amount => 3.0,
          :bet_num => "50-1572927635-10015363-3",
         :category => "poker",
       :company_id => 2,
     :confirmed_at => 1572927668,
       :created_at => 2019-11-05 06:24:24 UTC,
          :game_id => "120020002",
        :game_name => "600",
      :game_result => "0252d36,2232c122a,3042a29",
        :game_type => "6001",
          :partner => "rmg",
         :raw_data => {
                "gameNo" => "50-1572927635-10015363-3",
        "PlayerFullName" => "800040_DFCTrmg1803-chenhanting",
               "classNo" => 6001,
                "TypeId" => 600,
                "deskNo" => 120020002,
                "seatNo" => 3,
             "playerNum" => 2,
            "validScore" => "3.00",
            "totalScore" => "3.00",
           "profitScore" => "-3.00",
            "shareScore" => "0.00",
             "startTime" => "2019-11-05 12:20:35",
               "endTime" => "2019-11-05 12:21:08",
             "cardValue" => "0252d36,2232c122a,3042a29",
             "channelNo" => 800040,
              "siteCode" => "800040_DFCTrmg"
    },
       :settled_at => 1572927668,
       :start_time => 1572927635,
       :updated_at => 2019-11-05 06:24:24 UTC,
          :user_id => "1803",
     :valid_amount => 3.0,
      :win_or_loss => -3.0
}
````

自己電腦的時區

````ruby
> Time.now
2019-11-05 18:42:20 +0800
> Time.current
Tue, 05 Nov 2019 18:42:25 CST +08:00

> Time.parse("2019-11-04 16:20:23")
2019-11-04 16:20:23 +0800
> Time.zone.parse("2019-11-04 16:20:23")
Mon, 04 Nov 2019 16:20:23 CST +08:00
> Time.zone.parse("2019-11-04 16:20:23").to_i
1572855623
> Time.parse("2019-11-04 16:20:23").to_i
1572855623
````

測試站的時區

````ruby
> Time.now
2019-11-05 10:44:09 +0000
> Time.current
Tue, 05 Nov 2019 18:44:16 CST +08:00

> Time.zone.parse("2019-11-04 16:20:23")
Mon, 04 Nov 2019 16:20:23 CST +08:00
> Time.parse("2019-11-04 16:20:23")
2019-11-04 16:20:23 +0000
> Time.parse("2019-11-04 16:20:23").to_i
1572884423
> Time.zone.parse("2019-11-04 16:20:23").to_i
1572855623
````

#### RMG 重複抓取遊戲紀錄問題

##### 方法1

如何 query 遊戲紀錄

```ruby
GameLog.where(bet_num:"50-1572924383-10010224-2").first
```

如何移除陣列其中一個 hash：https://stackoverflow.com/questions/10034678/how-can-i-delete-one-element-from-an-array-by-value

```ruby
# 第一版
result = [{ a: 1, b: 5 }, { a: 2 , b: 6 }, { a: 2, b: 7 }, { a: 4, b: 8 }]

repeated = []
result.each do |content|
  repeated.push content if content[:a].eql? 2
end

result -= repeated

# 第二版
result = [{ a: 1, b: 5 }, { a: 2 , b: 6 }, { a: 2, b: 7 }, { a: 4, b: 8 }]
result.delete_if {|content| content[:a].eql? 2 }

# 寫進 rmg
result  = gen_key_value_pair(body['c']['data']) 
result.delete_if do |content| 
  bet_num = content.dig('gameNo')
  bet_num.eql? GameLog.where(bet_num: bet_num).first 
end
```

##### 方法2

```ruby
mongo_bet = GameLog.find_or_initialize_by(partner: 'rmg', bet_num: bet['gameNo'])
next unless mongo_bet.new_record?  # 找到重複筆，跳下一筆不覆寫

mongo_bet = GameLog.find_or_create_by(partner: 'xj_agile_bet', bet_num: bet['id'])
# we will skip the bet log if it already be confirmed
next if mongo_bet.confirmed_at.present?
```

抓取遊戲紀錄

```ruby
user = User.find_by account:'jjjjjjjjjiiiiiiiii'
# 先抓取 14:30-14:41
# 再抓取 14:30-14:45

# 先抓取 14:30-14:41 玩一筆
user = User.find_by account:"jjjjjjjjjiiiiiiiii"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.11.07 14:30:00', '2019.11.07 14:41:00', 3, 'poker')
WriteBetLogToMongoJob.new.perform

# 再抓取 14:30-14:45 玩兩筆，其中一筆在 14:30-14:41 區間玩
user = User.find_by account:"jjjjjjjjjiiiiiiiii"
key = user.company.game_key('rmg')
job = GetBetLogJob.new.perform('rmg', key, '2019.11.07 14:30:00', '2019.11.07 14:45:00', 3, 'poker')
WriteBetLogToMongoJob.new.perform
```

![image-20191107145438805](/Users/hanting/Library/Application Support/typora-user-images/image-20191107145438805.png)

### MG 遊戲重構

strategy.rb的方法

````ruby
 # === === ===
 # 依序檢查 strategy flow 的參數
 #   1. sty_opt[:strategy]
 #   2. STRATEGY
 #   3. DEFAULT_STRATEGY
 #
 def get_sty_flow(sty_opt)
   return sty_opt[:strategy] if sty_opt[:strategy].present?
   return self.class::STRATEGY if self.class::const_defined?(:STRATEGY)
   DEFAULT_STRATEGY
 end

 # === === ===
 # 預設流程
 #
 # example
 # 如果 action = "login" 則會自動檢查以下 method 是否存在
 # - sty_login_params
 # - sty_login_perform  不存在則使用 strategy_perform
 # - sty_login_receiver 不存在則使用 strategy_receiver
 # - sty_login_handler
 #
 def sty_flow(action, sty_opt = {})
   sty_init(action)
   get_sty_flow(sty_opt).each do |k, v|
     go_method = "sty_#{action}_#{k}".to_sym
     go_method = v if !self.respond_to?(go_method)
     self.send(go_method) if go_method.present?
   end
   sty_data
 end

# 最後結果存在 sty_data
def sty_data
  self.sty["sty_#{sty_action}".to_sym][:data]
end

def sty_data=(data = nil)
  self.sty["sty_#{sty_action}".to_sym][:data] = data
end
````

進入sty_flow

````ruby
def login(device = nil, options = {})
  @opts = options
  # 如果沒有 game_type，則先導入自己的 game_list。意即進入遊戲大廳
  return gems_list(@user, device, opt_auth_token) if opt_game_id.blank?

  begin
    # 進入遊戲
=>  sty_flow(__method__)
  rescue => e
    Rails.logger.egame_qtech.debug(e.message)
  end
end

# 這裏的sty_opt，指的是option
````

以下為傳進登入的option

````ruby
user = User.find_by account: "chenhanting"
options = {
    :category => :egame,
    :game_id => "END",
    }
}

GamesV3::Mg.new(user).login("desktop", options)
````

如何讓mg導入遊戲大廳，改成以下狀況使得 localhost 可以使用，不過後來將game_host的值刪除就不用改code

````ruby
module GamesV2
  module Helper
    
    ...
    # === === ===
    # qtech, pt 這類型用來導入自己的大廳
    def gems_list(user, device, auth_token, game_key = const_game_key)
      egame = user.company.company_games.includes(:game).find_by(game_sources: { category: :egame, partner: game_key })

      return if egame.blank?

      query = {
        auth_token: auth_token,
        locale: I18n.locale,
        device: device,
      }.to_query

      "#{user.company.game_host}/egame/#{game_key}?#{query}"
    end
    ...
      
  end
end
````

````ruby
class AddGameHostToCompany < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :game_host, :string
  end
end
````

![image-20191117144636457](/Users/hanting/Library/Application Support/typora-user-images/image-20191117144636457.png)

`egame = user.company.company_games.includes(:game).find_by(...)` 為

````ruby
egame = {
                :id => 77,
              :name => "MG电子",
        :company_id => 2,
    :game_source_id => 70,
     :secret_key_id => 6,
            :status => "active",
             :order => 3,
     :wallet_active => true,
          :hot_game => nil,
             :image => #<ImageUploader:0x00007feb467615b8 ...>,
      :image_mobile => nil,
         :image_app => #<ImageUploader:0x00007feb467613b0 ...>,
        :created_at => Wed, 13 Dec 2017 17:59:36 CST +08:00,
        :updated_at => Fri, 25 Oct 2019 12:25:19 CST +08:00,
     :allowed_trial => true,
    :is_maintenance => false
}
````

一般來說標準流程在 DEFAULT_STRATEGY，若有較特殊狀況則將 STRATEGY 寫在遊戲中。`sty_opt[:strategy]`則沒有使用過。

````ruby
    21: def get_sty_flow(sty_opt)
 => 22:   return sty_opt[:strategy] if sty_opt[:strategy].present?
    23:   return self.class::STRATEGY if self.class::const_defined?(:STRATEGY)
    24:   DEFAULT_STRATEGY
    25: end
````

games/mg.rb

````ruby
# 此為 payload
def builder
  Nokogiri::XML::Builder.new do |xml|
    xml.send(@xml_call, @params)
  end
end

# 提供 login, game, balance, transfer 使用
def api_post  
  return :false unless block_given?

  uri = URI.parse(MG_MEMBER_BASE_URL)
  api = Net::HTTP::Post.new(uri, @headers)
  api.body = builder.doc.root.to_xml    # builder 在這裏

  response = https(uri).request(api)
  ApiCaller.save('POST', uri, @params, response,
    action: @action,
    account: @user.account,
    partner: 'mg')

  raise CustomError.new(:api_response_code_not_200, 'response.code != 200') if response.code != '200'

  res = Hash.from_xml(response.body)[@xml_resp]
  yield(res)
end
````

rails console 使用 xml ，builder.doc.root.to_xml 為之後的 payload

````ruby
a = Nokogiri::XML::Builder.new do |xml|
     xml.send('qwer', 'tyuiop')
    end

# 輸出為：
# <Nokogiri::XML::Builder:0x00007f83c222d150 @doc=#<Nokogiri::XML::Document:0x3fc1e1116218 name="document" children=[#<Nokogiri::XML::Element:0x3fc1e11139dc name="qwer" children=[#<Nokogiri::XML::Text:0x3fc1e1112564 "tyuiop">]>]>, @parent=#<Nokogiri::XML::Document:0x3fc1e1116218 name="document" children=[#<Nokogiri::XML::Element:0x3fc1e11139dc name="qwer" children=[#<Nokogiri::XML::Text:0x3fc1e1112564 "tyuiop">]>]>, @context=nil, @arity=1, @ns=nil>

> a.doc
"<?xml version="1.0"?>
<qwer>tyuiop</qwer>"
  
> a.doc.root
"<qwer>tyuiop</qwer>"

> a.doc.root.to_xml
"<qwer>tyuiop</qwer>"
````

strategy 流程：

````ruby
 require 'rest-client'
 # 定義預設流程
 DEFAULT_STRATEGY = {
   params: nil,                  # 傳遞參數
   perform: :strategy_perform,   # 打出api，並將紀錄儲存至influxdb
   receiver: :strategy_receiver, # 接收api，並傳成功或失敗的狀態
   handler: nil,                 # 整理、取出資料(url等)
 }.freeze

 def strategy_perform
   self.sty_res = RestClient::Request.execute(sty_params)
   api_caller_save
 rescue RestClient::ExceptionWithResponse => err
   raise CustomError.new(:rest_client_exception_with_response, err.response)
 end

 def strategy_receiver(&block)
   if block_given?
     st = yield(sty_res.code, sty_res)
   else
     st = (sty_res.code == 200) ? :ok : :error
   end

   if st == :error
     raise CustomError.new(:sty_response_code_not_200, "response.code != 200")
   else
     self.sty_status = st
   end
 end

 def api_caller_save
   ApiCaller.save(
     sty_params[:method],
     sty_params[:url],
     (@params || sty_params[:payload]),  # payload解釋如下
     sty_res,
     {
       action: sty_action,
       account: @account,
       partner: @partner.to_s,
     }
   )
 end
````

strategy_perform, strategy_receive 為通用方法，一般來說沒有例外的話，預設用strategy_perform, strategy_receive。strategy_receiver 的 block 使用時機為利用 sty_\<action>\_receiver 呼叫 strategy_receiver 的 block

mg遊戲有game的行為，去game_controller 只看到 game 和進入大廳有關係。 mg 遊戲走 go_to_game，其他遊戲走 go_to_game_v2

````ruby
# app/controllers/client/games/game_controller.rb

def go_to_game_v2
  game = SimpleGameFactory.create_game(@partner, current_user)
  redirect_to game.login( current_device,
                          params: params.permit(:locale, :trial, :partner, :game_id),
                          category: 'egame')
rescue GamesV2::Error => e
  msg = if e.type == GamesV2::Error::NOT_EXISTING
          I18n.t('controller.client.games.game_no_exist')
        else
          I18n.t('controller.client.games.maintenance')
        end
  redirect_to maintenance_path, alert: msg
end

# app/controllers/client/games/game_controller.rb

def go_to_game
  case @partner
  when 'mg'
    game_url = Mg.new(current_user)
                 .game(params[:game_id],    # games/mg.rb
                       request.base_url,
                       trial: params[:trial])
    if game_url
      redirect_to game_url
    else
      redirect_to egame_path(partner: @partner), alert: I18n.t('controller.client.games.can_not_create_account', partner: @partner)
    end
  else
    go_to_game_v2
  end
end

# games/mg.rb

def game(game_id, host, trial: false)
  @game_id = game_id
  @host = host
  set_api(trial ? :demo : __method__)
  api_post do |res|
    res['launchUrl'] if res['status'] == '0'
  end
end
````

紀錄go_to_game的參數：

````ruby
> current_user
account: "chenhanting"

> params[:game_id]
"46704"

> request.base_url
"http://127.0.0.1:3300"

# 試玩
> params[:trial]
nil # true or nil

> game_url
"https://redirect.dirdelivery.com/Casino/Default.aspx?applicationid=163&serverid=2487&gamename=dragonDance&languagecode=en&ul=en&lobbyname=mobile88&loginType=VanguardSessionToken&authToken=0WrrXNzvInFTOp0cKJDRC1Si_PryCxWhOpc9wkgx-d01fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu&lobbyURL=http%3A%2F%2F127.0.0.1%3A3300%2Fegame%2Fmg&bankingURL=http%3A%2F%2F127.0.0.1%3A3300%2Fclient%2Fwallets&showva=playcheck"
````

trial 是試玩的意思

````ruby
> request.parameters
{
    "auth_token" => "jmnfavLQX5iCgGQV1UmjUfGS1aJVokVx",
        "locale" => "zh-CN",
         "trial" => "true",
       "partner" => "mg",
    "controller" => "client/games/egame",
        "action" => "go_to_game",
       "game_id" => "46704"
}

> options
{
      :params => {
         "locale" => "zh-CN",
          "trial" => "true",
        "partner" => "mg",
        "game_id" => "46704"
    },
    :category => "egame"
}
````

Ohm 提到問題是我在 games_v3/mg_obj 的 sty_create_handler 上面寫 GamesV3.Mg.new(@user).create 要寫成 create才可以，不然的話 @mg_account 會被重置

````ruby
# === handler: 整理、取出資料、或是回寫DB
def sty_create_handler
  res = json_parser(sty_res)
  if res['success'] == true
    @user.update(mg_account: @mg_account, mg_pwd: @mg_pwd)
    return :ok
  elsif (res['success'] == false) && (res['message'] == 'Username existed.')
    @mg_account.next!
    GamesV3.Mg.new(@user).create  # self 為 GamesV3::Mg
  end
end

# 應該要寫成這樣才對
def sty_create_handler
  res = json_parser(sty_res)
  if res['success'] == true
    @user.update(mg_account: @mg_account, mg_pwd: @mg_pwd)
    return :ok
  elsif (res['success'] == false) && (res['message'] == 'Username existed.')
    @mg_account.next!
    create  # self 為 GamesV3::Mg
  end
end
````

api_post為何？

先複習標準流程

````ruby
 # - sty_login_params
 # - sty_login_perform  不存在則使用 strategy_perform
 # - sty_login_receiver 不存在則使用 strategy_receiver
 # - sty_login_handler
````

````ruby
# games/mg.rb

# 產出 @user_token
def login
  set_api(__method__)
  api_post do |res|
    res['token'] if res['status'] == '0'
  end
end

def api_post
  return :false unless block_given?

  # sty_<...>_perform
  uri = URI.parse(MG_MEMBER_BASE_URL)       # URL
  api = Net::HTTP::Post.new(uri, @headers)  # 需要 @headers
  api.body = builder.doc.root.to_xml        # 需要 @params, @xml_call
  
  # sty_<...>_receiver
  response = https(uri).request(api)

  raise CustomError.new(:api_response_code_not_200, 'response.code != 200') if response.code != '200'

  res = Hash.from_xml(response.body)[@xml_resp]
  yield(res)
end

# 其中builder 和 https
def builder
  Nokogiri::XML::Builder.new do |xml|
    xml.send(@xml_call, @params)
  end
end

def https(uri)
  https = Net::HTTP.new(uri.host, uri.port)
  https.use_ssl = true
  https
end

# 大家如何用 api_post，舉例來說
api_post do |res|
  res['token'] if res['status'].eql? '0'
end
````

自己試試看使用 block

````ruby
# 失敗(寫錯了)
{a: 1, b: 2, c: 3} do |res|
  res[:a] if res[:b].zero?
end

# 失敗(寫錯了)
{a: 1, b: 0, c: 3} do |res|
  res[:a] if res[:b].zero?
end
````

````ruby
# 成功
def my_method
  puts "reached the top"
  yield
  puts "reached the bottom"
end

my_method do
  puts "reached yield"
end
````

````ruby
# 成功（回傳:'Ilovemyself'）
def gen_user_token
  res = {'token'=> 'Ilovemyself', 'status' => '0'}
  yield(res)
end

gen_user_token do |response|
  response['token'] if response['status'].eql? '0'
end

# 成功（回傳: nil）
def gen_user_token
  res = {'token'=> 'Ilovemyself', 'status' => '1'}
  yield(res)
end

gen_user_token do |response|
  response['token'] if response['status'].eql? '0'
end
````

自己執行xml 相關的腳本

````ruby
MG_DOMAIN = 'https://ag.adminserv88.com'
MG_MEMBER_BASE_URL = MG_DOMAIN + '/member-api-web/member-api'
@headers = {
  'Content-Type' => 'application/xml',
  'X-Requested-With' => 'X-Api-Client',
  'X-Api-Call' => 'X-Api-Client'
}
@params = {
  :timestamp => Time.current.utc.to_s,
  :apiusername => 'apiadmin',
  :apipassword => 'apipassword',
  :token => nil,
  :language => 'zh-TW',
  :gameId => "46704",
  :bankingUrl => "http://127.0.0.1:3300/client/wallets",
  :lobbyUrl => "http://127.0.0.1:3300/egame/mg",
  :logoutRedirectUrl => "http://127.0.0.1:3300",
  :demoMode => 'true',
} 

def builder
  Nokogiri::XML::Builder.new do |xml|
    xml.send('mbrapi-changecredit-call', @params)
  end
end

def https(uri)
  https = Net::HTTP.new(uri.host, uri.port)
  https.use_ssl = true
  https
end

# def gen_user_token
  uri = URI.parse(MG_MEMBER_BASE_URL)
  api = Net::HTTP::Post.new(uri, @headers)  # 執行失敗
  api.body = builder.doc.root.to_xml        # 執行成功（？？？）
  response = https(uri).request(api)
  
  res = Hash.from_xml(response.body)['mbrapi_changecredit_resp']
#  yield(res)
# end

gen_user_token do |res|
  res['token'] if res['status'].eql? '0'
end
````

進入 gen_user_token

````ruby
user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user)
mg.gen_user_token
````

進入 login

````ruby
options =
{
      :params => {
         locale: "zh-CN",
          trial: "true",
        partner: "mg",
        game_id: "46704"
    },
    :category => "egame"
}

user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).login('desktop', options)
````

### MG 重構

Mg（第一版） 進入遊戲的一些參數

````ruby
> @user_token
"MNNwVAEfhEi-4-cRXZ3Sl0yl9Bv-ArQc2MN_Cis-M7Q1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu"

> @params
{
            :timestamp => "2019-11-07 09:26:57 UTC",
          :apiusername => "apiadmin",
          :apipassword => "apipassword",
                :token => "MNNwVAEfhEi-4-cRXZ3Sl0yl9Bv-ArQc2MN_Cis-M7Q1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu",
             :language => "en",
               :gameId => "46704",
           :bankingUrl => "http://localhost:3300/client/wallets",
             :lobbyUrl => "http://localhost:3300/egame/mg",
    :logoutRedirectUrl => "http://localhost:3300",
             :demoMode => "false"
}

> @headers
{
        "Content-Type" => "application/xml",
    "X-Requested-With" => "X-Api-Client",
          "X-Api-Call" => "X-Api-Client"
}

> api.body
"<mbrapi-launchurl-call timestamp=\"2019-11-07 08:55:39 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" token=\"2Igd5a9X1Z1kXfEscH_W24cHPV6WjUkFyVd5PC-wwLU1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu\" language=\"en\" gameId=\"46704\" bankingUrl=\"http://localhost:3300/client/wallets\" lobbyUrl=\"http://localhost:3300/egame/mg\" logoutRedirectUrl=\"http://localhost:3300\" demoMode=\"false\"/>"

> response
{
                 "date" => [
        [0] "Thu, 07 Nov 2019 08:57:02 GMT"
    ],
         "content-type" => [
        [0] "text/plain;charset=ISO-8859-1"
    ],
    "transfer-encoding" => [
        [0] "chunked"
    ],
           "connection" => [
        [0] "close"
    ],
           "set-cookie" => [
        [0] "__cfduid=d844dfc0e4a7b38f70806c3138067e9e11573117022; expires=Fri, 06-Nov-20 08:57:02 GMT; path=/; domain=.adminserv88.com; HttpOnly"
    ],
                 "vary" => [
        [0] "Accept-Encoding"
    ],
      "cf-cache-status" => [
        [0] "DYNAMIC"
    ],
            "expect-ct" => [
        [0] "max-age=604800, report-uri=\"https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct\""
    ],
               "server" => [
        [0] "cloudflare"
    ],
               "cf-ray" => [
        [0] "531e18f0ef75dadc-TPE"
    ]
}

> response.code
"200"

> response.body
"<mbrapi-launchurl-resp status=\"0\" timestamp=\"2019-11-07 08:56:25.618 UTC\" token=\"UgTwuwJv40eTqcBZdlnEo1BTbh2xDViKEeiRLMHiny01fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu\" launchUrl=\"https://redirect.dirdelivery.com/Casino/Default.aspx?applicationid=163&amp;serverid=2487&amp;gamename=dragonDance&amp;languagecode=en&amp;ul=en&amp;lobbyname=mobile88&amp;loginType=VanguardSessionToken&amp;authToken=E8Yqc8FqMQ__uPaLj0IXHKHLbP02QfFwkr250mfw5881fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu&amp;lobbyURL=http%3A%2F%2Flocalhost%3A3300%2Fegame%2Fmg&amp;bankingURL=http%3A%2F%2Flocalhost%3A3300%2Fclient%2Fwallets&amp;showva=playcheck\"/>"

> Hash.from_xml(response.body)[@xml_resp]
{
       "status" => "0",
    "timestamp" => "2019-11-07 08:56:25.618 UTC",
        "token" => "UgTwuwJv40eTqcBZdlnEo1BTbh2xDViKEeiRLMHiny01fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu",
    "launchUrl" => "https://redirect.dirdelivery.com/Casino/Default.aspx?applicationid=163&serverid=2487&gamename=dragonDance&languagecode=en&ul=en&lobbyname=mobile88&loginType=VanguardSessionToken&authToken=E8Yqc8FqMQ__uPaLj0IXHKHLbP02QfFwkr250mfw5881fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu&lobbyURL=http%3A%2F%2Flocalhost%3A3300%2Fegame%2Fmg&bankingURL=http%3A%2F%2Flocalhost%3A3300%2Fclient%2Fwallets&showva=playcheck"
}
````

Mg 進入遊戲

````ruby
options =
{
      :params => {
         locale: "zh-CN",
          trial: "true",
        partner: "mg",
        game_id: "46704"
    },
    :category => "egame"
}

user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).login('desktop', options)
````

````ruby
> @user_token
nil

# 與games/mg的差別在於，games_v3/mg沒有token
> @params  
{
            :timestamp => "2019-11-07 09:10:28 UTC",
          :apiusername => "apiadmin",
          :apipassword => "apipassword",
                :token => nil,
             :language => :en,
               :gameId => "46704",
           :bankingUrl => "http://127.0.0.1:3000/client/wallets",
             :lobbyUrl => "http://127.0.0.1:3000/egame/mg",
    :logoutRedirectUrl => "http://127.0.0.1:3000",
             :demoMode => true
}
> @headers
{
        "Content-Type" => "application/xml",
    "X-Requested-With" => "X-Api-Client",
          "X-Api-Call" => "X-Api-Client"
}

> api.body
"<mbrapi-changecredit-call timestamp=\"2019-11-07 09:18:38 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" token=\"\" language=\"en\" gameId=\"46704\" bankingUrl=\"http://127.0.0.1:3000/client/wallets\" lobbyUrl=\"http://127.0.0.1:3000/egame/mg\" logoutRedirectUrl=\"http://127.0.0.1:3000\" demoMode=\"true\"/>"

> response
{
                 "date" => [
        [0] "Thu, 07 Nov 2019 09:19:56 GMT"
    ],
         "content-type" => [
        [0] "text/plain;charset=ISO-8859-1"
    ],
  
    ...
    
               "cf-ray" => [
        [0] "531e3a7c0a0d45dc-TPE"
    ]
}

> res
{
       "status" => "1002",
    "timestamp" => "2019-11-07 09:19:19.558 UTC"
}
````

先在games_v3/mg的initialize執行@user_token = gen_user_token ，所以我在initialize下中斷點看看

執行 gen_user_token

````ruby
options =
{
      :params => {
         locale: "zh-CN",
          trial: "true",
        partner: "mg",
        game_id: "46704"
    },
    :category => "egame"
}

user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user)
````

結果

````ruby
> @headers
{
        "Content-Type" => "application/xml",
    "X-Requested-With" => "X-Api-Client",
          "X-Api-Call" => "X-Api-Client"
}

> @params
{
       :timestamp => "2019-11-07 09:39:55 UTC",
     :apiusername => "apiadmin",
     :apipassword => "apipassword",
        :username => "dfctchenhantin",
        :password => "xEpv79Rc",
       :ipaddress => "192.168.0.1",
       :partnerId => "88KIOSK",
    :currencyCode => "CNY"
}

> api.body
"<mbrapi-changecredit-call timestamp=\"2019-11-07 09:39:55 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" username=\"dfctchenhantin\" password=\"xEpv79Rc\" ipaddress=\"192.168.0.1\" partnerId=\"88KIOSK\" currencyCode=\"CNY\"/>"

> response
{
                 "date" => [
        [0] "Thu, 07 Nov 2019 09:40:54 GMT"
    ],
         "content-type" => [
        [0] "text/plain;charset=ISO-8859-1"
    ],
    "transfer-encoding" => [
        [0] "chunked"
    ],
           "connection" => [
        [0] "close"
    ],
           "set-cookie" => [
        [0] "__cfduid=d16c75302cafa0ef20bf43ac6d36b16441573119654; expires=Fri, 06-Nov-20 09:40:54 GMT; path=/; domain=.adminserv88.com; HttpOnly"
    ],
                 "vary" => [
        [0] "Accept-Encoding"
    ],
      "cf-cache-status" => [
        [0] "DYNAMIC"
    ],
            "expect-ct" => [
        [0] "max-age=604800, report-uri=\"https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct\""
    ],
               "server" => [
        [0] "cloudflare"
    ],
               "cf-ray" => [
        [0] "531e5931cde845ac-TPE"
    ]
}

> self.sty_res.body (response.body)
"<mbrapi-changecredit-resp status=\"1002\" timestamp=\"2019-11-07 09:58:16.555 UTC\"/>"

> res
{
       "status" => "1002",
    "timestamp" => "2019-11-07 09:40:17.433 UTC"
}

> sty_data
{}


> builder
#<Nokogiri::XML::Builder:0x00007f9978197fa8 @doc=#<Nokogiri::XML::
...
@ns=nil>

> builder.doc
"<?xml version="1.0"?>
<mbrapi-changecredit-call timestamp="2019-11-07 10:05:57 UTC" apiusername="apiadmin" apipassword="apipassword" username="dfctchenhantin" password="xEpv79Rc" ipaddress="192.168.0.1" partnerId="88KIOSK" currencyCode="CNY"/>"

> builder.doc.root
"<mbrapi-changecredit-call timestamp="2019-11-07 10:05:57 UTC" apiusername="apiadmin" apipassword="apipassword" username="dfctchenhantin" password="xEpv79Rc" ipaddress="192.168.0.1" partnerId="88KIOSK" currencyCode="CNY"/>"

> builder.doc.root.to_xml
"<mbrapi-changecredit-call timestamp=\"2019-11-07 10:05:57 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" username=\"dfctchenhantin\" password=\"xEpv79Rc\" ipaddress=\"192.168.0.1\" partnerId=\"88KIOSK\" currencyCode=\"CNY\"/>"
````

在games/mg 的 `initialize`執行@user_token = login，在login下中斷點：

````ruby
> @user_token
"MNNwVAEfhEi-4-cRXZ3Sl0yl9Bv-ArQc2MN_Cis-M7Q1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu"

> @headers
{
        "Content-Type" => "application/xml",
    "X-Requested-With" => "X-Api-Client",
          "X-Api-Call" => "X-Api-Client"
}

# 無異
> @params
{
       :timestamp => "2019-11-07 09:46:29 UTC",
     :apiusername => "apiadmin",
     :apipassword => "apipassword",
        :username => "dfctchenhantin",
        :password => "xEpv79Rc",
       :ipaddress => "192.168.0.1",
       :partnerId => "88KIOSK",
    :currencyCode => "CNY"
}

# games_v3/mg 開頭為 mbrapi-changecredit-call
# games_v3/mg 多了 apipassword=\"apipassword\"
# 其他無異
> api.body
"<mbrapi-login-call timestamp=\"2019-11-07 09:46:29 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" username=\"dfctchenhantin\" password=\"xEpv79Rc\" ipaddress=\"192.168.0.1\" partnerId=\"88KIOSK\" currencyCode=\"CNY\"/>"

# 無異
> response
{
                 "date" => [
        [0] "Thu, 07 Nov 2019 09:48:43 GMT"
    ],
         "content-type" => [
        [0] "text/plain;charset=ISO-8859-1"
    ],
    "transfer-encoding" => [
        [0] "chunked"
    ],
           "connection" => [
        [0] "close"
    ],
           "set-cookie" => [
        [0] "__cfduid=daa191d28443e39bd755f434a92a0328a1573120123; expires=Fri, 06-Nov-20 09:48:43 GMT; path=/; domain=.adminserv88.com; HttpOnly"
    ],
                 "vary" => [
        [0] "Accept-Encoding"
    ],
      "cf-cache-status" => [
        [0] "DYNAMIC"
    ],
            "expect-ct" => [
        [0] "max-age=604800, report-uri=\"https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct\""
    ],
               "server" => [
        [0] "cloudflare"
    ],
               "cf-ray" => [
        [0] "531e64a36b194546-TPE"
    ]
}

# 不一樣
> response.body
"<mbrapi-login-resp status=\"0\" timestamp=\"2019-11-07 09:48:06.278 UTC\" token=\"57puwIcZTXHVkPEA48kxrvNrMP-uKMhrFGfPZKHoyVQ1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu\" casinoId=\"2301\"/>"

> Hash.from_xml(response.body)
{
    "mbrapi_login_resp" => {
           "status" => "0",
        "timestamp" => "2019-11-07 09:48:06.278 UTC",
            "token" => "57puwIcZTXHVkPEA48kxrvNrMP-uKMhrFGfPZKHoyVQ1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu",
         "casinoId" => "2301"
    }
}

> res
{
       "status" => "0",
    "timestamp" => "2019-11-07 09:48:06.278 UTC",
        "token" => "57puwIcZTXHVkPEA48kxrvNrMP-uKMhrFGfPZKHoyVQ1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu",
     "casinoId" => "2301"
}

# 相異地方: name="password" value="xEpv79Rc"
builder
#<Nokogiri::XML::Builder:0x00007fac8df9add0 @doc=#<Nokogiri::XML::
...
@ns=nil>

# mbrapi-changecredit-call 與 mbrapi-login-call 不一樣
> builder.doc
"<?xml version="1.0"?> 
<mbrapi-login-call timestamp="2019-11-07 09:46:29 UTC" apiusername="apiadmin" apipassword="apipassword" username="dfctchenhantin" password="*****" ipaddress="192.168.0.1" partnerId="88KIOSK" currencyCode="CNY"/>"

> builder.doc.root
"<mbrapi-login-call timestamp="2019-11-07 09:46:29 UTC" apiusername="apiadmin" apipassword="apipassword" username="dfctchenhantin" password="*****" ipaddress="192.168.0.1" partnerId="88KIOSK" currencyCode="CNY"/>"

> builder.doc.root.to_xml
"<mbrapi-login-call timestamp=\"2019-11-07 09:46:29 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" username=\"dfctchenhantin\" password=\"*****\" ipaddress=\"192.168.0.1\" partnerId=\"88KIOSK\" currencyCode=\"CNY\"/>"
````

結果發現是@xml_call 和 @xml_resp 值錯誤，總之後來就好了

進入 balance

```ruby
user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).balance
```

進入 order

````ruby
# 創立新訂單
user = User.find_by account:"chenhanting"
order = Client::GameOrder.create(user_id: 1803, partner: "mg", order_type: "deposit", order_num: %(han#{Random.new_seed}), amount: 1.0, status: "new_order")
mg = GamesV3::Mg.new(user).transfer(order)

# output => true
````

進入self.CheckState

先看wallet_order/check_job.rb

````ruby
class WalletOrder::CheckJob < WalletOrderJob
  # 判斷訂單完成了沒
  def self.create(game_wallet, game_order, result = nil, job_settings = nil)
    # 前面 result 如果原本是symbol 在這會是 string
    set(job_settings) if job_settings.present?
    job = perform_later(game_wallet.id, result)
    game_wallet.update(job: "#{job.queue_name}|#{job.provider_job_id}")
    # BroHand define job_code "#{job.queue_name}|#{job.provider_job_id}"
    game_order.logging('Check')
  end

  def perform(game_wallet_id, check = nil)
    game_wallet = Client::GameWallet.find(game_wallet_id)
    game_order = game_wallet.game_order
    set_render_text(game_order.order_type) # @LOADING_TEXT @FAIL_TEXT @SUCCESS_TEXT

    return { status: false, message: 'GameOrderStateError!!' } unless game_order.checking?

    begin
      game_object = SimpleGameFactory.create_game(game_wallet.name, game_order.user)
      check_method = game_object.class.method(:CheckState)
      check = game_object.respond_to?(:check) ? game_object.check(game_order) : check  # 這一行

      case check_method.call(check, game_order)
````

由於mg遊戲沒有ckeck，所以我先在self.CheckState(check, game_order) 的 check 分別帶入'true', 'loading', 'pending'

````ruby
# 創立新訂單
user = User.find_by account:"chenhanting"
order = Client::GameOrder.create(user_id: 1803, partner: "mg", order_type: "deposit", order_num: %(han#{Random.new_seed}), amount: 1.0, status: "new_order")
mg = GamesV3::Mg.new(user).transfer(order)

check = GamesV3::Mg.CheckState('true', order)
check = GamesV3::Mg.CheckState('loading', order)
check = GamesV3::Mg.CheckState('pending', order)
````

幾個問題想要討論

- mg遊戲沒有check的action

- get_bet_history沒有@params參數

- 如何進入 egame 的 get_bet_history

- get_bet_log_job.rb

  ````ruby
  if self.class.under_any_maintain?(...
    ...
  else
    # get bet history
    bet_log_timestamp.process!
    bets = case category
           when 'casino'
             game_obj.get_casino_bet_history(key, _start_time, _end_time)
           when 'egame'
             game_obj.get_egame_bet_history(key, _start_time, _end_time)
           when 'bet_result'
             game_obj.get_bet_return_history(key)
           else # game
             game_obj.get_bet_history(key, _start_time, _end_time)
           end
  ````

  要如何讓 egame 進到 get_bet_history，可以先問 Evan 如何做到的

 Net::HTTP::Post 改成 ResClient：首先先改寫login動作

````ruby
options =
{
      :params => {
         locale: "zh-CN",
          trial: "true",
        partner: "mg",
        game_id: "46704"
    },
    :category => "egame"
}

user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).login('desktop', options)
````

傳給 RestClient 的參數

````ruby
> sty_params
{
     :method => :post,
        :url => #<URI::HTTPS https://ag.adminserv88.com/member-api-web/member-api>,
    :payload => "{\"timestamp\":\"2019-11-08 02:43:52 UTC\",\"apiusername\":\"apiadmin\",\"apipassword\":\"apipassword\",\"token\":\"NLLCYy1D6-Ks4a_2BodZTW_3NjLQzyV21YZDcyRzXTs1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu\",\"language\":\"en\",\"gameId\":\"46704\",\"bankingUrl\":\"http://127.0.0.1:3000/client/wallets\",\"lobbyUrl\":\"http://127.0.0.1:3000/egame/mg\",\"logoutRedirectUrl\":\"http://127.0.0.1:3000\",\"demoMode\":true}",
    :headers => {
            "Content-Type" => "application/xml",
        "X-Requested-With" => "X-Api-Client",
              "X-Api-Call" => "X-Api-Client"
    }
}
````

自己打 api

````ruby
@params = {
  timestamp: Time.current.utc.to_s,
  apiusername: 'apiadmin',
  apipassword: 'apipassword',
  token: @user_token,
  language: @lang,
  gameId: @game_id.to_s,
  bankingUrl: "#{@host}/client/wallets",
  lobbyUrl: "#{@host}/egame/mg",
  logoutRedirectUrl: @host,
  demoMode: @action,
}

@headers = {
  'Content-Type' => 'application/xml',
  'X-Requested-With' => 'X-Api-Client',
  'X-Api-Call' => 'X-Api-Client',
}

api = RestClient.post GamesV3::Mg::MG_MEMBER_BASE_URL, @params, @headers
````

錯誤訊息

````txt
warning: Overriding "Content-Type" header "application/xml" with "application/x-www-form-urlencoded" due to payload

RestClient::InternalServerError: 500 Internal Server Error
from /Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/rest-client-2.0.2/lib/restclient/abstract_response.rb:223:in `exception_with_response'
````

5xx 錯誤訊息內容

- `500` Internal Server Error - 內部伺服器錯誤。
- `501` Not Implemented – 標頭值指定未實作的設定。
- `502` Bad Gateway - Web 伺服器在作為閘道或 Proxy 時收到無效的回應。
- `503` Service Unavailable - 服務無法使用。 這是 IIS 6.0 專用的錯誤碼。
- `504` Gateway Timeout - 閘道逾時。
- `505` HTTP Version Not Supported - 不支援的 HTTP 版本。

細看進去哪邊導致錯誤

````ruby
    70: def self.post(url, payload, headers={}, &block)
 => 71:   Request.execute(:method => :post, :url => url, :payload => payload, :headers => headers, &block)
    72: end
````

````ruby
    51: def self.execute(args, & block)
 => 52:   new(args).execute(& block)
    53: end

> args
{
     :method => :post,
        :url => "https://ag.adminserv88.com/member-api-web/member-api",
    :payload => "{\"timestamp\":\"2019-11-08 03:46:15 UTC\",\"apiusername\":\"apiadmin\",\"apipassword\":\"apipassword\",\"token\":\"TztRa8lugfS9Y3cdtfiBjBJUAiQVbnkojfcJ3CQb9QY1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu\",\"language\":\"en\",\"gameId\":\"46704\",\"bankingUrl\":\"http://127.0.0.1:3000/client/wallets\",\"lobbyUrl\":\"http://127.0.0.1:3000/egame/mg\",\"logoutRedirectUrl\":\"http://127.0.0.1:3000\",\"demoMode\":true}",
    :headers => {
            "Content-Type" => "application/xml",
        "X-Requested-With" => "X-Api-Client",
              "X-Api-Call" => "X-Api-Client"
    }
}
````

再進去（這行開始錯誤）

From: /Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/rest-client-2.0.2/lib/restclient/request.rb @ line 72 RestClient::Request#initialize:

````ruby
    67:       else
    68:         raise ArgumentError, "must pass :url"
    69:       end
    70: 
    71:       @user = @password = nil
 => 72:       parse_url_with_auth!(url)
    73: 
    74:       # process cookie arguments found in headers or args
    75:       @cookie_jar = process_cookie_args!(@uri, @headers, args)
    76: 
    77:       @payload = Payload.generate(args[:payload])
````

再進去（這行開始錯誤）

From: /Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/rest-client-2.0.2/lib/restclient/request.rb @ line 145 RestClient::Request#execute:

````ruby
    142: def execute & block
    143:   # With 2.0.0+, net/http accepts URI objects in requests and handles wrapping
    144:   # IPv6 addresses in [] for use in the Host request header.
 => 145:   transmit uri, net_http_request_class(method).new(uri, processed_headers), payload, & block
    146: ensure
    147:   payload.close if payload
    148: end
````

參考資料：As per my findings, spring does not support content types `application/x-www-form-urlencoded`, `application/json`and `application/xml`together.

詢問Brohand：

````ruby
RestClient.post GamesV3::Mg::MG_MEMBER_BASE_URL, @params.to_xml, @headers
````

此為代入參數錯誤，原因是payload的地方，也就是 @params.to_xml 出來的格式為

````xml
<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<hash>\n  <timestamp>2019-11-08 04:09:06 UTC</timestamp>\n  <apiusername>apiadmin</apiusername>\n  <apipassword>apipassword</apipassword>\n  <token>KSEdp_JSQ31uocfs7rYD-byevqPkBZu0k4BjeIbnkKU1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu</token>\n  <language type=\"symbol\">en</language>\n  <gameId>46704</gameId>\n  <bankingUrl>http://127.0.0.1:3000/client/wallets</bankingUrl>\n  <lobbyUrl>http://127.0.0.1:3000/egame/mg</lobbyUrl>\n  <logoutRedirectUrl>http://127.0.0.1:3000</logoutRedirectUrl>\n  <demoMode type=\"boolean\">true</demoMode>\n</hash>\n
````

和預期（如下）不相符

````xml
<mbrapi-launchurl-call timestamp=\"2019-11-08 04:09:06 UTC\" apiusername=\"apiadmin\" apipassword=\"apipassword\" token=\"KSEdp_JSQ31uocfs7rYD-byevqPkBZu0k4BjeIbnkKU1fK39XC8R5W2WZFhrCCcBELOiQ-PO3BeUMHSLO_oGPKmv19VJRlvXrxDYVZCke0_F_hhk88bhz0FQXFU1PpXu\" language=\"en\" gameId=\"46704\" bankingUrl=\"http://127.0.0.1:3000/client/wallets\" lobbyUrl=\"http://127.0.0.1:3000/egame/mg\" logoutRedirectUrl=\"http://127.0.0.1:3000\" demoMode=\"true\"/>
````

只要改為下列傳輸方式即可成功

````ruby
RestClient.post GamesV3::Mg::MG_MEMBER_BASE_URL, builder.doc.root.to_xml, @headers
````

另外還有一個部分，是原本印出錯誤的地方

````ruby
   uri = URI.parse(GamesV3::Mg::MG_MEMBER_BASE_URL) # URL
=> api = Net::HTTP::Post.new(uri, @headers)  # 需要 @headers
   api.body = builder.doc.root.to_xml        # 需要 @params, @xml_call
   response = https(uri).request(api)
   self.sty_res = response
   api_caller_save
````

錯誤訊息為awesome_print錯誤，並不是程式本身的錯誤，所以程式本身並沒有錯才可以執行下去。

`````shell
output error: #<ArgumentError: wrong number of arguments (given 1, expected 0)>

/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/formatter.rb:114:in `convert_to_hash'
/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/formatter.rb:47:in `awesome_self'
/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/formatter.rb:28:in `format'
/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/inspector.rb:115:in `unnested'
/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/inspector.rb:74:in `awesome'
/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/core_ext/kernel.rb:10:in `ai'
/Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/awesome_print-1.8.0/lib/awesome_print/custom_defaults.rb:45:in `block in pry!'
`````

小錯誤

````ruby
# 對的
RestClient.post GamesV3::Mg::MG_MEMBER_BASE_URL, builder.doc.root.to_xml, header

# 錯的(原因是它要把method, url, payload, headers寫成一個雜湊)
RestClient::Request.execute(:post, URI.parse(GamesV3::Mg::MG_MEMBER_BASE_URL), builder.doc.root.to_xml, header)

# 錯的
RestClient::Request.execute(method: :post, url: URI.parse(GamesV3::Mg::MG_MEMBER_BASE_URL), payload: builder.doc.root.to_xml, headers: header)
````

錯在normalize_url(args[:url])

From: /Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/rest-client-2.0.2/lib/restclient/request.rb @ line 66 RestClient::Request#initialize:

````ruby
    61: 
    62:     def initialize args
    63:       @method = normalize_method(args[:method])
    64:       @headers = (args[:headers] || {}).dup
    65:       if args[:url]
 => 66:         @url = process_url_params(normalize_url(args[:url]), headers)
    67:       else
    68:         raise ArgumentError, "must pass :url"
    69:       end
    70: 
    71:       @user = @password = nil
````

進入 normalize_url(url)

````ruby
    462: def normalize_url(url)
 => 463:   url = 'http://' + url unless url.match(%r{\A[a-z][a-z0-9+.-]*://}i)
    464:   url
    465: end
````

後來發現錯在我的url給的是 URI.parse(...)，去掉就好了

進入gen_user_token

```ruby
user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).gen_user_token
```

進入balance

```ruby
user = User.find_by account:"chenhanting"
GamesV3::Mg.new(user).balance
```

進入transfer

```ruby
user = User.find_by account:"chenhanting"
order = Client::GameOrder.create(user_id: 1803, partner: "mg", order_type: "withdrawal", order_num: %(han#{Random.new_seed}), amount: 1.0, status: "new_order")
GamesV3::Mg.new(user).transfer(order)

user = User.find_by account:"chenhanting"
order = Client::GameOrder.create(user_id: 1803, partner: "mg", order_type: "deposit", order_num: %(han#{Random.new_seed}), amount: 1.0, status: "new_order")
GamesV3::Mg.new(user).transfer(order)
```

進入gen_token

```ruby
user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).gen_token
```

錯誤訊息 RestClient::Found: 302 Found
from /Users/hanting/.rvm/gems/ruby-2.4.6@fatcat/gems/rest-client-2.0.2/lib/restclient/abstract_response.rb:223:in `exception_with_response'

From: /Users/hanting/fatcat/app/services/games/mg.rb @ line 41 Mg#token:

````ruby
    33: def token
    34:   return [@token, @tokenID] if @token && @tokenID
    35: 
    36:   set_api(__method__)
    37:   uri = URI.parse(MG_DOMAIN + @api_url)
    38:   api = Net::HTTP::Post.new(uri, @headers)
    39:   api.set_form_data(@params)
    40:   response = https(uri).request(api)
 => 41:   ApiCaller.save('POST', uri, @params, response,
    42:       action: @action,
    43:       partner: 'mg')
    44: 
    45:   if response.code == '200'
    46:     res = JSON.parse(response.body)
    47:     return [res['token'], res['id']]
    48:   else
    49:     raise CustomError.new(:api_response_code_not_200, 'response.code != 200')
    50:   end
    51: end

> response.code
"302"
````

進入create

```ruby
user = User.find_by account:"chenhanting"
mg = GamesV3::Mg.new(user).create

> self.sty_params
{
     :method => :put,
        :url => "https://ag.adminserv88.comlps/secure/network//downline",
    :payload => "{\"crId\":null,\"crType\":\"ma\",\"neId\":null,\"neType\":\"ma\",\"tarType\":\"m\",\"username\":\"dfctchenhantin\",\"name\":\"chenhanting\",\"password\":\"xEpv79Rc\",\"confirmPassword\":\"xEpv79Rc\",\"currency\":\"CNY\",\"language\":\"en\",\"email\":\"\",\"mobile\":\"\",\"casino\":{\"enable\":true},\"poker\":{\"enable\":false}}",
    :headers => {
            "Content-Type" => "application/json",
        "X-Requested-With" => "X-Api-Client",
              "X-Api-Call" => "X-Api-Client",
              "X-Api-Auth" => nil
    }
}
```

遇到錯誤訊息 SocketError，發現是發現是base_url錯誤： Failed to open TCP connection to ag.adminserv88.comlps:443 (getaddrinfo: nodename nor servname provided, or not known)
from /Users/hanting/.rvm/rubies/ruby-2.4.6/lib/ruby/2.4.0/net/http.rb:906:in `rescue in block in connect'

進入 games/mg.rb 測試，以及測試結果

````ruby
user = User.find_by account:"chenhanting"
mg = Mg.new(user).create

> response.code
"302"
> response.body
""

> @headers
{
        "Content-Type" => "application/json",
    "X-Requested-With" => "X-Api-Client",
          "X-Api-Call" => "X-Api-Client",
          "X-Api-Auth" => nil
}

> @params
{
               :crId => nil,
             :crType => "ma",
               :neId => nil,
             :neType => "ma",
            :tarType => "m",
           :username => "dfctchenhantin",
               :name => "chenhanting",
           :password => "xEpv79Rc",
    :confirmPassword => "xEpv79Rc",
           :currency => "CNY",
           :language => "en",
              :email => "",
             :mobile => "",
             :casino => {
        :enable => true
    },
              :poker => {
        :enable => false
    }
}

> MG_DOMAIN + @api_url
"https://ag.adminserv88.com/lps/secure/network//downline"
````

測get_bet_history 

````ruby
user = User.find_by account:"chenhanting"
start_time = Time.current - 2.hours
end_time = Time.current - 70.minutes
GamesV3::Mg.new(user).get_bet_history(start_time, end_time)
````

得到的錯誤訊息為`302` 

物件已移動，並告知移動過去的網址。針對表單架構驗證，這通常表示為「物件已移動」。 要求的資源暫時存於不同的 URI 底下。 由於重新導向可能偶而改變，用戶端應繼續使用要求 URI 來執行未來的要求。 除非以 Cache-Control 或 Expires 標頭欄位表示，此回應才能夠快取。

````shell
> RestClient::Request.execute(sty_params)
RestClient::Found: 302 Found
````

進入遊戲的參數：

````ruby
options =
{
      :params => {
         locale: "zh-CN",
          trial: "true",
        partner: "mg",
        game_id: "46704"
    },
    :category => "egame"
}
````

### 其他

1. 同步遊戲紀錄：http://127.0.0.1:3000/zh-CN/bet_log_checks?company_name=JV+Diamond

2. payload 小知識：

   google到一篇很好的文章對payload為何這樣叫有很好的解釋，文中指出這個名詞是借用運輸工具上的觀念而來的，例如：卡車、油罐車、貨輪等所謂的**載具**，然後通常一個載具的總重量一定**大於**載具的承載量，例如油罐車的總重量包含了他所運載的油量、司機的重量、油罐車行駛所需的油量，但我們所關心僅是油罐車所承載的油量而已。對programming來說，我們可以某api的response為例子來看

   ```json
     {
         "status":"OK",
         "data":
             {
                 "message":"Hello, world!"
             }
     }
   ```

   其中的Hello, world!正是payload，也是我們關心的部分。到這裡我們應該了解為何參數名要叫做`payload`，而非`data`或是`params`是有其目的性的，而更進一步的熟悉與使用payload這個參數，則就要再深入看該方法或函式的使用與定義了。

### 參考網址

1. 刪除redis的參考網址：https://github.com/resque/redis-namespace/issues/56
2. payload 小知識：http://noelsaga.herokuapp.com/blog/2015/07/18/kai-fa-zhong-,chang-jian-de-can-shu-payloadshi-shi-mo