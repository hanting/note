# Note

1. Sign up = Register

2. hot slot：電玩遊戲

3. hot fix：程式補丁

4. fatcat：有財有勢的大亨，知名人士，有錢有勢的人。說他們是fat cat 基於兩點原因。第一個是有財有勢的人給人的形象都是大腹便便，一身膘肉，更重要的是他們的錢袋子也是鼓鼓的；第二個原因是，貓是很精明的動物，而有錢有勢的人往往也是聰明人哦。所以用fat cat 來形容再合適不過。

5. latency 延遲

6. The man you just mentioned still doesn’t ring a bell with me.
   你剛提到的那個男人，我仍然一點印象也沒有。

7. stake：賭局

8. parlay：串關

   參考資料：https://www.chinatimes.com/realtimenews/20181008000003-260403?chdtv

9. merchant：商人

10. parentheses：括號

11. trial：試用

12. credentials [[ plural \]](https://dictionary.cambridge.org/zht/help/codes.html)

    the [abilities](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/ability) and [experience](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/experience) that make someone [suitable](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/suitable) for a [particular](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/particular) [job](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/job) or [activity](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/activity), or [proof](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/proof) of someone's [abilities](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/ability) and [experience](https://dictionary.cambridge.org/zht/詞典/英語-漢語-繁體/experience)

    資歷，資格；資格證書，資質證明

    *All the candidates had excellent academic credentials.*所有候選人都具有優秀的學歷。

    *She was asked to show her press credentials.*她被要求出示記者證。

13. changelog：變更日誌

14. back office：後台

15. Resgistor：註冊

### 紐約賞析

參考資料：https://udn.com/author/articles/2/810

