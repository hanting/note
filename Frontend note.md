# Note

以下統整出到職至今前端相關資料

### HTML

name屬性

可以說幾乎每個做過Web開發的人都問過，到底元素的ID和Name有什麼區別阿？為什麼有了ID還要有Name呢?!
而同樣我們也可以得到最classical的答案：ID就像是一個人的身份證號碼，而Name就像是他的名字，ID顯然是唯一的，而Name是可以重複的。

上周我也遇到了ID和Name的問題，在頁面裡輸入了一個input type="hidden"，只寫了一個ID='SliceInfo'，賦值後submit，在後台用 Request.Params["SliceInfo"]卻怎麼也去不到值。後來恍然大悟因該用Name來標示，於是在input裡加了個 Name='SliceInfo'，就一切ok了。

剛開始對於ID和Name的解答說的太籠統了，當然那個解釋對於ID來說是完全對的，它就是Client端HTML元素的Identity。而Name其實要複雜的多，因為Name有很多種的用途，所以它並不能完全由ID來代替，從而將其取消掉。

參考資料（id和name的區別）：https://www.itread01.com/content/1550180377.html

參考資料（name陣列）：https://mattstauffer.com/blog/a-little-trick-for-grouping-fields-in-an-html-form/

參考資料（其實form_for已經把name包好了）：https://flatironschool.com/blog/how-params-works-in-rails

參考資料（進一步了解rails和name屬性關係）：https://www.rubyguides.com/2019/06/rails-params/

### CSS

1. margin 與 padding 的用法需要了解
2. CSS Media Queries：在製作 RWD 響應式網頁時，一定會用到的 CSS 語法就是 media，代表的就是針對指定的「媒體」設定樣式，這篇將會對 CSS 的 Media Queries 做個詳細介紹，也幫助自己未來撰寫時，更有個方向與參考

### Navbar

1. 導覽列（預計用於實驗室網站）
   - 範例1：https://codesandbox.io/s/flexbox-nav-version-1-hho4o
   - 範例2：https://github.com/wilsmex/edu/tree/master/navbar-responsive

### Bootstrap

1. 按鈕如何改變字的顏色：https://github.com/mdbootstrap/bootstrap-material-design/issues/119
2. 可以參考的樣版（大推）：
   - https://getbootstrap.com/docs/4.4/examples/
   - https://bootsnipp.com/ （想找靈感請看這邊）
3. 登入畫面樣板
   - https://bootsnipp.com/snippets/vl4R7
   - https://bootsnipp.com/snippets/bxzmb

4. 其他可使用樣板
   - https://bootsnipp.com/snippets/8M2WK (Card)
   - https://bootsnipp.com/snippets/KQ (留言)

5. rails 的 bootstrap simple_form 語法：http://simple-form-bootstrap.plataformatec.com.br/examples/horizontal

6. bootstrap 切版講解（講得很好）：https://andy6804tw.github.io/2018/01/06/bootstrap-tutorial(1)/

7. 按鈕靠右對齊：https://codedefault.com/s/how-can-i-set-bootstrap-buttons-to-right-align

8. footer固定在底部：https://stackoverflow.com/questions/19330611/fixed-footer-in-bootstrap
9. Carousel：https://mdbootstrap.com/docs/jquery/javascript/carousel/

### Common sense

1. Favicon是favorites icon的縮寫，亦被稱為website icon（網頁圖示）、page icon（頁面圖示）或urlicon（URL圖示）：https://zh.wikipedia.org/wiki/Favicon

### Javscript

1. this：http://www.jstips.co/zh_tw/javascript/return-objects-to-enable-chaining-of-functions/

   this：https://www.w3schools.com/js/js_this.asp

2. 樂透產生器：https://codepen.io/realcreator/pen/yeqpqJ

   樂透產生器：https://codepen.io/ferhatkatesci/pen/Gxzxyg

3. 排列組合：https://codereview.stackexchange.com/questions/7001/generating-all-combinations-of-an-array

4. yield：https://codeburst.io/what-are-javascript-generators-and-how-to-use-them-c6f2713fd12e

5. 其他參數：https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/rest_parameters

6. Invoke function

   ````js
   function myFunction() {
     alert('hello');
   }
   
   myFunction()
   // calls the function once
   ````

   reference: https://developer.mozilla.org/zh-TW/docs/Learn/JavaScript/Building_blocks/Functions

7. Arguments 物件：https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/arguments

8. How to get index of an array：https://appdividend.com/2018/12/19/javascript-array-indexof-example-tutorial/

9. I'm trying to fix an old script written for me. I need it to run without `<body onload="init ();">`. I'd like to run the function from inside the script without inline code like that command. Sorry I'm not a JS expert but how do I do this?

   ````html
   <script type="text/javascript">
       function init() {
           // Put your code here
       }
       window.onload = init;
   </script>
   ````

   Or, if you are using jQuery

   ````javascript
   $(function() {
       // Your code here
   });
   ````

   因為在我的專案無法使用 `<body onload="init ();">` ，所以要做以上改寫。

   參考網址：https://stackoverflow.com/questions/7561315/alternative-to-body-onload-init
   
10. 使用 typeof 看型別：https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Operators/typeof

11. JS console is more than console.log: https://medium.com/devgorilla/the-console-object-provides-access-to-the-browsers-debugging-console-354eda9d2d50

### jquery

1. Set the default value in dropdownlist using jQuery：https://stackoverflow.com/questions/4781420/set-the-default-value-in-dropdownlist-using-jquery

2. \$(\$0).val() 的 $0 代表取得當前的值，所以當在瀏覽器欲檢查元素按右鍵點選「檢查」，再進去"console"輸入jquery指令，便可以取得當前的值。

3. $('#time_type').val('start_time'); 代表在#time_type中指派（assign）一個新值

4. Problem：Is this:

   ```js
   var contents = document.getElementById('contents');
   ```

   The same as this:

   ```js
   var contents = $('#contents');
   ```

   Given that jQuery is loaded?

   Ans1：Not exactly!!

   ```js
   document.getElementById('contents'); //returns a HTML DOM Object
   
   var contents = $('#contents');  //returns a jQuery Object
   ```

   ------

   In jQuery, to get the same result as `document.getElementById`, you can access the jQuery Object and get the first element in the object (Remember JavaScript objects act similar to associative arrays).

   ```js
   var contents = $('#contents')[0]; //returns a HTML DOM Object
   ```

   Ans2：No.

   Calling `document.getElementById('id')` will return a raw DOM object.

   Calling `$('#id')` will return a jQuery object that wraps the DOM object and provides jQuery methods.

   Thus, you can only call jQuery methods like `css()` or `animate()` on the `$()` call.

   You can also write `$(document.getElementById('id'))`, which will return a jQuery object and is equivalent to `$('#id')`.

   You can get the underlying DOM object from a jQuery object by writing `$('#id')[0]`.

   參考網址：https://stackoverflow.com/questions/4069982/document-getelementbyid-vs-jquery

5. 插入內容 - 使用prepend：

   ````html
   <html>
   <head>
   <script type="text/javascript" src="/jquery/jquery.js"></script>
   <script type="text/javascript">
   $(document).ready(function(){
     $("button").click(function(){
       $("p").prepend("<b>Hello world!</b> ");
     });
   });
   </script>
   </head>
   <body>
   <p>This is a paragraph.</p>
   <p>This is another paragraph.</p>
   <button>插入文字</button>
   </body>
   </html>
   ````

6. 從零開始的jQuery 學習紀錄：https://ithelp.ithome.com.tw/articles/10193972

