# Google Cloud Platform

以下紀錄實作php版本切換的歷程

## 使用前

1. 在使用GCP之前，先進入網站填入個人資料：https://console.cloud.google.com/getting-started?hl=zh-TW。
   
2. 簡單部署：使用App Engine、Cloud SQL、Cloud Storage。不過個人認為，如果只是小型網站的話，應該不至於需要使用Cloud Storage
   
3. 為了能夠在終端機下指令，先安裝google cloud SDK

   參考網址：https://cloud.google.com/sdk/docs/downloads-interactive?hl=zh-tw

   參考網址(windows)：https://cloud.google.com/sdk/docs/quickstart-windows?hl=zh-tw

4. 安裝完，執行$ gcloud init後，會跳轉到以下網址

   網址：https://cloud.google.com/sdk/auth_success

## 基本部署

參考網址：https://orangecat.tw/google-app-engine-app-example-with-laravel/

記得app.yaml必須填妥，不然會發生500伺服器錯誤

````yaml
runtime: php
env: flex

runtime_config:
  document_root: public
  composer_flags: --no-dev

# Ensure we skip ".env", which is only for local development
skip_files:
  - .env

env_variables:
  # Put production environment variables here.
  APP_URL: https://forward-bucksaw-253914.appspot.com/
  APP_ENV: production
  APP_LOG: errorlog
  APP_DEBUG: false
  APP_KEY: base64:CtEcZpXetZfsrU3HNjKQo+oMhk2vcIbcTtN0lUty7pg=
  STORAGE_DIR: /tmp
  VIEW_COMPILED_PATH: /tmp
````

## SQL

利用ipinfo.io查看自己目前的ip位址，在專案裡面加入自己的ip位置，它會加入自己的白名單。做好gcp上面的設定之後，在sequel pro進行連線，連線時username為root，密碼為當時設定的密碼（密碼：ashly）

在cloud sequal給予以下指令：gcloud sql connect taiwanvenice。進入到mysql語法頁面時，看到的taiwan-venice即為從本地端匯入的資料庫。

````mysql
> show databases;                                                                                                                                                       
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
| taiwan-venice      |
+--------------------+
````

以下為在mysql 下的語法

```mysql
show databases;
USE taiwan-venice;
select email from users;
+-------------------------+
| email                   |
+-------------------------+
| ashlychang@gmail.com    |
| quack.website@gmail.com |
| chouliy@tfam.gov.tw     |
| yaruchung@tfam.gov.tw   |
| yumei@tfam.gov.tw       |
+-------------------------+
```

編輯app.yaml為

````yaml
runtime: php
env: flex

runtime_config:
  document_root: public
  composer_flags: --no-dev

# Ensure we skip ".env", which is only for local development
skip_files:
  - .env

env_variables:
  # Put production environment variables here.
  APP_URL: https://forward-bucksaw-253914.appspot.com/
  APP_ENV: production
  APP_LOG: errorlog
  APP_DEBUG: false
  APP_KEY: base64:CtEcZpXetZfsrU3HNjKQo+oMhk2vcIbcTtN0lUty7pg=
  STORAGE_DIR: /tmp
  VIEW_COMPILED_PATH: /tmp
  # Replace USER, PASSWORD, DATABASE, and CONNECTION_NAME with the
  # values obtained when configuring your Cloud SQL instance.
  CLOUDSQL_USER:root
  CLOUDSQL_PASSWORD:ashly
  CLOUDSQL_DSN: "mysql:dbname=taiwan-venice;unix_socket=/cloudsql/forward-bucksaw-253914:asia-east1:taiwanvenice"
````



參考連結：https://ithelp.ithome.com.tw/articles/10197683

## 使用laravel

1. 使用 composer：https://www.itread01.com/content/1544497097.html

2. 使用laravel：http://yifancc.blogspot.com/2017/02/phpmac-laravel.html

3. shell script

   ````shell
   $ laravel -V                                          
   Laravel Installer 2.1.0
   $ php -v                                             
   PHP 7.2.22 (cli) (built: Sep 14 2019 18:36:32) ( NTS )
   Copyright (c) 1997-2018 The PHP Group
   Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
       with Zend OPcache v7.2.22, Copyright (c) 1999-2018, by Zend Technologies
   $ php artisan --version                     
   Laravel Framework 6.0.4
   ````

   

## 其他

- 小撇步：執行 exec $SHELL 就是重新執行終端機的意思

- php版本切換：https://jigsawye.com/2016/02/01/setup-laravel-development-environment-with-homebrew/

  這個網站介紹了關於php的安裝流程

- 在 App Engine 標準環境中安裝 phpMyAdmin（重要嗎？）：https://cloud.google.com/sql/docs/mysql/phpmyadmin-on-app-engine?hl=zh-tw

- 印萱提供給我precompile前端的工具：https://prepros.io/

- 這是從本地端匯出到gcp的流程，也介紹了關於部署的流程：http://www.skychin.me/posts/2018-11-02/connect-laravel-5.7-to-cloud-sql/

- composer.lock：http://blog.tonycube.com/2016/12/composer-php.html

- laravel整個部署流程（大推）：https://bugswarehouse.blogspot.com/2018/06/gcplaravel56-google-cloud-platform.html

