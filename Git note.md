# Note

1. 如何解決衝突：使用rebase
   git checkout dev
   git pull upstream dev

   以上兩行：將肥貓的dev拉到本地端的dev
   git checkout fix/add-README.md

   git branch（確定在分支fix/add-README.md）

   git diff
   git rebase dev

   (中間可能進入vscode當中修改)
   git diff
   git add .
   git rebase --continue
   git push -f

2. 如何解決衝突：使用rebase

   解衝突之後，git add <產生衝突的檔案>

   git rebase --continue

   git push origin +add/news (git push -f)

3. 如果遇上cherry-or-revert怎麼辦？

   使用：git revert --quit

   使用：git cherry-pick --quit、git cherry-pick --continue

   參考資料：https://github.com/Powerlevel9k/powerlevel9k/issues/224

4. 與遠端有關：[https://git-scm.com/book/zh-tw/v1/Git-%E5%9F%BA%E7%A4%8E-%E8%88%87%E9%81%A0%E7%AB%AF%E5%8D%94%E5%90%8C%E5%B7%A5%E4%BD%9C](https://git-scm.com/book/zh-tw/v1/Git-基礎-與遠端協同工作)

5. Git rebase 修改commit歷史記錄：https://tingtinghsu.github.io/blog/articles/2019-05-05-git_rebase

6. 如果不小心把原本要.gitignore的東西放上去怎麼辦？

   ````shell
   git rm <檔案名>
   ````

7. gitlab 不能 force push 的解決方式：https://stackoverflow.com/questions/32246503/fix-gitlab-error-you-are-not-allowed-to-push-code-to-protected-branches-on-thi

8. 修改最後一次commit： `git commit --amend -m "..."`

   參考資料：https://gitbook.tw/chapters/using-git/amend-commit1.html

9. Gitlab change project url: https://forum.gitlab.com/t/rename-project-url/23599

10. gitlab慣例：程式碼最後一行後面會多一行空行

11. .gitignore 專門設定哪一些檔案在push的時候不要一起進來

    我們會把較機密的檔案以及紀錄檔，設定不傳上來。

12. Code Review

    - PR: Pull Request. 拉取请求，给其他项目提交代码
    - LGTM: Looks Good To Me. 朕知道了 代码已经过 review，可以合并
    - SGTM: Sounds Good To Me. 和上面那句意思差不多，也是已经通过了 review 的意思
    - WIP: Work In Progress. 传说中提 PR 的最佳实践是，如果你有个改动很大的 PR，可以在写了一部分的情况下先提交，但是在标题里写上 WIP，以告诉项目维护者这个功能还未完成，方便维护者提前 review 部分提交的代码。
    - PTAL: Please Take A Look. 你来瞅瞅？用来提示别人来看一下
    - TBR: To Be Reviewed. 提示维护者进行 review
    - TL;DR: Too Long; Didn't Read. 太长懒得看。也有很多文档在做简略描述之前会写这么一句
    - TBD: To Be Done(or Defined/Discussed/Decided/Determined). 根据语境不同意义有所区别，但一般都是还没搞定的意思

13. 設定 upstream： https://zlargon.gitbooks.io/git-tutorial/content/remote/upstream.html

14. Revert "soft" : 重新改發過的commit。
    情境為發了太多的commit，需要縮減commit，只commit重點。