# Note

### Terminal

1. exec $SHELL 指令，相當於關閉終端機再重啟

### Chrome

1. command+shift+N：開啟無痕模式
2. command+R：重新整理
3. 打開Chrome DevTools 的 console：`Ctrl + Shift + J`、`Cmd + Opt + J`

### console

1. command+D：開啟新視窗

2. Ctrl+U：刪除游標以左

3. Ctrl+K：刪除游標以右

4. Ctrl+W：刪除游標以左、空格以右的內容

5. Ctrl+E：游標跳到最右

6. Ctrl+A：游標跳到最左

7. [rails console]：reload! 代表重新載入

8. linux：ls、ls -l、ls -al区别

   执行三个命令后，显示效果如下：

   [root@linuxserver test]$ ls            //显示不隐藏的文件与文件夹

   [root@linuxserver test]$ ls -l         //显示不隐藏的文件与文件夹的详细信息

   [root@linuxserver testing]$ ls -al //显示所有文件与文件夹的详细信息

   參考網址：https://blog.csdn.net/DJxyl/article/details/72778779

### VSCode 

- command+J : 打開vscode終端機 

  ctrl+~：關閉vscode終端機

- zsh and oh-my-zsh : https://zhuanlan.zhihu.com/p/35283688

- command+p : 找檔案

- 換分頁
  - Command + 1、2、3、4...
  - command + option + 左右
  
- command+D 可以一次改所有關鍵字

- 進入code` > `喜好設定` > `設定，可以進入settings.json

  目前的 json檔：

  ````json
  {
      // "terminal.integrated.shell.osx": "/bin/bash",
      "[erb]": {},
      "editor.tabSize": 2,
      // 將 os系統的 bash更換為zsh
      "terminal.integrated.shell.osx": "/bin/zsh",
      // 將 終端機字體設定可用的字體
      "terminal.integrated.fontFamily": "SauceCodePro Nerd Font",
      // 調整字體大小
      "terminal.integrated.fontSize": 14,
      "files.insertFinalNewline": true,
      "terminal.integrated.rendererType": "dom",
      "git.confirmSync": false,
      "explorer.confirmDragAndDrop": false,
      "window.zoomLevel": 0,
      // Jack: 如果是用vscode可以在設定裡加上這些， 不要總是讓空白鍵造成無意義的file change
      "diffEditor.ignoreTrimWhitespace": false,
      "files.trimTrailingWhitespace": true,
  }
  ````

  參考資料：https://poychang.github.io/my-vscode-config/

- ctrl+`：開啟終端機

- 開啟檔案快速鍵：Visual Studio Code 開啟工作目錄中的檔案的快速鍵為Ctrl + P，然後在出現的搜尋欄位輸入檔案名稱便可查找並開啟檔案。

- 刪除行快速鍵：Visual Studio Code刪除行的快速鍵是Ctrl + Shift + K

- 所有快捷鍵：https://code.visualstudio.com/docs/getstarted/keybindings#_basic-editing

- 移動程式碼快速鍵：Visual Studio Code(VS Code)移動程式碼快速鍵如下

  先選取要移動的程式碼，然後按

  Windows：向上移動Alt + ↑ / 向下移動Alt + ↓。

  Mac：向上移動⌥ + ↑ / 向下移動⌥ + ↓

- ERB-VSCode-Snippets
  ![image-20190917161235272](/Users/hanting/Library/Application Support/typora-user-images/image-20190917161235272.png)

- 安裝GitLens：可以看到是誰編輯哪一個文檔。

- vscode加入eof：https://stackoverflow.com/questions/44704968/visual-studio-code-insert-new-line-at-the-end-of-files

### Typora 

- Shift+Tab : 上一階層

  Tab : 下一階層