# 重要訊息

#### 2019.11.27 Joey 發佈訊息

Benford's law distribution 本福特分佈產生器

執行 100 次，會產生 100 個數字並符合其 Benford's 機率分佈。

````ruby
def benford
  r = rand
  (1..9).map { |n| Math.log10(1 + 1.0 / n) }
        .inject([]) { |ar, el| ar + [(ar.last || 0) + el] }
        .each_with_index.map { |p, i| [i + 1, p] }.find { |_n, p| r < p }
end
````

rails console：

````ruby
2.4.6 :034 > benford
 => [1, 0.3010299956639812]
````

其中 1 為依照本福特分佈隨機產生的數字， 0.301 為出現小於 1 這個數字的機率。

````ruby
2.4.6 :027 > benford
 => [7, 0.9030899869919435]
````

其中 7 為依照本福特分佈隨機產生的數字， 0.903 為出現小於 7 這個數字的機率。

參考來源：https://gist.github.com/wteuber/6e30b658e0a36b3b7258e22c019ddfed

#### 2019.11.18 Joey 發佈訊息

各位 Ruby on Rails 同學，這裏分享一個建議：如果沒有特別的理由、沒獲得特別好處的話，class 的命名盡量遵守 ruby 慣例：「第一個字母為大寫，之後的為小寫」例如：Rmg。

以 fatcat 為例：我們現有一個 class BetLog::RMG 。而「全大寫」在 ruby 慣例代表常數，所以 BetLog::RMG 看起來也像是 BetLog 類別的 class constants RMG。除了混淆的問題以外，也會失去一些好處，例如：rails 有提供一個 method：

````ruby
classify('ham_and_eggs') # => "HamAndEgg"
classify('posts')        # => "Post"
````

全大寫的 class 將無法享受到這項因為遵守慣例所帶來的便利。雖然寫程式的當下，您可能覺得沒差別，但未來哪一天要用的時候，您可能就會扼腕，當初為何沒有照慣例命名。若命名為 RMG 或是 Rmg 對你來說都一樣的話，建議選擇駝峰式命名法。

另外，如果要了解沒有依照駝峰命名的壞處，大家可以參考 api/v2/helpers/bet_histories.rb  這個檔案。