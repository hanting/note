# Note

以下統整出到職至今Ruby和Rails相關資料

## Ruby

### 變數

1. 易混淆的變數：https://ithelp.ithome.com.tw/articles/10158912

2. 介紹變數、宣告變數：[https://pjchender.github.io/2017/09/26/ruby-%E8%AE%8A%E6%95%B8%EF%BC%88variable%EF%BC%89%E8%88%87%E5%B8%B8%E6%95%B8%EF%BC%88constant%EF%BC%89/](https://pjchender.github.io/2017/09/26/ruby-變數（variable）與常數（constant）/)

3. enum - 建立易讀的狀態屬性：[https://blog.niclin.tw/2017/07/30/%E4%BD%BF%E7%94%A8-avtiverecordenum-%E5%BB%BA%E7%AB%8B%E6%98%93%E8%AE%80%E7%9A%84%E7%8B%80%E6%85%8B%E5%B1%AC%E6%80%A7/](https://blog.niclin.tw/2017/07/30/使用-avtiverecordenum-建立易讀的狀態屬性/)

   以上的 enum 跟 RAILS 的 enum 不同

4. Heap 和 Stack 的不同

   Stack: 區域變數使用堆積的方式存取運算符、方法、變數的運算，所以不會有 Race Condition

   Heap: 實體變數、類別變數存取的方式，因為Ruby的類別或實體共用這些變數，所以有機率發生 Race Condition。除此之外，它們會在 Heap 隨機存取一個記憶體位置，可能會出現效能問題

### caller and source_location

1. source_location的用法：
   - <Class>.method(:<方法名>).source_location                   #查詢類別方法所在位置 
   - <Class>.instance_method(:<方法名>).source_location  #查詢實體方法所在位置
2. caller的用法：
   - caller、caller_locations、caller[0..2]、caller_locations[2..4]

### API

1. rest-client(GitHub)：https://github.com/rest-client/rest-client

2. rest-client(get/post)：https://www.cnblogs.com/znsongshu/p/9462352.html

3. 使用 RestClient::Request.execute(sty_params)

   - get

   ````ruby
   self.sty_params = {
     method: :get,
     url: @key[:game_url],
     headers: {
       params: {
         proxy: @key[:proxy],
         requestTime: @timenow,
         d: self.class.aes_encrypt(description, @key[:des_key]),
         key: self.class.gen_key(@key, timenow),
       },
     },
   }
   ````

   - post：payload 的格式取決於 headers，可能為json, xml, txt...等等

   ````ruby
   self.sty_params = {
     method: :post,
     url: GamesV3::Mg::MG_MEMBER_BASE_URL,
     payload: builder.doc.root.to_xml, 
     headers: {
       'Content-Type' => 'application/xml',
       'X-Requested-With' => 'X-Api-Client',
       'X-Api-Call' => 'X-Api-Client',
     },
   }
   
   # 其中 builder
   def builder
     Nokogiri::XML::Builder.new do |xml|
       xml.send(@xml_call, @params)
     end
   end
   ````

   ````ruby
   self.sty_params = {
     method: :put,
     url: GamesV3::Mg::MG_DOMAIN + "/lps/secure/network/#{@token_id}/downline",
     payload: @params.to_json,
     headers: {
       'Content-Type' => 'application/json',
       'X-Requested-With' => 'X-Api-Client',
       'X-Api-Call' => 'X-Api-Client',
       'X-Api-Auth' => @token,
     },
   }
   ````

   ````ruby
   self.sty_params = {
     method: :post,
     url: GamesV3::Mg::MG_DOMAIN + '/lps/j_spring_security_check',
     payload: @params,
     headers: {
       'X-Requested-With' => 'X-Api-Client',
       'X-Api-Call' => 'X-Api-Client',
     },
   }
   ````

   - put 

     How to use:  RestClient.**put**(url, payload, headers = {}, &block) ⇒ `Object`

   ````ruby
   self.sty_params = {
     method: :put,
     url: GamesV3::Mg::MG_DOMAIN + "/lps/secure/network/#{@token_id}/downline",
     payload: @params.to_json,
     headers: {
       'Content-Type' => 'application/json',
       'X-Requested-With' => 'X-Api-Client',
       'X-Api-Call' => 'X-Api-Client',
       'X-Api-Auth' => @token,
     },
   }
   ````

   - 格式
   
     POST https://host:8443/my/path/to/endpoint 
     HTTP/1.1 Content-Type: application/x-www-form-urlencoded 
   
     arg0=val0&arg1=val1&arg2=val2

### 基本用法

1. is_a?

   Returns true if *class* is the [class](https://apidock.com/ruby/Object/class) of *obj,* or if *class* is one of the superclasses of *obj* or modules included in *obj*.

   參考資料：https://apidock.com/ruby/Object/is_a%3F

2. 針對第一點的補充說明，說明.class、instance_of?、 equal?、is_a?、==、===、.eql?的使用方式

   參考資料1：https://ithelp.ithome.com.tw/articles/10160216

   參考資料2：https://ithelp.ithome.com.tw/articles/10205276?sc=iThelpR

3. 類別方法

   ````ruby
   class IronmanList
     class << self
       def find(id)
         ...
       end
     end
   end
   ````

   ````ruby
   class IronmanList
     def self.find(id) 
       ...
     end
   end
   ````

4. check_method = game_object.class.method(:CheckState)

   以上這句話的意思是，呼叫game_object實體類別的方法，呼叫的方法為CheckState

5. flat_map的使用方式：https://apidock.com/ruby/Enumerable/flat_map

   輸入：[false,true].flat_map {|i| i }  

   輸出：[
                  [0] false,
                  [1] true
               ]

6. split：https://apidock.com/ruby/String/split

7. .nil? .empty? .blank? .present? 

   .present? <-> .blank?

   參考資料：https://mgleon08.github.io/blog/2015/12/16/ruby-on-rail-nil-empty-blank-present/

8. Ruby Arrays: Insert, Append, Length, Index, Remove

   參考資料：https://gistpages.com/posts/ruby_arrays_insert_append_length_index_remove

9. map用法：https://www.rubyguides.com/2018/10/ruby-map-method/

10. 不管是哪邊都很重要的正規表達式：https://larry850806.github.io/2016/06/23/regex/

13. do..each：https://motion-express.com/blog/20141010-ruby-block-map-collect

14. send 動態叫入方法：https://ruby-china.org/topics/4313

    ````ruby
    print "Search for: "
    request = gets.chomp
     
    if request == "writer"
      puts book.writer
    elsif request == "press"
      puts book.press
    elseif request == "date"
      puts book.date
    ......
    ````

    可以改寫成

    ````ruby
    request = gets.chomp
     
    if book.respond_to?(request)
      puts book.send(request)
    else
      puts "Input error"
    end
    ````

    ````ruby
    # 官方的使用方式
    
    class Klass
      def hello(*args)
        "Hello " + args.join(' ')
      end
    end
    k = Klass.new
    k.send :hello, "gentle", "readers"   #=> "Hello gentle readers"
    ````

    參考資料：https://apidock.com/ruby/Object/send

15. respond_to? 的用法：

    ````ruby
    obj = Object.new
    if obj.respond_to?("talk")
       obj.talk
    else
       puts "Sorry, object can't talk!"
    end
    
    # 若找不到talk，也不會跳出undefined method 'talk' for #<Object:0x12345678> (NoMethodError)的錯誤
    ````

    參考網址：http://galeki.is-programmer.com/posts/183.html

16. 比較一下，respond_to 是rails 的用法，與 respond_to? 完全是不一樣的意思

    `respond_to` is a [**Rails** method](http://apidock.com/rails/ActionController/MimeResponds/respond_to) for responding to particular request types. For example:

    ```ruby
    def index
      @people = Person.find(:all)
    
      respond_to do |format|
        format.html
        format.xml { render :xml => @people.to_xml }
      end
    end
    ```

17. input: {a:6,b:7}.to_query
    output: "a=6&b=7"

18. instance_variable_get 使用方法

    ````ruby
    class Fred
      def initialize(p1, p2)
        @a, @b = p1, p2
      end
    
      def find
        puts instance_variable_get("@a")
      end
    end
    fred = Fred.new('cat', 99)
    puts fred.instance_variable_get(:@a)    #=> "cat"
    puts fred.instance_variable_get("@b")   #=> 99
    fred.find
    ````

19. instance_variable_set 使用方式

    ````ruby
    class Fred
      def initialize(p1, p2)
        @a, @b = p1, p2
      end
    end
    fred = Fred.new('cat', 99)
    fred.instance_variable_set(:@a, 'dog')   #=> "dog"
    fred.instance_variable_set(:@c, 'cat')   #=> "cat"
    fred.inspect                             #=> "#<Fred:0x401b3da8 @a=\"dog\", @b=99, @c=\"cat\">"
    ````

20. module 不能被直接取用

    ````ruby
    module Helper
      def print_map
        puts "Hello!!!"
      end
    end
    
    if Helper.respond_to?('print_map')
      Helper.print_map
    else
      puts("METHOD isn't exist")
    end
    
    # => METHOD isn't exist
    ````

21. self.[]用法：自己定義運算子

    ````ruby
    class MyClass
      def self.[](arg)
        "hello #{arg}"
      end
    
      def [](arg)
        "Hi #{arg}"
      end
    end
    
    puts MyClass[2]
    puts MyClass.new[3]
    ````

22. Ohm 筆記

    ````ruby
       "cat/my_book".camelize # => "Cat::MyBook"
       "Cat::MyBook".underscore # => "cat/my_book"
       "cat/my_book".dasherize # => "cat/my-book"
       "book".pluralize            # => "books"
       "person".pluralize          # => "people"
       "fish".pluralize            # => "fish"
       "books".singularize            # => "book"
       "people".singularize           # => "person"
       "book_and_person".pluralize # => "book_and_people"
       "book and person".pluralize # => "book and people"
       "BookAndPerson".pluralize   # => "BookAndPeople"
       "books_and_people".singularize # => "books_and_person"
       "books and people".singularize # => "books and person"
       "BooksAndPeople".singularize   # => "BooksAndPerson"
       "cat/my_books".humanize # => "Cat/My books"
       "cat/my_books".titleize # => "Cat/My Books"
       "cat/my_book".classify  # => "Cat::MyBook"
       "cat/my_books".classify # => "Cat::MyBook"
       "cat/my_book".tableize # => "cat/my_books"
       "Cat::MyBook".tableize  # => "cat/my_books"
       "Module".constantize     # => Module
       "A::b::C".deconstantize # >> A::B
       "A::b::C".demodulize # >> C
    ````

23. 兩種寫法差很多。寫法2省了很多效能，因為當程式進入到@currency，若@currency賦值變直接跳出。

    ````ruby
    # 寫法1
    def currency(currency, option = {})
      currency = option&.dig(currency) || currency
      @currency ||= currency
    end
    
    # 寫法2
    def currency(currency, option = {})
      @currency ||= option&.dig(currency) || currency
    end
    ````

24. 在ensure底下如果要回傳值，一定要寫return，而且最好不要在ensure寫回傳值

25. __method\__使用方法

    ````ruby
    class A
      def foo
        puts "A#foo: `I am #{method(__method__)}'"
      end
    end
    
    class B < A
      def foo
        puts "B#foo: `I am #{method(__method__)}'"
        super
      end
    end
    
    A.new.foo # A#foo: `I am #<Method: A#foo>'
    B.new.foo # B#foo: `I am #<Method: B#foo>'
    
    # B#foo: `I am #<Method: B#foo>'
    # A#foo: `I am #<Method: A#foo>' # <- this is what I want
    ````

28. Ruby idioms(慣例) : The splat operator

    - The `*` is called the "splat operator"; I'm not sure I could give you the technical definition (though I'm sure you'd find it soon enough with Google's help), but the way I'd describe it is that it basically takes the place of hand-writing multiple comma-separated values in code.
    - splat operator可以把array展開
    - first, *rest, last  = ["a", "b", "c", "d"]  => first: "a", rest: ["b", "c"], last: "d"

    

    The double splat operator came out back in Ruby 2.0. It’s pretty similar to the original splat with one difference: it can be used for hashes.

    reference: https://ithelp.ithome.com.tw/articles/10206862

29. 類別裡頭有沒有定義常數：self.class::const_defined?(:<常數>)

    ````ruby
    def get_sty_flow(sty_opt)
      return sty_opt[:strategy] if sty_opt[:strategy].present?
      return self.class::STRATEGY if self.class::const_defined?(:STRATEGY)
      DEFAULT_STRATEGY
    end
    ````

30. %w quotes like single quotes `''` (no variable interpolation, fewer escape sequences), while %W quotes like double quotes `""`

    ````ruby
    irb(main):001:0> foo="hello"
    => "hello"
    irb(main):002:0> %W(foo bar baz #{foo})
    => ["foo", "bar", "baz", "hello"]
    irb(main):003:0> %w(foo bar baz #{foo})
    => ["foo", "bar", "baz", "\#{foo}"]
    ````

    `%w`, `%i`, `%` 的用法
    <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190923154400999.png" alt="image-20190923154400999" style="zoom:50%;" />

31. 陣列用法

    ````ruby
    ## 陣列疊代
    Array.each{|element|}
    Array.collect{|element|}
    Array.map{|element|}
    Array.select{|element|}    # 選出回傳為 true 的 element
    Array.reject{|element|}    # 刪掉回傳為 true 的 element
    Array.each.with_index{|element, index|}
    Array.all?{|element|}      # 如果陣列裡面沒東西也會回傳 true
    Array.any?{|element|}
    
    ## 陣列取用
    Array.slice([start], [length])
    
    ## 添加元素
    Array.push([element])
    Array.insert([index], [element])
    Array.unshift([element])  # 添加在最前面
    
    ## 其他
    Array.flatten      # 拆成一維陣列
    Array.uniq         # 移除陣列中相同的值
    [*'A'..'Z']        # 產生 A...Z 的陣列
    foo, bar = ['a', 'b']  # 陣列的解構賦值
    ````

33. Proc 將 Block 物件化：https://railsbook.tw/chapters/07-ruby-basic-3.html

    ````ruby
    say_hello_to = Proc.new { |name| puts "你好，#{name}"}
    
    say_hello_to.call("尼特羅會長")    # 使用 call 方法
    say_hello_to.("尼特羅會長")        # 使用小括號（注意，有多一個小數點）
    say_hello_to["尼特羅會長"]         # 使用中括號
    say_hello_to === "尼特羅會長"      # 使用三個等號
    say_hello_to.yield "尼特羅會長"    # 使用 yield 方法
    
    # 這幾種方式都可以呼叫 Proc 物件
    ````

34. 重要！fetch(Hash): https://apidock.com/ruby/Hash/fetch 

    ````ruby
    h = { "a" => 100, "b" => 200 }
    h.fetch("a")                            #=> 100
    h.fetch("z", "go fish")                 #=> "go fish"
    h.fetch("z") { |el| "go fish, #{el}"}   #=> "go fish, z"
    
    h = { "a" => 100, "b" => 200 }
    h.fetch("z") # => prog.rb:2:in `fetch': key not found (KeyError)
    ````

35. 重要！merge

    ````ruby
    h1 = { "a" => 100, "b" => 200 }
    h2 = { "b" => 254, "c" => 300 }
    h1.merge(h2)   #=> {"a"=>100, "b"=>254, "c"=>300}
    h1.merge(h2){|key, oldval, newval| newval - oldval}
                   #=> {"a"=>100, "b"=>54,  "c"=>300}
    h1             #=> {"a"=>100, "b"=>200}
    ````

36. URI.parse

    ```ruby
    URI::parse(uri_str)
    ```

    [String](https://apidock.com/ruby/String) with [URI](https://apidock.com/ruby/URI).

37. 什麼是block_given?

    ```ruby
    def f2(n, &p)
      if block_given?
        p[n] # call proc p
        # 'p[n]' can be alternated with 'p.call(n)'
        # 'yield n' also works
      else
        puts 'no block'
      end
    end
    
    f2(2){|n| n.times{puts 'iam block'}} # 印2次'iam block'
    f2(345)                              # 印出 'no block'(無論傳甚麼n進去都不影響)
    ```

    自己試試看

    ````ruby
    def f2(n, &p)
      if block_given?
        yield(n,n*n)
      else
        puts 'no block'
      end
    end
    
    f2(2){|n| n.times{puts 'I am block'}}
    ````

38. hash相關，什麼是slice?

    ````ruby
    > { a: 1, b: 2, c: 3, d: 4 }.slice(:a, :b)
    {
        :a => 1,
        :b => 2
    }
    ````

39. each_with_object 的用法

    ````ruby
    > %w(foo bar).each_with_object({}) { |str, hsh| hsh[str] = str.upcase }
    {
        "foo" => "FOO",
        "bar" => "BAR"
    }
    ````

    以 Evan 要 mapping 花色來說

    ````ruby
    mapping = {
      0 => '♠',
      1 => '♥',
      2 => '♣',
      3 => '♦',
    }
    
    (1..52).to_a.each_with_object({}) do |number, hsh|
      a = number / 13
      b = number % 13
      if b.zero?
        a -= 1
        b = 13
      end
      case b
      when 1 then b = "A"
      when 11 then b = "J"
      when 12 then b = "Q"
      when 13 then b = "K"
      else b
      end
      hsh[number] = "#{mapping[a]}#{b}"
    end
    
    ````

40. reject 的用法

    ````ruby
    # Remove even numbers
    (1..30).reject { |n| n % 2 == 0 }
    # => [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29]
    
    result = [{ a: 1, b: 5 }, { a: 2 , b: 6 }, { a: 2, b: 7 }, { a: 4, b: 8 }]
    result.reject {|content| content[:a].eql? 2 }
    ````

41. .to_boolean

    ````ruby
    > "f".to_boolean
    false
    > "fa".to_boolean
    true
    > "fal".to_boolean
    true
    > "fals".to_boolean
    true
    > "false".to_boolean
    false
    ````

42. args

    ````ruby
    def print_all(*args)
      args.second
    end
    
    print_all(1, 2, 3, 4, 5)
    ````

43. 若 ruby 沒有設定任何回傳值，預設為回傳 true

44. 使用form_for：https://apidock.com/rails/ActionView/Helpers/FormHelper/form_for

45. 如果要require，class要改成module：https://stackoverflow.com/questions/47119729/rails-loading-custom-class-from-lib-folder-in-controller/47119932

46. .try() 與 &. : http://mitrev.net/ruby/2015/11/13/the-operator-in-ruby/

47. 使用rebase從把舊的分支接到最新的dev上面：https://gitbook.tw/chapters/branch/merge-with-rebase.html

48. 在測試中，should 跟 expect(response).to 都不是好寫法，寫成 is_expected.to

    參考網址-如何寫好的測試：http://www.betterspecs.org/

49. Turn a new line(\n) into a <\br> tag with Ruby

    參考資料：https://coderwall.com/p/nbzpta/turn-a-new-line-n-into-a-br-tag-with-ruby

50. 如何使用byebug：
    next: 跳到下一行
    step: 若該行有行式，則進入該行
    up/down: up可以從step跳出來 

    參考資料：https://rails.ruby.tw/debugging_rails_applications.html

    參考資料：https://rubyplus.com/articles/3631-Debugging-using-ByeBug-Gem-in-Rails-5

51. pry-debug利器 :

    - 安裝：https://github.com/pry/pry

    - 使用：http://blog.xdite.net/posts/2012/08/13/pry-the-new-debugger

    - continue，继续执行程序直到结束

      finish: 继续执行直到当前frame结束

      next: 继续执行当前frame内的命令

      step: 跳过当前，进入下一个frame。

52. Monkey Patch：http://noelsaga.herokuapp.com/blog/2018/01/23/ruby-shi-yong-monkey-patch-de-jian-yi

53. Ruby "initialize" method : https://www.rubyguides.com/2019/01/ruby-initialize-method/

54. How to Use attr_accessor, attr_writer & attr_reader : https://www.rubyguides.com/2018/11/attr_accessor/

55. `to_sym` ：https://apidock.com/ruby/String/to_sym
    `instance_variable_set`：https://apidock.com/ruby/Object/instance_variable_set
    <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190919165238649.png" alt="image-20190919165238649" style="zoom:50%;" />

56. The most interesting addition to Ruby 2.3.0 is the Safe Navigation Operator(`&.`)：
    http://mitrev.net/ruby/2015/11/13/the-operator-in-ruby/

57. to_proc`和 `map` 用法之一
    <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190919170122109.png" alt="image-20190919170122109" style="zoom:100%;" />

    `to_proc`和 `map` 用法之二

    <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190919170203535.png" alt="image-20190919170203535" style="zoom:50%;" />

53. Ruby 數字格式：https://idiosyncratic-ruby.com/49-what-the-format.html

54. Ruby的self物件與singleton method : https://ithelp.ithome.com.tw/articles/10200398

55. Rails Polymorphic relationship and link_to：https://stackoverflow.com/questions/1283858/rails-polymorphic-relationship-and-link-to

61. each_if in ruby：https://stackoverflow.com/questions/5212380/is-there-an-each-if-in-ruby

#### Hash

1. _options = {} : 為rails中的慣例。rails中，有些方法必須要丟相應的參數，但那些參數卻用不到。
   參考資料：https://stackoverflow.com/questions/18407618/what-are-options-hashes

2. .dig(適用於Array|Hash)：https://ruby-china.org/topics/32080

   使用 dig() 來取用hash

   ```ruby
   hash = {
     image: {
       '1x': 'v6/customer_brands/openstack.png',
       '2x': 'v6/customer_brands/openstack@2x.png'
     }
   }
   
   hash.dig(:image, :'1x')         # 'v6/customer_brands/openstack.png',
   hash.dig(:image, :foo, :bar)    # nil
   
   hash[:image][:'1x']             # "v6/customer_brands/openstack.png"
   hash[:image][:'foo'][:'bar']    # undefined method `[]' for nil:NilClass
   ```

3. hash跟array可以直接指派值進去，舉例來說：

   ```ruby
   > a = []; a[5]=20;
   > a
   [
       [0] nil,
       [1] nil,
       [2] nil,
       [3] nil,
       [4] nil,
       [5] 20
   ]
   ```

4. 有了 Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }，就可以隨便宣告 hash

   ```ruby
   a = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
   a[:a][:b] = 'chenhanting' # 不會產生錯誤
   
   a = {}
   a[:a][:b] = 'chenhanting' # 會產生錯誤
   ```

5. Hashes have a thing called a [`default_proc`](http://www.ruby-doc.org/core-2.0.0/Hash.html#method-i-default_proc), which is simply a proc that Ruby runs when you try to access a hash key that doesn't exist. This proc receives both the hash itself and the target key as parameters.

   You can set a Hash's `default_proc` at any time. Passing a block parameter to `Hash.new` simply allows you to initialize a Hash and set its `default_proc` in one step:

   ```ruby
   h = Hash.new
   h.default_proc = proc{ |hash, key| hash[key] = 'foo' }
   
   # The above is equivalent to:
   
   h = Hash.new{ |hash, key| hash[key] = 'foo' }
   ```

   We can also access the default proc for a hash by calling `h.default_proc`. Knowing this, and knowing that the ampersand (`&`) allows a proc passed as a normal parameter to be treated as a block parameter, we can now explain how this code works:

   ```ruby
   cool_hash = Hash.new{ |h, k| h[k] = Hash.new(&h.default_proc) }
   ```

   The block passed to `Hash.new` will be called when we try to access a key that doesn't exist. This block will receive the hash itself as `h`, and the key we tried to access as `k`. We respond by setting `h[k]` (that is, the value of the key we're trying to access) to a new hash. Into the constructor of this new hash, we pass the "parent" hash's `default_proc`, using an ampersand to force it to be interpreted as a block parameter. This is the equivalent of doing the following, to an infinite depth:

   ```ruby
   cool_hash = Hash.new{ |h, k| h[k] = Hash.new{ |h, k| h[k] = Hash.new{ ... } } }
   ```

   The end result is that the key we tried to access was initialized to a new Hash, which itself will initialize any "not found" keys to a new Hash, which itself will have the same behavior, etc.

6. hash的基本用法：https://docs.ruby-lang.org/en/2.0.0/Hash.html

#### 其他

1. 面試精選(30天修煉Ruby面試精選30題)：https://ithelp.ithome.com.tw/users/20111177/ironman/1613

## Rails

### 網路常識

1. 什麼是payload?

   google到一篇很好的文章對payload為何這樣叫有很好的解釋，文中指出這個名詞是借用運輸工具上的觀念而來的，例如：卡車、油罐車、貨輪等所謂的**載具**，然後通常一個載具的總重量一定**大於**載具的承載量，例如油罐車的總重量包含了他所運載的油量、司機的重量、油罐車行駛所需的油量，但我們所關心僅是油罐車所承載的油量而已。對programming來說，我們可以某api的response為例子來看

   ````json
     {
         "status":"OK",
         "data":
             {
                 "message":"Hello, world!"
             }
     }
   ````

   其中的Hello, world!正是payload，也是我們關心的部分。到這裡我們應該了解為何參數名要叫做`payload`，而非`data`或是`params`是有其目的性的，而更進一步的熟悉與使用payload這個參數，則就要再深入看該方法或函式的使用與定義了。

   參考網址：http://noelsaga.herokuapp.com/blog/2015/07/18/kai-fa-zhong-,chang-jian-de-can-shu-payloadshi-shi-mo

### 基本功能

1. ActiveRecord::Enum：

   在raills使用enum的好處為速度比較快，以及使用index的方式在資料庫比較好查詢

   ````ruby
   class Conversation < ActiveRecord::Base
     enum status: [ :active, :archived ]
   end
   
   # 使用實體方法
   # conversation.update! status: 0
   conversation.active!
   conversation.active? # => true
   conversation.status  # => "active"
   
   # conversation.update! status: 1
   conversation.archived!
   conversation.archived? # => true
   conversation.status    # => "archived"
   
   # conversation.status = 1
   conversation.status = "archived"
   
   conversation.status = nil
   conversation.status.nil? # => true
   conversation.status      # => nil
   
   # 使用類別方法
   Conversation.active
   Conversation.archived
   
   # 使用query
   Conversation.where(status: [:active, :archived])
   Conversation.where.not(status: :active)
   ````

   參考資料：https://api.rubyonrails.org/v5.1.6/classes/ActiveRecord/Enum.html

2. rails logger： https://rails.ruby.tw/debugging_rails_applications.html

3. simple_form：http://simple-form-bootstrap.plataformatec.com.br/documentation

4. Rails 5 之後對 `belongs_to` 加入的新限制。如果覺得這樣有點麻煩想要關掉，可在 `belongs_to` 後面加上 `optional: true` 的參數

5. `before_save` *跟* `before_create` *的差別，在於* `before_save` *是每次存檔的時候都會經過，但* `before_create` *只有在「新增」的時候才會觸發。*

6. file_field 上傳圖片用法：https://apidock.com/rails/ActionView/Helpers/FormHelper/file_field

   ````ruby
   file_field(:user, :avatar)
   # => <input type="file" id="user_avatar" name="user[avatar]" />
   
   file_field(:post, :image, multiple: true)
   # => <input type="file" id="post_image" name="post[image][]" multiple="multiple" />
   
   file_field(:post, :attached, accept: 'text/html')
   # => <input accept="text/html" type="file" id="post_attached" name="post[attached]" />
   
   file_field(:post, :image, accept: 'image/png,image/gif,image/jpeg')
   # => <input type="file" id="post_image" name="post[image]" accept="image/png,image/gif,image/jpeg" />
   
   file_field(:attachment, :file, class: 'file_input')
   # => <input type="file" id="attachment_file" name="attachment[file]" class="file_input" />
   ````

7. 使用html表單時，客製csrf保護：https://stackoverflow.com/questions/23932425/authenticity-token-is-missing-in-custom-action

8. Frozen String Literal: 

   常數就不用加`.freeze`

   ````ruby
   # frozen_string_literal: true
   
   class BetLog::Ganapati < BetLog
   end
   ````

   參考資料：https://ithelp.ithome.com.tw/articles/10214518

9. 淺談Concern - 相同的方法要好好收納

   參考資料1：https://ithelp.ithome.com.tw/articles/10188073

   參考資料2：https://blog.niclin.tw/2019/07/30/rails-service-concern-library-different/

11. 好用除錯工具：

    - https://mgleon08.github.io/blog/2015/12/19/ruby-on-rails-debugging/
    - https://docs.gitlab.com/ee/development/pry_debugging.html

### Model

基本的CRUD請參照這篇網址：https://guides.rubyonrails.org/association_basics.html

When you declare a `belongs_to` association, the declaring class automatically gains 6 methods related to the association:

- `association`
- `association=(associate)`
- `build_association(attributes = {})`
- `create_association(attributes = {})`
- `create_association!(attributes = {})`
- `reload_association`

When you declare a `has_many` association, the declaring class automatically gains 17 methods related to the association:

- `collection`
- `collection<<(object, ...)`
- `collection.delete(object, ...)`
- `collection.destroy(object, ...)`
- `collection=(objects)`
- `collection_singular_ids`
- `collection_singular_ids=(ids)`
- `collection.clear`
- `collection.empty?`
- `collection.size`
- `collection.find(...)`
- `collection.where(...)`
- `collection.exists?(...)`
- `collection.build(attributes = {}, ...)`
- `collection.create(attributes = {})`
- `collection.create!(attributes = {})`
- `collection.reload`

When initializing a new `has_one` or `belongs_to` association you must use the `build_` prefix to build the association, rather than the `association.build` method that would be used for `has_many` or `has_and_belongs_to_many` associations. To create one, use the `create_` prefix.

以第十二週的進度來說，news_controller.rb 的 new `一對一`跟`一對多`的方法都寫在上面，可以提供參考

````ruby
class Admin::NewsController < Admin::BaseController
  ...
  def new
    @article = News.new
    # 一對多
    # @article.messages.build
    # @article.galleries.build

    # 一對一
    @article.build_message
    @article.build_gallery

    add_breadcrumb 'new', :new_news_path
  end
  ...
end
````

2. 使用 pluck 進行資料庫的 query：https://apidock.com/rails/ActiveRecord/Calculations/pluck

3. How to get params from url: https://stackoverflow.com/questions/4352839/rails-how-to-get-all-parameters-from-url

4. Reload the current page：https://stackoverflow.com/questions/7465259/how-can-i-reload-the-current-page-in-ruby-on-rails

5. Joey提醒：

   `update`, `create`-> 結果為true或false

   `update!`, `create!`-> 若出現錯誤會出現exception

6. after_save, after_commit 之間的關係

   以下是 Active Record 可用的回呼，**依照執行順序排序**：

   3.1 新建物件

   - `before_validation`
   - `after_validation`
   - `before_save`
   - `around_save`
   - `before_create`
   - `around_create`
   - `after_create`
   - `after_save`
   - `after_commit/after_rollback`

   ##### 3.2 更新物件

   - `before_validation`
   - `after_validation`
   - `before_save`
   - `around_save`
   - `before_update`
   - `around_update`
   - `after_update`
   - `after_save`
   - `after_commit/after_rollback`

   ##### 3.3 刪除物件

   - `before_destroy`
   - `around_destroy`
   - `after_destroy`
   - `after_commit/after_rollback`

7. 尋找資料庫的欄位（關鍵字：column, field）

   ````ruby
   > News.column_names
   => ["id", "title", "editor", "category", "content", "summary", "click_base", "click", "publish_at", "user_id", "created_at", "updated_at"]
   > News.column_names.include? 'click'
   => "true"
   ````

   講解：

   - For a class

     Use `Class.column_names.include? attr_name` where `attr_name` is the string name of your attribute. In this case: `Number.column_names.include? 'one'`

   - For an instance

     Use `record.has_attribute?(:attr_name)` or `record.has_attribute?('attr_name')` (Rails 3.2+) or `record.attributes.has_key? attr_name`.

     In this case: `number.has_attribute?(:one)` or `number.has_attribute?('one')` or `number.attributes.has_key? 'one'`

   - 參考資料：https://stackoverflow.com/questions/1710004/how-to-check-if-a-model-has-a-certain-column-attribute

8. default_scope

   ````ruby
   class Article < ActiveRecord::Base
     default_scope where(:published => true)
   end
   
   Article.all # => SELECT * FROM articles WHERE published = true
   ````

   參考資料：https://apidock.com/rails/ActiveRecord/Base/default_scope/class

   這一篇網站討論詬病：https://stackoverflow.com/questions/25087336/why-is-using-the-rails-default-scope-often-recommend-against

9. 實例演說

   - https://www.justinweiss.com/articles/search-and-filter-rails-models-without-bloating-your-controller/

#### Mongodb

1. new_record?

   Returns true if this object hasn’t been saved yet — that is, a record for the object doesn’t exist yet; otherwise, returns false.

2. find_or_create_by

   Finds the first record with the given attributes, or creates a record with the attributes if one is not found

3. find_or_initialize_by

   Like `find_or_create_by,` but calls `new` instead of `create`

### controller

1. controller 的每一個 action 裡面，都有 `request` 和 `response`

2. ActionController::ParameterMissing：https://stackoverflow.com/questions/20346320/actioncontrollerparametermissing-param-not-found-order/20346477

3. 為了`Gamatron`的`返回鍵`查詢到，控制器有`request.original_url`、`request.referrer`可以使用

   參考連結：https://apidock.com/rails/ActionDispatch/Request/original_url

   參考連結：https://stackoverflow.com/questions/4652084/ruby-on-rails-how-do-you-get-the-previous-url

### Tool

1. 如何強致終止rails server（大推）：https://stackoverflow.com/questions/4473229/rails-server-says-port-already-used-how-to-kill-that-process
2. request 常用的方法 `params`, `parameters`, `format`, `base_url`, `method`, `path`

### Devise

1. Devise 快速上手：https://andyyou.github.io/2015/04/04/devise/
2. 兩種用戶（user 跟 admin）：[https://medium.com/@tienshunlo_32785/rails-devise%E7%B3%BB%E5%88%97%E6%96%87-%E5%AF%A6%E4%BD%9C%E5%85%A9%E7%A8%AE%E7%94%A8%E6%88%B6-user-%E8%B7%9F-admin-e86018df4e88](https://medium.com/@tienshunlo_32785/rails-devise系列文-實作兩種用戶-user-跟-admin-e86018df4e88)
3. Making email as optional (not mandatory) field in devise in Rails
   - http://nirmalhota.com/2016/08/tip-making-email-as-optional-field-in-devise-in-rails/
   - https://ruby-china.org/topics/21620

### Routes

1. 實戰聖經：https://ihower.tw/rails/routing.html

2. 如何拿掉前綴：

   使用`scope module`

   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191212111856574.png" alt="image-20191212111856574" style="zoom:50%;" />

   參考資料：https://rails.ruby.tw/routing.html
   
3. `rails routes` 的用法
   參考資料：[https://pjchender.github.io/2017/10/02/rails-routes-%E7%9A%84%E4%BD%BF%E7%94%A8/](https://pjchender.github.io/2017/10/02/rails-routes-的使用/)

   ```cmd
   bin/rails routes                # 列出所有路由
   bin/rails routes -g admin       # 搜尋特定路由 (grep)
   bin/rails routes -c admin/users # 搜尋路由中特定 controller
   ```

### Form Helper

1. 私房菜（大推）：https://pjchender.github.io/2017/10/12/rails-action-view-form-helpers/

### Migration

1. 實戰聖經：https://ihower.tw/rails/migrations.html

### Rubocop

1. How to correctly use `guard clause` in Ruby

   ````ruby
   def require_admin
     unless current_user && current_user.role == 'admin'
       flash[:error] = "You are not an admin"
       redirect_to root_path
     end        
   end
   
   # 改成以下方式 
   # 說明：You can use the return statement here. Essentially, there is no need for the method to continue if those conditions are met, so you can bail out early.
   def require_admin
     return if current_user && current_user.role == 'admin'
   
     flash[:error] = "You are not an admin"
     redirect_to root_path
   end
   ````

### RestClient

1. stack overflow

   ```ruby
   require 'rest-client'
   
   RestClient.get 'http://example.com/resource', {:params => {:id => 50, 'foo' => 'bar'}}
   RestClient.get 'http://example.com/resource'
   
   xml = '<xml><foo>bar</foo><bar>foo</bar></xml>'
   RestClient.post 'http://example.com/resource', xml , {:content_type => :xml}
   RestClient.put 'http://example.com/resource', xml , {:content_type => :xml}
   
   RestClient.delete 'http://example.com/resource'
   ```

   參考網址：https://stackoverflow.com/questions/8452410/rest-client-example-in-ruby

### Validation

1. 驗證日期、時間：https://code-examples.net/zh-TW/q/14eb2e

### Gem

1. Annotate
   - https://medium.com/%E5%AF%A6%E7%94%A8%E7%9A%84rubygems/annotate-%E5%9C%A8activerecord%E9%96%8B%E9%A0%AD%E9%A1%AF%E7%A4%BA%E8%B3%87%E6%96%99%E8%A1%A8%E5%85%A7%E5%AE%B9-a93d7efb459d
   - https://www.driftingruby.com/episodes/annotate
2. High-Voltage: https://github.com/thoughtbot/high_voltage

### 好文

1. Leon：https://mgleon08.github.io/blog/categories/rails/

2. Nic：https://blog.niclin.tw/

3. 簡易票選系統（高見龍）：https://railsbook.tw/chapters/13-crud-part-1.html

    

# Sinatra

1. 高見龍老師講sinatra：https://www.slideshare.net/ryudoawaru/mopcon2014

## Docker

1. 用 Docker 部署 Rails，原來是這樣：https://5xruby.tw/posts/deploying-your-docker-rails-app/

## 待看影片

圖片區

![image-20191219134005288](/Users/hanting/Library/Application Support/typora-user-images/image-20191219134005288.png)



1. Strong Params: https://www.driftingruby.com/episodes/complex-strong-parameters
2. View Form
   - https://www.driftingruby.com/episodes/not-rjs-and-turbolinks
   - https://www.driftingruby.com/episodes/not-rjs-and-turbolinks-part-2
   - https://www.driftingruby.com/episodes/dry-up-your-javascript
   - https://www.driftingruby.com/episodes/client-side-validations
   - https://ihower.tw/rails/actioncontroller.html（講解crsf token）
   - 跳脫controller：https://www.driftingruby.com/episodes/actioncontroller-renderer
3. Websocket: 
   - https://www.driftingruby.com/episodes/faye-websockets-part-1
   - https://www.driftingruby.com/episodes/faye-websockets-part-2
4. SEO: https://www.driftingruby.com/episodes/meta-tags
5. 增進效能：https://www.driftingruby.com/episodes/eager-loading-with-goldiloader
6. 部署：https://www.driftingruby.com/episodes/production-deployment-on-ubuntu
7. bundle open：看Gem裡面的程式碼，相關設定要 export
8. calendar：https://www.driftingruby.com/episodes/fullcalendar-events-and-scheduling
9. pundit：https://www.driftingruby.com/episodes/authorization-with-pundit
10. rails api：
    - https://www.driftingruby.com/episodes/rails-api-basics
    - https://www.driftingruby.com/episodes/rails-api-active-model-serializer
    - https://www.driftingruby.com/episodes/rails-api-authentication-with-jwt (已經在電腦上)
11. add index in database: https://www.driftingruby.com/episodes/database-index-optimization-and-migration-maintenance
12. chart: https://www.driftingruby.com/episodes/charts-and-graphs
13. redis: https://www.driftingruby.com/episodes/redis-basics
14. sidekiq: https://www.driftingruby.com/episodes/sidekiq-basics
15. 加密：https://www.driftingruby.com/episodes/client-side-encryption
16. ruby技巧：https://www.driftingruby.com/episodes/random-ruby-tips-and-tricks
17. Search：https://www.driftingruby.com/episodes/searchkick-and-elasticsearch
18. locale：https://www.driftingruby.com/episodes/working-with-internationalization
19. Nested Form：
    - https://www.driftingruby.com/episodes/nested-forms-from-scratch
    - https://www.driftingruby.com/episodes/nested-forms-with-cocoon
20. JS
    - 某頁面專用JS：https://www.driftingruby.com/episodes/page-specific-javascript-in-ruby-on-rails
    - 配合 Form：https://www.driftingruby.com/episodes/javascript-select-form-fields-with-chosen
    - 裁切照片：https://www.driftingruby.com/episodes/cropping-images-with-jcrop
21. Routes
    - 高級用法：https://www.driftingruby.com/episodes/routing-partials
22. Data Tables：https://www.driftingruby.com/episodes/datatables
23. Subdomain：https://www.driftingruby.com/episodes/datatables
24. 肥貓的遊戲服務：https://www.driftingruby.com/episodes/creating-custom-ruby-on-rails-generators
25. Error
    - Slack：https://www.driftingruby.com/episodes/custom-error-pages-with-slack-notification
26. database：https://www.driftingruby.com/episodes/activerecord-migrations
27. Autocomplete Search：https://gorails.com/episodes/global-autocomplete-search?autoplay=1
28. 部落格系統範例：https://snipcart.com/blog/rails-ecommerce-tutorial-refinery-cms
29. Using VueJS for Nested Forms in Rails
    - Part 1：https://gorails.com/episodes/using-vuejs-for-nested-forms-part-1?autoplay=1
    - Part 2：https://gorails.com/episodes/using-vuejs-for-nested-forms-part-2?autoplay=1



