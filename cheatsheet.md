# Cheat Sheet

### Time

````ruby
DateTime.strptime("1571393643",'%s') # start-time
DateTime.strptime("1571393748",'%s') # end-time

Time.at(1571393643)
Time.at(1571393749)

Time.zone.parse('2019.12.25 17:00:00').to_i*1000
Time.zone.parse('2019.12.25 17:05:00').to_i*1000

DateTime.now.strftime('%Q')
````

````ruby
> Time.parse(bet['startTime']).to_i
1572855623

> Time.zone.at(1572855623).strftime("%Y-%m-%d %H:%M:%S")
"2019-11-04 16:20:23"

> bet['startTime']
"2019-11-04 16:20:23"

> Time.now.localtime("+05:30")
2019-11-06 07:17:41 +0530
> Time.now.localtime("+05:30")
2019-11-06 07:17:46 +0530
> Time.now.localtime("+00:30")
2019-11-06 02:17:54 +0030
> Time.now.localtime("+00:00")
2019-11-06 01:47:58 +0000
> Time.now.localtime("+08:00")
2019-11-06 09:48:08 +0800
````

### 連接Action Cable

必要設定

```ruby
# config/environments/development.rb
  config.action_cable.url = 'ws://localhost:3000/cable'

# app/assets/javascripts/client/cable.js.erb
  SocketChannel.cable = ActionCable.createConsumer('ws://' + host + '/cable?token='+ getCookie('user_token'));

# secrets.yml
  action_cable:
    host: 'localhost:3000'
    allowed_origin: localhost
```

開啟設定

- 在終端機輸入 sidekiq，開啟排程（3000 port）（與後台同一個port）
- 後台開的時候跟 sidekiq一起開
- sidekiq monitor：127.0.0.1:3000/sidekiq

### 使用Heroku

資料庫遷移： heroku run rake db:migrate 

遠端控制model：heroku run rails console

### Rails logger

以下為使用 console 呼叫 poker_rmg.log

```shell
cd log
ls
tail -f poker_rmg.log
```

在 ruby 使用 logger

```ruby
Rails.logger.poker_rmg.info("...")
Rails.logger.poker_rmg.error("...")
Rails.logger.poker_rmg.warning("...")
```

### Rails 5.0.0

Run Server: `bundle exec rails server -p 3004`