## 第一週放假-中秋連假

### 工具 

1. Typora to Word
   安裝：https://pandoc.org/installing.html
   使用：pandoc -o README.docx README.md
   參考網址：https://blog.miniasp.com/post/2018/10/06/Useful-tool-Pandoc-universal-document-converter
2. 使用git的小技巧：https://lighter.github.io/2016/07/09/19-tips-for-everyday-git-use/
3. 如果要看 word 的目錄，請打開功能窗格
4. mac快捷鍵：https://support.apple.com/zh-tw/HT201236
5. 全棧營的網路概論
   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190915071548535.png" alt="image-20190915071548535" style="zoom:50%;" />
6. 取消復原：command+shift+z
   VScode 直行註解：alt+option+mouse
7. Linux創建文件夹，文件並寫入内容：https://blog.csdn.net/u013101178/article/details/81383242
8. Cherry-pick: https://cythilya.github.io/2018/05/30/git-cherry-pick/
9. Github 中的 ssh、https 路徑有什麼差異？ - 如何設定 Github SSH 金鑰：https://wcc723.github.io/git/2018/02/12/github-ssh-https/

### 收到的建議

- 張嘉儒：嘉儒從一位老闆的角度鼓勵我，他自己當老闆培訓員工的時候，當員工態度夠積極，基本上自己給薪會比較踏實。告訴我只要願意認真努力，基本上不會有任何問題。

- 張印萱

  ![image-20190914144727875](/Users/hanting/Library/Application Support/typora-user-images/image-20190914144727875.png)

#### 傷科口袋名單

1. 明悅和欣悅（打勾是明悅、欣悅是松山分店）
   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190914145044014.png" alt="image-20190914145044014" style="zoom:20%;" />
2. 土城仁山堂的洪醫生及推拿師群

### 全棧營

1. 如何設置後台：

   *Namespace*是*Scope*的一種特定應用，特別適合例如後台介面，這樣就整組`controller`、網址`path`、*URL Helper*前置名稱`都影響到：

   ```ruby
   namespace :admin do
     resources :projects
   end
   ```

   如此*controller*會是`Admin::ProjectsController`，網址如*/admin/projects*，而*URL Helper*如`admin_projects_path`這樣的形式。

   在*Namespace*下也可以設定它的首頁，例如：

   ```ruby
   namespace :admin do
   	root "projects#index"
   end
   ```

   就樣連`http://localhost:3000/admin/`就會使用*ProjectsController index action*了。
   參考網址：https://ihower.tw/rails/routing.html

## 第二週放假

以下為第二週放假研究的項目

### RubyMine

1. Do one of the following:
   - On the context menu of the selection, click Diagram, and on the submenu, select the way you want to view the model: Show Diagram or Show Diagram Pop-up.
   - Press ⌥⇧⌘U or ⌥⌘U.
2. You can open a UML class diagram without using your pointing device. Consider such a workflow: press ⌘↑, then press ⌥⌘U.
3. 常用的快捷鍵
  

| Shortcut    | Action                                                       |
| ----------- | ------------------------------------------------------------ |
| Double ⇧    | [Search Everywhere](https://www.jetbrains.com/help/ruby/searching-everywhere.html)Find anything related to RubyMine or your project and open it, execute it, or jump to it. |
| ⌃⇧A         | [Find Action](https://www.jetbrains.com/help/ruby/searching-everywhere.html#find_action)Find a command and execute it, open a tool window or search for a setting. |
| ⌃N ⌃⇧N ⌃⌥⇧N | [Find a class, file, or symbol](https://www.jetbrains.com/help/ruby/searching-everywhere.html)Find and jump to the desired class, file, or symbol. |
| Double ^    | Run AnythingExecute commands, such as opening a project, launching a run/debug configuration, running a command-line utility, and so on. The available commands depend on the set of plugins and tools you have configured for your project. |
| ⌃E          | [View recent files](https://www.jetbrains.com/help/ruby/navigating-through-the-source-code.html#recent_files)Select a recently opened file from the list. |
| ⌥⏎          | [Show intention actions](https://www.jetbrains.com/help/ruby/intention-actions.html)Improve or optimize a code construct. |
| ⌃Space      | [Basic code completion](https://www.jetbrains.com/help/ruby/auto-completing-code.html#basic_completion)Complete names of classes, methods, fields, and keywords within the visibility scope. |
| ⌃W ⌃⇧W      | [Extend or shrink selection](https://www.jetbrains.com/help/ruby/working-with-source-code.html)Increase or decrease the scope of selection according to specific code constructs. |
| ⌃/ ⌃⇧/      | [Add/remove line or block comment](https://www.jetbrains.com/help/ruby/working-with-source-code.html#editor_lines_code_blocks)Comment out a line or block of code. |
| ⌃⇧F7        | Highlight usages in a fileHighlight all occurrences of the selected fragment in the current file. |

4. debugger : https://www.cnblogs.com/chiangchou/p/idea-debug.html

### Grape

1. https://niclin.gitbooks.io/grape-on-rails-101/
2. https://mgleon08.github.io/blog/2019/03/15/rails-with-grape/
3. https://mikecoutermarsh.com/rails-grape-api-key-authentication/
4. http://jokercatz.blogspot.com/2015/05/ruby-sinatra-rails-grape-swagger.html

### 家常菜

[https://cookpad.com/tw/%E6%90%9C%E5%B0%8B/%E5%AE%B6%E5%B8%B8%E8%8F%9C](https://cookpad.com/tw/搜尋/家常菜)

## 第三週放假

1. composer安裝：https://duvien.com/blog/installing-composer-mac-osx
2. 重新啟動bashfile:  source ~/.bash_profile
3. laravel installation: https://laravel.com/docs/5.8/installation
4. bootstrap table 對齊：https://blog.csdn.net/Peng_Hong_fu/article/details/70662979

## 第五週放假

1. mac護眼功能：https://www.playpcesor.com/2017/03/apple-mac-Night-Shift.html

