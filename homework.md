# 回家作業

1. http/https的差異:
   http://跟https://之間的不同在於網路文字傳送協定中標準的不同，您可以在Google瀏覽器Chrome上瞭解，在網址欄位上連結為https開始的網址會多顯示"安全"標示，讓您簡單的瞭解網站對於使用者的友善性。 http是網頁與您的電腦瀏覽器直接透過明文進行傳輸，以一般(非安全)模式下進行互動交談，所以在網際網路上內容有可能遭攔有心人士截竊聽的，HTTP協定不使用加密協定，其中原因包含：加密會多消耗許多運算資源，也會佔用更多的傳輸頻寬，而緩存機制跟著會失效。

   HTTPS協定，以保密為前提為研發，可以算是HTTP的進階安全版。是以加入SSL協定作為

   安全憑證，因此網站透過協定上的加密機制後能夠防止資料竊取者就算攔截到了傳輸資訊卻也無法直接看到傳輸中的資料，也因此較大型有串聯金融信用機制會使用到較敏感度資料的企業網站多會選擇使用HTTPS協定，提供保障客戶在網站上的使用資訊。

   參考網址：https://www.webdesigns.com.tw/HTTPorHTTPS.asp

2. Chrome相容性：http://www.manongjc.com/article/65956.html

3. Html、CSS、Javascript

   ![img](https://i.imgur.com/GIqgBgG.jpg)

   - **HTML** 就是管理網頁的架構，一個好的網頁其 **HTML** 可以說是相當單純且具有易讀性，這種網頁不但方便前端工程師進行後續的維護外，也比較容易讓你的網頁增加曝光率讓搜尋引擎能把你的網頁擺在最前面，此種行為稱為 [SEO](https://zh.wikipedia.org/wiki/搜尋引擎最佳化)，而目前 **HTML** 已經發布到第五版同時也是目前最廣為使用的版本，因此大家常聽到的 **HTML5** 其實就是指第五版的 **HTML** 
   - **CSS** 就是管理網頁的外貌，**CSS** 之於網頁如同化妝品之於人類，因此 **CSS** 相當考驗工程師的美感，像筆者就完全沒有美感不懂設計刻出來的網頁通常都奇醜無比XD，而目前 **CSS** 已經發布到第三版同時也是目前最廣為使用的版本，因此大家常聽到的 **CSS3** 其實就是指第三版的 **CSS**
   -  **JavaScript** 的存在就是為了要控制網頁的內容，所以可以猜想到 **JavaScript** 最重要的概念就是要控制 **HTML** 中的各個 **element**，既然要控制 **element** 那勢必得提供相對應的 [Web API](https://developer.mozilla.org/zh-TW/docs/Web/API)，在剛剛的連結中可能會被裡面琳瑯滿目的 **API** 嚇到，這邊筆者建議看懂 [DOM API](https://developer.mozilla.org/zh-TW/docs/Web/API/Document) 即可，更詳細的 **API** 介紹會在下一篇文章加以敘述，講了這麼多好像都沒有真正提到如何寫 **JavaScript**，這邊跟 **CSS** 一樣有三種方法，而且三種方法都是一樣的道理喔 

   參考資料：https://ithelp.ithome.com.tw/articles/10202326

4. css/scss/sass: 

   CSS用來控制網頁外觀，不算是一種可程式化語言，為許多屬性設定的組合，但CSS的缺點是很難有組織性，且不易維護，CSS Preprocessor因而誕生。定義一種新語言，透過語言進行網頁樣式設計，再轉換生成CSS檔，為撰寫樣式的過程加上語言程式特性，讓程式更簡潔，可讀性更好更容易維護。

   SCSS/SASS是隨之而來的程式語言，大多為Rails使用者使用。SASS/SCSS有變數、巢狀、混合、繼承的特性。SCSS寫法就像原本的CSS，且CSS可以寫在SCSS裡面，而SASS不用寫分號跟大括號，寫法較為簡潔
   參考資料：https://www.slideshare.net/newstory0113/csssass 

5. Javascript系列：

   1. Javascript
      JavaScript is a programming language that is essential for web development. If you think of any web page you interact with today, JavaScript is most likely providing the action on the page. JavaScript can be used to change the status of a button when it is clicked on, create a chat window at the bottom of your screen, or even create a web-based game. With modern tools like [Node.js](https://nodejs.org/), JavaScript can now also be used to save data to a database or to create desktop applications. The applications are limitless, and we're excited to see what you'll create. Along with HTML and CSS, JavaScript is one of the three major tools you'll need to understand if you want to build a modern website. While this course won't focus on HTML or CSS, a little later on we'll show you how to include JavaScript in a basic HTML file.
      參考資料：https://www.rithmschool.com/courses/javascript/introduction-to-javascript-intro

   2. ES6
      Often in the JavaScript community you'll hear the word ECMAScript thrown around as well, as in ECMAScript 2015 (abbreviated to ES2015, or sometimes ES6). So what's the difference between ECMAScript and JavaScript? There are plenty of resources online discussing the difference, but essentially ECMAScript is a *standard*. JavaScript is an implementation of that standard. The standard dictates certain features and sets of functionality, but there can be different implementations that follow the standard. There are many different JavaScript engines that implement the ECMAScript standard and are competing for dominance; the most popular right now is Google's V8 engine
      參考資料：https://www.rithmschool.com/courses/javascript/introduction-to-javascript-intro

   3. ES7/ES8/ES9...
      參考資料：https://kknews.cc/zh-tw/news/2kebemz.html

   4. CoffeeScript

      CoffeeScript 就是語法比較簡潔易懂的 Javascript，寫完 CoffeeScript 之後可以把它編譯成 Javascript，使用 CoffeeScript 好像用 Python 在寫 Ruby，但實際上寫的是 JavaScript

      參考網址：https://larry850806.github.io/2016/06/04/coffeeScript/
      參考網址：https://kaochenlong.com/2011/08/03/coffeescript-introduction/
      參考網址（語法）：https://rocodev.gitbooks.io/rails-102/content/chapter1-mvc/a/coffeescript.html

   5. 前端Framework：

      早期的網頁幾乎都是以靜態網頁為主，所有資料直接從 Server 端輸出，幾乎不需要做運算，就是單純的 HTML。 就算有後端語言，頂多也就是負責表單的處理，GET / POST 這些大家熟到不能再熟的東西。後來過了幾年，有了「動態網頁程式」的語言 (如 PHP、ASP 等) 開始由後端程式語言負責處理頁面邏輯， 加上資料庫系統的成熟，使得原本無法紀錄狀態的網頁，可以利用資料庫來記錄狀態及資料。
      

      作為前端開發控制網頁的手段，開發者第一個要面臨的問題就是 DOM 的操作。各家瀏覽器對 JavaScript 的各種實作可以說是五花八門，其中最典型的例子就是網頁的「事件處理」。除了解決跨瀏覽器的問題之外，另一個痛點就是對於 DOM 的控制。在 jQuery 解決了 DOM 操作的問題後，首先面臨的就是狀態管理的問題。
      

      在jquery流行的時代，並沒有狀態管理的觀念，後端把資料吐在 HTML 上，我們就透過 JavaScript 直接把資料從 DOM 取出，處理完再寫回 DOM 上。 像這樣把 DOM 當做是資料結構來處理資料的方式顯然有很大的問題，當某個 DOM 被刪除的時候，是否意味著這個資料將永久消失再也拿不到了？所以當時多數人會做的事情，就是把資料往某個全域變數丟。 優點就是彈性大，缺點就是彈性太大，什麼人都可以存取。

      

      從早期的「義大利麵式程式碼」(Spaghetti code) 把所有的東西通通往 HTML 頁面塞，到了後來有人提倡「關注點分離」， 將 HTML、CSS 及 JavaScript 拆開來，這是表現層級上的關注點分離。當專案的架構越來越大，人們開始把「關注點」從表現層移到了架構層面，於是又分成了「資料層」、「表現層」以及「邏輯層」，也就是所謂的 MVC 概念。
      

      當瀏覽器開啟 View 的時候，View 會向 Controller 提出請求。 Controller 處理完商業邏輯的部分之後，要求 Model 端改變狀態，然後 Model 再將資料反映到 View 上。當前後端分離後，後端幾乎不需要去處理前端 View 的部分。 相對地，後端的 V 變成前端的 M，那些原本放在後端處理畫面、邏輯的部分，有很大程度地往前端移動，比起把原本後端的路由管理接回來用，此時前端更注重的是資料與狀態的管理，以及它們如何跟畫面的整合與同步。

      <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190913135440551.png" alt="image-20190913135440551" style="zoom:50%;" />

      參考資料：
      [https://kuro.tw/posts/2019/07/31/%E8%AB%87%E8%AB%87%E5%89%8D%E7%AB%AF%E6%A1%86%E6%9E%B6/](https://kuro.tw/posts/2019/07/31/談談前端框架/)

6. AWS與GCP

   首先要先講PaaS和IaaS

   - IaaS(Infrastructure as a Service) 基礎建設即服務:
     將網路建置及server硬體採買、是否虛擬化過程交給廠商管理，而公司本身只要針對硬體設施以外的項目如作業系統、硬體上所需執行的服務及應用程式即可，或是我們可以解釋成僅和外部廠商租賃雲端機房、伺服器、網路環境等等，而不用像過去需要在公司內部自行建置機房，而大部分提供雲端機房的環境亦提供恆溫、24小時監控外、機房本身又具備高規格的防震等級，可以有效負擔掉企業內部機房的維運成本。(例如Linode, AWS, DigitalOcean...等)
   - PaaS(Platform as a Service)平台即服務: 
     除IaaS的項目之外，更把作業系統、開發程式環境都交給廠商管理，這樣公司只要專注在資料處理及服務(應用程式)的開發項目，就像Microsoft Azure所提供的Paas服務，像IaaS一樣提供包括基礎架構服務器，存儲和網路，而且還包括開發環境、開發工具，商業智能（BI）服務，數據庫管理系統等。以微軟Azure而言，PaaS旨在支援完整的Web應用程序生命週期：構建，測試，部署，管理和更新，並於Paas項目上提供更完善的維護及管理。PaaS提供軟體部署平台（runtime），抽象掉了硬體和作業系統細節，可以無縫地擴展（scaling）。開發者只需要關注自己的業務邏輯，不需要關注底層。

   再來分別介紹AWS和GCP

   - AWS
     以下為AWS提供的服務內容，服務內容非常多樣
     ![image-20190913152938867](/Users/hanting/Library/Application Support/typora-user-images/image-20190913152938867.png)

   - GCP是google 雲端服務的統稱，但是下面有很多內容。假設今天我們用rails在本機端寫好了一個專案，需要部署的時候，有幾個選項

     1. 去買一台真的伺服器，申請真實IP與網路流量、在上面安裝linux、建置語言環境等等。
     2. 上網在遠端租一台主機、或使用VPS、Cloud Computing等方式，遠端建立VM(虛擬主機)的概念，然後再建置語言環境。
     3. 直接找像是Heroku這種，已經幫你省去建置環境的步驟，你只要把專案準備好，推上去就work。

     第二點和第三點，google都可以提供服務

     * Google Compute Engine: GCE屬於IAAS(Infrastructure as a Service)，也就是營運商提供給你運算力，讓你去處理自己的VM(虛擬機器)。對應到其他廠商的服務，像是AWS Elastic Compute及Microsoft Azure都是屬於這類型的模式。這種的好處是彈性很大，譬如你可以處理自己的load-balancingCPURAMCPU
     * Google App Engine: 像Heroku的服務
     * Google Kubernetes Engine 

7. S3
   以下為S3所提供的服務介紹 
   ![image-20190913152717704](/Users/hanting/Library/Application Support/typora-user-images/image-20190913152717704.png)

8. Apache/nginx

   在了解Apache/nginx以前，一定要先了解什麼是web server。Apache、Nginx 都是 web server，主要可以拿來 serve static files、serve dynamic web apps 。注意！但是他們都不能夠直接拿來服務 Ruby web apps，必須要透過額外的 add-on 才行。其中 Apache 應該是比較常聽到的架設選項，但是近年大家都偏好使用 Nginx 是因為他比較沒那麼肥大，而且速度比 Apache 快，所以變成了架設 server 的首選。

9. Docker
   ![image-20190914220255998](/Users/hanting/Library/Application Support/typora-user-images/image-20190914220255998.png)

   | Feauture     | Containers        | Virtual Machines ( 傳統的虛擬化 ) |
   | ------------ | ----------------- | --------------------------------- |
   | 啟動         | 秒開              | 最快也要分鐘                      |
   | 容量         | MB                | GB                                |
   | 效能         | 快                | 慢                                |
   | 支援數量     | 非常多 Containers | 10多個就很了不起了                |
   | 複製相同環境 | 快                | 超慢                              |

   

   Docker 中的幾個名詞，分別為

   ***Image*** ：映像檔，可以把它想成是以前我們在玩 VM 的 Guest OS（ 安裝在虛擬機上的作業系統 ）。Image 是唯讀（ R\O ）

   ***Container*** ：容器，利用映像檔（ Image ）所創造出來的，一個 Image 可以創造出多個不同的 Container，Container 也可以被啟動、開始、停止、刪除，並且互相分離。Container 在啟動的時候會建立一層在最外（上）層並且是讀寫模式（ R\W ）。

   ***Registry*** ：可以把它想成類似 [GitHub](https://github.com/)，裡面存放了非常多的 Image ，可在 [Docker Hub](https://hub.docker.com/) 中查看。
   參考網址：https://github.com/twtrubiks/docker-tutorial

10. Locker : https://mgleon08.github.io/blog/2017/11/01/optimistic-locking-and-pessimistic-locking/

11. Unicorn/Puma
    要了解Unicorn/Puma，首先要知道什麼是App server。通常你在 `rails new project` 的時候所預設 server 是 `WEBrick`，這就是一個 `app server`，所以你在 run `$ rails s` 的時候就會看到 `=> Booting Puma`，這是因為 rails 內建的 app server 是 `Puma`。

    參考資料：https://old.michaelhsu.tw/2013/07/04/server/

    參考資料（英文）：https://scoutapm.com/blog/which-ruby-app-server-is-right-for-you

12. rdbms與nosql

    - **rdbms** (**Relational Database Management System** 關聯式資料庫) : 
      關聯式資料庫在意的是資料之間的關聯性，也因此在設計關聯式資料庫時綱要 (Schema)。基於上述的特徵，RDBMS 有一件事情非常重要：**正規化 (normalization)**，藉由正規化避免不同資料表儲存了重複的資訊，如此一來在資料更新時可以 DB 中有重複的資料沒有同時被更新。不過也正因為這件事，綱要的設計就變得更重要，因為這時綱要的設計牽涉到的不只是正規化的問題，該怎麼更新、儲存資料到資料表就跟你的綱要有密切的關連性。綱要的設計對於 RDBMS 的重要性，不過這同時也引出了一個問題：? 嗯，通常這就是一件大工程，因為第一件事情是要把所有的資料表變更成新的綱要架構，資料量大的時候這很費工；另一件事情就是所有用到這個 DB 的程式也都要更新修改，因此這是一件大工程。

    - **nosql** (Not only **SQL**) : 
      現今網路應用的資料量會有多龐大 (像是 FB 或 google 在處理的資料量)，這些資料絕對不可能只讓一台主機去處理，一定會分散在許多主機。當 RDBMS 的資料表分散在不同主機時，想要取得某個查詢的結果或者是要更新資料時會變得相當麻煩：因為你可能會需要從主機 A 的資料表 X 去跟主機 B 的資料表 Y 先利用關鍵字找出你有興趣的資料區間，接著再這把這些資料統合成你要的結果。如果資料量又更多時，問題也不會因為僅僅多加幾台主機就能緩解，因為如何維護著這些資料表的關聯性也會是一大挑戰。所以就結論而言，在分散式系統的環境下 RDBMS 的優勢就難以發揮了，這些都是導致後來提出 NoSQL 的契機。

      NoSQL 其中一大特色就是設計 DB 時可以無綱要，他儲存資料的方法不像 RDBMS 有固定的欄位與綱要，他的概念偏向於 

      1. 每一筆資料都當成一個文件 (而且每個文件可以長得不一樣)
      2. 每一個文件都給他一個唯一的編號

      這樣的組合就像一個 hash table 的概念。NoSQL 在意的是某個編號底下的文件有沒有需要更新、需不需要新增/刪除某份/些文件等等，因此相較於 RDBMS，文件之間是否有重複的資料以及是否有任何關連性比較不是 NoSQL 在意的重點，也因此早期 NoSQL 好像真的就是指不用 SQL，不過現在普遍認為 NoSQL 應該是 Not Only SQL，其中一個原因應該是來自於 NoSQL 其實也是可以支援部分 SQL 語法的。比較常看到的例子就是某個遊戲的玩家資料，又或者是社群網站上的個人資料 (像是 FB 就自己開發了一套 NoSQL 叫 Cassandra) 等。

    參考網址：https://shininglionking.blogspot.com/2018/04/rdbms-vs-nosql.html
    參考網址：https://www.mongodb.com/scale/nosql-vs-relational-databases 

13. .erb/.haml/.slim

    * .erb : https://fireapp.kkbox.com/doc/tw/tutorial_2.html
    * .haml : https://ithelp.ithome.com.tw/articles/10128441
    * .slim : https://symphonyh.github.io/cloudblog/2017/03/06/slim/ 

14. 什麼是IP address、什麼是DNS?

    - DNS
      網際網路上的所有電腦，從智慧型手機或筆記型電腦到為大量零售網站提供內容服務的伺服器，都是使用數字找到彼此並互相通訊。這些數字稱為 **IP 地址**。當您開啟 Web 瀏覽器進入網站時，不需要記住這些冗長的數字進行輸入，而是輸入像 example.com 這樣的**網域名稱**就可以連接到正確的位置。

    - IP address

      **網際網路協定位址**（英語：Internet Protocol Address，又譯為**網際協定位址**），縮寫為**IP位址**（英語：IP Address），是分配給網路上使用網際協定（英語：Internet Protocol, IP）的裝置的數字標籤。常見的IP位址分為IPV4與IPV6
      
      Internet 中所謂的「IP 位址（IP Address）」，就像現實生活中每戶人家都要擁有唯一的地址一樣，傳送者可以根據 IP 位址進行辨識，將資料傳送到唯一目的地位址完成通訊。世界各地的 IP 位址必須具有一致性，才不會導致辨識上的混亂，故必須依循共同的規範才能達成。 　　目前我們所使用的 IP 位址為第四版 IP 位址，一般稱為 IPv4 位址。

15. GET/POST的差別?

    |            | GET                                                | POST                                                     |
    | ---------- | -------------------------------------------------- | -------------------------------------------------------- |
    | 網址差異   | 會帶有 HTML Form 表單的參數與資料。                | 網址差異網址資料傳遞時，網址並不會改變。                 |
    | 資料傳遞量 | 由於不透過 URL 帶參數，所以不受限於 URL 長度限制。 | 由於是透過 URL 帶資料，所以有長度限制。                  |
    | 安全性     | 表單參數與填寫內容可在 URL 看到。                  | 透過 HTTP Request 方式，故參數與填寫內容不會顯示於 URL。 |

    嚴格來說一般的表單可以用 GET 直接傳遞，而需要保密的資料必須用 POST 來處理，像是會員登入的帳號密碼。以下圖片為透過 GET 方式傳遞資料的 URL 呈現結果範例，可以由網址看出表單中的參數為 name，傳遞的值為 My name is Jeff.。
    參考網址：https://www.wibibi.com/info.php?tid=235

16. 什麼是session? 什麼是cookie?

17. Request和Response中間發生什麼事情?

    <img src="https://d1.awsstatic.com/Route53/how-route-53-routes-traffic.8d313c7da075c3c7303aaef32e89b5d0b7885e7c.png" alt="how-route-53-routes-traffic" style="zoom:80%;" /> 

    ![image-20190913161222851](/Users/hanting/Library/Application Support/typora-user-images/image-20190913161222851.png) 

    For example, when you type [www.google.com](http://www.google.com/), the browser will do the following things.

    1. The browser checks the cache for a DNS record to find the corresponding IP address of maps.google.com

    2. If the requested URL is not in the cache, ISP’s DNS server initiates a DNS query to find the IP address of the server that hosts maps.google.com.
    3.  The browser sends an HTTP request to the web server.
    4. The server handles the request and sends back a response.
    5. The server sends out an HTTP response.
    6. The browser displays the HTML content (for HTML responses which is the most common).

    參考資料：https://aws.amazon.com/tw/route53/what-is-dns/
    參考資料： https://medium.com/@maneesha.wijesinghe1/what-happens-when-you-type-an-url-in-the-browser-and-press-enter-bb0aa2449c1a

18. 什麼是websocket? Client和server是什麼溝通的

    - Why websocket?
      People who are new to WebSocket may ask the same question: Since we have already had the HTTP protocol, why do we need another protocol? What benefits can it bring to us? The answer is that because the HTTP protocol has a flaw: communication only can be initiated by the client.

      For example, let's say we want to know the weather today: the server will return the results of the query only if the client makes a request to the server. The HTTP protocol doesn't allow the server to push information to the client actively.

      The characteristic of the one-way request is that it will be very troublesome for the client to know if a continuous state change has happened on the server. So we have to use polling: we will send a query every other time to find out if the server has new information. The most typical scene is the chat room.

      Polling is inefficient and it will waste resources (you have to keep connecting, or the HTTP connection is always needed to be open). Therefore, in order to find a better way, engineers invented WebSocket.

      <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190914101204867.png" alt="image-20190914101204867" style="zoom:50%;" />

    - What is websocket?
      The WebSocket protocol was born in 2008 and became an international standard in 2011. All browsers have been already supported it. The biggest feature of the protocol is that the server can push information to the client actively, and the client can also send information to the server actively. It is a true two-way equal communication and is one of the server push technologies.

      <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190914145835717.png" alt="image-20190914145835717" style="zoom:50%;" />Z

      Acknowledgment: 確認

    參考資料（英文）：https://www.tutorialspoint.com/websockets/websockets_communicating_server.htm
    參考資料（易懂）：https://www.tutorialdocs.com/article/websocket-tutorial-in-10-minutes.html

19. Ruby中，Module和class不同?

    | 比一比               | 類別 Class               | 模組 Module  |
    | -------------------- | ------------------------ | ------------ |
    | 父類別 superclass    | 模組 Module              | 物件 Object  |
    | 繼承 inheritance     | 可繼承                   | 不可繼承     |
    | 包含 inclusion       | 不可被包含               | 可被包含     |
    | 延伸 extension       | 不可延伸                 | 可被延伸     |
    | 實體化 instantiation | 可被實體化(instantiated) | 不可被實體化 |

20. Ruby中，symbol和string不同?

    | 比一比         | 符號 symbol                     | 字串 string                       |
    | -------------- | ------------------------------- | --------------------------------- |
    | 意思           | 有名字的`符號物件`              | 指向`字串物件`的變數              |
    | 可不可變       | 不可變 immutable                | 可變 mutable                      |
    | 修改陣列       | 不可使用`[]=`方法               | 可使用`[]=`方法修改字串           |
    | 陣列方法       | 可使用[] 取得陣列內的字元       | 可使用[] 取得陣列內的字元         |
    | 字元方法       | 可使用.length .upcase .downcase | 可使用.l.length .upcase .downcase |
    | 符號與字串轉換 | 符號轉字串`.to_s`               | 字串轉符號`.to_sym`               |

    參考網址：https://tingtinghsu.github.io/blog/articles/2018-09-15-day07_ruby_interview_questions_symbol_vs_string

21. Private和一般method不同?
    參考網址：https://tingtinghsu.github.io/blog/articles/2018-09-15-day06_ruby_interview_questions_public_protected_private_method

22. 用Ruby寫出費氏數列

    ```ruby
    def fib(n)
      return n if n==0 || n==1
      fib(n-1)+fib(n-2)
    end
    ```

    ```Ruby
    def fib_fast(n, mem = {})
      return n if n == 0 || n == 1
      mem[n] ||= fib_fast(n-1, mem) + fib_fast(n-2, mem)
    end
    ```

23. 請圈出使用過的gem
    <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190913161641997.png" alt="image-20190913161641997" style="zoom:70%;" />

    * mysql2 : https://ihower.tw/rails/advanced-installation.html

    * Mongoid : Setting up Rails app with Mongoid

    * pg : postgresql

    * yaml_db : 將 db 資料轉成 yaml 備份、還原

    * cancancan : 一種可以處理 authorization 的 gem

      參考資料：http://wangmengqi.logdown.com/posts/1209114-cancancan-rights-management-a
      參考資料（devise,rolify,cancancan）： https://mgleon08.github.io/blog/2016/01/21/devise-rolify-cancan/

    * rolify : https://pjchender.github.io/2018/06/09/gem-rolify/

    * pundit : 

    * capabara : 使用 Selenium 的 gem，可以模擬瀏覽器的行為

    * Faker : 這個套件可以快速的產生很多種的看起來像真的「假資料」。
      參考資料：https://railsbook.tw/chapters/09-using-gems.html

    * byebug : 當程式不按預期執行時，可以試著印出 Log 或在終端裡查看來找出問題。不幸的是，有時這些方法無法有效的找出問題所在。當需要逐步追蹤程式碼如何執行，除錯器是你的最佳夥伴。除錯器也可以幫助你了解 Rails 的原始碼。若不知道從何下手，從任何一個請求切入，再運用下面教你的方法，一步一步深入 Rails 原始碼。在 Rails 應用程式裡的任何地方，可以使用 `byebug` 方法來呼叫除錯器。
      參考資料：https://rails.ruby.tw/debugging_rails_applications.html

24. Rack和Rails的關係
    Rack 提供了一個介面，讓你的 Ruby 程式可以與 Web Server 之間進行溝通，從最簡單的 Ruby 程式，到輕量級的 Ruby 框架（例如 Sinatra），甚至是本書一直在介紹的 Rails，說到底它們都是一種 Rack 的應用程式。Rack簡單來說就是提供一個能夠回應 `call` 方法的物件，並且回傳一個包含以下三個元素的陣列：

    1. HTTP 狀態（數字型態，例如正常回應是 200、找不到頁面是 404、伺服器錯誤是 500）。
    2. HTTP Header（Hash 型態）。
    3. Body（陣列型態，或是只要是個可以回應 `each` 方法的物件也可）。

    參考網址：https://railsbook.tw/extra/rack.html

25. N+1 query
    參考資料：https://mgleon08.github.io/blog/2016/01/10/ruby-on-rails-include-join-avoid-n-1-query/

26. 請完成下面TODO
    <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20190913154548284.png" alt="image-20190913154548284" style="zoom:50%;" />

    ```` ruby
    class User < ApplicationRecord
      has_many :comments
      
      def self.comments_count_hash
        return User.all.map{ |user|  [user[:account],user.comments.count] }.to_h
      end
    end
    ````

    ![image-20190915065348709](/Users/hanting/Library/Application Support/typora-user-images/image-20190915065348709.png)

    會有N+1 query
    參考資料：https://ithelp.ithome.com.tw/articles/10159101

## 額外部分

1. Proxy Server（代理伺服器）: 代理伺服器的工作是去各個 Web Server 抓取資料回來放在伺服器上來供用戶讀取下載，如此一來可以大幅減少到各個 Web server 抓取資料的時間。
   參考網址：https://www.ithome.com.tw/node/5006

2. Reverse Proxy Server（反向代理伺服器）

   反向代理伺服器做的事情恰好相反，負責將用戶端的資料傳送（HTTP）給藏在 Reverse Proxy Server 後面的 Web Server，這些躲在後面的 Web Server 不會、也不能直接被用戶直接連結，只能經由 Reverse Proxy Server 代理傳送和接收資料，如此不僅可以保護後方 Web Server 被攻擊，同時還可提供負載平衡、快取以及資料加密的功能。

3. CDN：https://fullstack.qzy.camp/posts/1755

##  其他方向

1. 多用query
   參考資料：[https://pjchender.github.io/2018/02/25/rails-active-record-query%EF%BC%88model-%E8%B3%87%E6%96%99%E6%9F%A5%E8%A9%A2%EF%BC%89/](https://pjchender.github.io/2018/02/25/rails-active-record-query（model-資料查詢）/)
2. Peter 教導如何寫測試：https://hackmd.io/@haB1MMg3Q_-pvvUi5c46SA/B18rapJ6V

## 參考資料（其他）

1. https://old.michaelhsu.tw/2013/07/04/server/
2. 論壇：http://hk.voidcc.com/tag/ruby-on-rails
3. rails好文章：https://mgleon08.github.io/blog/categories/rails/
4. ruby好文章：https://mgleon08.github.io/blog/categories/ruby/
5. length,size,count 之間的差別：https://blog.chh.tw/posts/rails-length-size-count/

## 預計要做的專案

1. https://fullstack.qzy.camp/courses/31/syllabus
   - How To: Add sign_in, sign_out, and sign_up links to your layout template：https://github.com/plataformatec/devise/wiki/How-To:-Add-sign_in,-sign_out,-and-sign_up-links-to-your-layout-template
   - 什麼是後台管理系統？https://www.design-hu.com/web-news/cms.html
   - 解答：https://fullstack.qzy.camp/posts/673