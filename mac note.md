# Note

### 操作

1. 在mac停用儀表板：https://diary.taskinghouse.com/posts/disable-dashboard-on-mac/

2. Ｑ：mac沒有剪下檔案貼上？

   Ａ：其實很簡單，選定要移動的**檔案**之後跟複製一樣按**下**快捷鍵「Command + C」，接著再到目標**檔案**夾中按**下**快捷鍵「Command + Option + V」，就可以直接把**檔案**像是**剪下**貼上那樣挪過去啦！ 所以別再說什麼**Mac 沒有剪下**貼上很難用的笑話了，因為他根本不叫**剪下**啊！

3. Control–Command–空白鍵：小圖

### 內建

1. Grapher：實用繪畫方程式模型的軟體
2. 數位測色計：可以看欲取顏色的RGB值
3. 活動監視器：ctrl+Alt+delete
4. imovie：影片編輯器
   - 可以加快影片速度：https://support.apple.com/kb/PH22933?viewlocale=zh_TW&locale=nl_BE
5. mac錄製螢幕：https://support.apple.com/zh-tw/HT208721

### 第三方