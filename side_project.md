# side project

### 規劃

1. 我想要做關於禪行/生活紀錄的網站，網站內容必須有

   - ~~提供每日恭讀的禪行週報、佛曲、心得體悟（前端~~）

   - 發文欄位：禪定(勾選、描述)、睡眠時間（描述）、運動（勾選和描述）、內省懺悔（描述）、實踐（描述）、接引（勾選、描述）、發願（描述）

   - 增設權限：無、北、中、南、最高權限。is_admin

   - 導覽列要有

     - ~~左邊：進入首頁~~
  - ~~右邊：登入、登出、註冊~~
   
   - ~~註冊資訊：email, password, password_confirm~~

   - 個人資訊：頭像（拖拉）、名字

2. ~~網站需要國際化，所以會有中文與英文版本~~

3. ~~和隆揚、至辰的討論區~~

4. 我在[1]看到有個沒有看過的用法User::COLOUR。看[2]網站明白那是在model裡面定義常數串列

   參考網址[1]：http://simple-form-bootstrap.plataformatec.com.br/documentation

   參考網址[2]：https://stackoverflow.com/questions/4110866/ruby-on-rails-where-to-define-global-constant

   可以在LabForum裡面定義「種類」常數

5. 使用ahoy：https://github.com/ankane/ahoy

   ahoy 會新增兩個表，一個是Events，另一個是Visits

6. 使用devise：https://github.com/plataformatec/devise

7. 未來網站會有一群使用者，想要在導覽列下面寫篩選條件，依照使用者、創建時間(create_at)、更新時間update_at。

### 成果

##### 討論版

![image-20191222221328795](/Users/hanting/Library/Application Support/typora-user-images/image-20191222221328795.png)

##### API (初次嘗試)

![image-20191222221721800](/Users/hanting/Library/Application Support/typora-user-images/image-20191222221721800.png)

![image-20191222221816856](/Users/hanting/Library/Application Support/typora-user-images/image-20191222221816856.png)

### 部署

deploy@140.120.32.219 的詳細資料

```text
全名 []: hanting
房間號碼 []: as789123456
工作電話 []: 0983168969
住家電話 []: 0229913688
其它 []:

Your identification has been saved in /home/deploy/.ssh/id_rsa.
Your public key has been saved in /home/deploy/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:ryhW/TNFoUHG+3phxnLiWIKXFup8F6QPHrMCI3EeBpw han@jvdiamondtech.com
The key's randomart image is:
+---[RSA 2048]----+
| . .     oo      |
|  E      .o .    |
|   .       + .   |
|  . +   . + .    |
|   = . +S= +     |
|  . + + @.= O    |
|   . * + %.X .   |
|    o +.=.O .    |
|   . ..o.. +     |
+----[SHA256]-----+


```

部署指令

```text
sudo su deploy
cap production deploy:check

sudo su hanting
cap production hanting:check
```

遇上的問題：

1. 沒有非最高權限的密碼：自己設定即可

   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191228100739260.png" alt="image-20191228100739260" style="zoom:30%;" />

2. Capistrano mkdir permission denied：https://stackoverflow.com/questions/24470520/capistrano-mkdir-permission-denied

3. ERROR linked file `/shared/config/database.yml` does not exist：https://ruby-china.org/topics/16259

4. 如何使用 git 刪除遠端分支不需要的檔案夾

   <img src="/Users/hanting/Library/Application Support/typora-user-images/image-20191228100803238.png" alt="image-20191228100803238" style="zoom:33%;" />

5. Rails佈署到ubuntu流程：https://motion-express.com/blog/%E5%B0%87rails%E4%BD%88%E7%BD%B2(deploy)%E5%88%B0vps%E4%B8%8A

目前進度：

1. 在 `user@EE911-PC03:/var/www/side-project` 資料夾裡面有以下這些檔案

   ````shell
   user@EE911-PC03:/var/www/side-project
   
   drwxrwxr-x 5 hanting hanting 4096 12月 28 09:55 ./
   drwxr-xr-x 4 hanting hanting 4096 12月 28 07:59 ../
   lrwxrwxrwx 1 hanting hanting   45 12月 28 09:55 current -> /var/www/side-project/releases/20191228015507/
   drwxrwxr-x 3 hanting hanting 4096 12月 28 10:04 releases/
   drwxrwxr-x 7 hanting hanting 4096 12月 28 09:55 repo/
   -rw-rw-r-- 1 hanting hanting  103 12月 28 09:55 revisions.log
   drwxrwxr-x 7 hanting hanting 4096 12月 28 08:23 shared/
   ````

   website only show "welcome to nginx"

   在`current`資料夾無法跑`rails db:create`

### 好文章

1. console CRUD：https://ithelp.ithome.com.tw/articles/10188185
2. active record （query）: https://rails.ruby.tw/active_record_querying.html
3. active record（驗證） : https://rails.ruby.tw/active_record_validations.html
4. blog with comment : https://web-crunch.com/lets-build-with-ruby-on-rails-blog-with-comments/

### 參考資料

- 好用的偵錯工具：http://james1239090-blog.logdown.com/posts/738014-debug-on-ruby-on-rails-debug-mode

- 在rails app放上網站 icon：https://discoposse.com/2018/09/04/setting-your-favicon-in-ruby-on-rails-apps/

  快速製作網站 icon : https://logomakr.com/1jHFqR (png檔)

  png to ico : https://convertico.com/

- font-awesome : https://github.com/bokmann/font-awesome-rails

- git reset的三種模式，以及若做壞了怎麼辦？https://dotblogs.com.tw/wasichris/2016/04/29/225157

- toggle button 的參考資料（實用）：https://stackoverflow.com/questions/39594248/how-to-add-a-switch-toggle-button-to-simple-form-in-rails

- Ruby Bootstrap大全：https://fullscreen.github.io/bh/

- 請看如何使用devise（重要）：https://andyyou.github.io/2015/04/04/devise/

- Bootstrap 頁面

  - https://github.com/BlackrockDigital/startbootstrap-blog-post
  - https://startbootstrap.com/templates/blog-post/

-  new, build, create, save方法詳述 : http://rubyer.me/blog/262/